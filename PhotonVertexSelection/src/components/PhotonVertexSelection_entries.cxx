// Gaudi/Athena include(s):
#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"

DECLARE_NAMESPACE_TOOL_FACTORY(CP, PhotonVertexSelectionTool)


DECLARE_FACTORY_ENTRIES(PhotonVertexSelection) {

   DECLARE_NAMESPACE_TOOL(CP, PhotonVertexSelectionTool)

}
