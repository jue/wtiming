// Local includes
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"

// EDM includes
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/EgammaDefs.h"

// Framework includes
#include "PathResolver/PathResolver.h"
#include "IsolationCorrections/ShowerDepthTool.h"

// ROOT includes
#include "TMVA/Reader.h"

// std includes
#include <algorithm>

namespace CP {

  //____________________________________________________________________________
  SG::AuxElement::Accessor<float> PhotonVertexSelectionTool::zvertex("zvertex");
  SG::AuxElement::Accessor<float> PhotonVertexSelectionTool::errz("errz");
  SG::AuxElement::Accessor<float> PhotonVertexSelectionTool::HPV_zvertex("HPV_zvertex");
  SG::AuxElement::Accessor<float> PhotonVertexSelectionTool::HPV_errz("HPV_errz");

  //____________________________________________________________________________
  PhotonVertexSelectionTool::PhotonVertexSelectionTool(const std::string &name)
  : asg::AsgTool(name)
  , m_showerTool(NULL)
  , m_mva1(NULL)
  , m_mva2(NULL)
  {
    declareProperty("ConfigFileMLP",
        m_configFileMLP  = "PhotonVertexSelection/DiphotonVertex_MLP.weights.xml" );
    declareProperty("ConfigFileMLP2",
        m_configFileMLP2 = "PhotonVertexSelection/DiphotonVertex_MLP2.weights.xml");
    declareProperty("conversionPtCut", m_convPtCut = 2e3);
    declareProperty("deltaPhiCut"    , m_deltaPhiCut = 0.04);
  }

  //____________________________________________________________________________
  PhotonVertexSelectionTool::~PhotonVertexSelectionTool()
  {
    SafeDelete(m_showerTool);
    SafeDelete(m_mva1);
    SafeDelete(m_mva2);
  }

  //____________________________________________________________________________
  StatusCode PhotonVertexSelectionTool::initialize()
  {
    ATH_MSG_INFO("Initializing...");

    // Shower depth tool
    m_showerTool = new CP::ShowerDepthTool();
    if (!m_showerTool->initialize()) {
      ATH_MSG_ERROR("Couldn't initialize ShowerDepthTool, failed to initialize.");
      return StatusCode::FAILURE;
    }
    
    // Get full path of configuration files for MVA
    m_configFileMLP  = PathResolverFindCalibFile(m_configFileMLP );
    m_configFileMLP2 = PathResolverFindCalibFile(m_configFileMLP2);

    // Setup MVAs
    SafeDelete(m_mva1);
    m_mva1 = new TMVA::Reader("!Color:!Silent");
    m_mva1->AddVariable("log_PV_pt_scal2 := log10(PV_pt_scal2)", &m_log_PV_pt_scal2);
    m_mva1->AddVariable("PV_deltaphi"                          , &m_PV_deltaphi    );
    m_mva1->AddVariable("PV_deltaz"                            , &m_PV_deltaz      );
    m_mva1->BookMVA    ("MLP method"                           , m_configFileMLP );

    SafeDelete(m_mva2);
    m_mva2 = new TMVA::Reader("!Color:!Silent");
    m_mva2->AddVariable("log_PV_pt_scal2 := log10(PV_pt_scal2)", &m_log_PV_pt_scal2);
    m_mva2->AddVariable("log_PV_pt_scal := log10(PV_pt_scal)"  , &m_log_PV_pt_scal );
    m_mva2->AddVariable("PV_deltaphi"                          , &m_PV_deltaphi    );
    m_mva2->AddVariable("PV_deltaz"                            , &m_PV_deltaz      );
    m_mva2->BookMVA    ("MLP method"                           , m_configFileMLP2);

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode PhotonVertexSelectionTool::getVertex(xAOD::PhotonContainer &photons, const xAOD::Vertex* &prime_vertex)
  {
    // Check if a conversion photon has a track attached to a primary/pileup vertex
    prime_vertex = getPrimaryVertexFromConv(photons);
    if (prime_vertex != NULL) {
      return StatusCode::SUCCESS;
    }

    // Update calo pointing auxdata for photons
    // TODO: Remove once variables properly included in derivations
    if (updatePointingAuxdata(photons).isFailure()) {
      ATH_MSG_WARNING("Couldn't update photon calo pointing auxdata, returning hardest vertex.");
      prime_vertex = getHardestVertex();
      return StatusCode::FAILURE;
    }
  
    // Find the common z-position from beam / photon pointing information
    double zCommon = 0.0, zCommonError = 0.0;
    if (setZCommon(photons, zCommon, zCommonError).isFailure()) {
      ATH_MSG_WARNING("Couldn't calculate zCommon, returning hardest vertex.");
      prime_vertex = getHardestVertex();
      return StatusCode::FAILURE;
    }

    // If there are any silicon conversions passing selection, use MVA1
    TMVA::Reader *reader = m_mva2;
    for (auto photon: photons) {
      if (passConvSelection(*photon)) reader = m_mva1;
    }

    // Vector sum of photons
    TLorentzVector vphotons = getPhotonsVector(photons);

    // Retrieve PV collection from TEvent
    const xAOD::VertexContainer* vertices = 0;
    if (evtStore()->retrieve(vertices, "PrimaryVertices").isFailure()) {
      ATH_MSG_WARNING("Couldn't retrieve PrimaryVertices from TEvent, returning NULL.");
      prime_vertex = NULL;
      return StatusCode::FAILURE;
    }

    // Loop over vertices and find best candidate
    float mlp = 0.0, mlp_max = -999;
    for (auto vertex: *vertices) {

      // Skip dummy vertices
      if (vertex->nTrackParticles() < 1) continue;

      // Get momentum vector of vertex
      TLorentzVector vmom = getVertexMomentum(vertex);

      // Set input variables for MVA
      m_log_PV_pt_scal  = log10(getVertexSumPt(vertex));
      m_log_PV_pt_scal2 = log10(getVertexSumPt(vertex, 2));
      m_PV_deltaphi     = fabs(vmom.DeltaPhi(vphotons));
      m_PV_deltaz       = (zCommon - vertex->z())/zCommonError;

      // Get likelihood probability from MVA
      mlp = reader->EvaluateMVA("MLP method");

      // Keep track of maximal likelihood vertex
      if (mlp > mlp_max) {
        mlp_max = mlp;
        prime_vertex = vertex;
      }

    } // loop over vertices

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  const xAOD::Vertex* PhotonVertexSelectionTool::getHardestVertex()
  {
    // Retrieve PV collection from TEvent
    const xAOD::VertexContainer* vertices = 0;
    if (evtStore()->retrieve(vertices, "PrimaryVertices").isFailure()) {
      ATH_MSG_WARNING("Couldn't retrieve PrimaryVertices from TEvent, exiting.");
      return NULL;
    }

    // Check for primary vertex in collection
    for (auto vertex: *vertices) {
      if (vertex->vertexType() == xAOD::VxType::VertexType::PriVtx)
        return vertex;
    }
    
    // Couldn't find one
    ATH_MSG_WARNING("Couldn't find a primary vertex in this event, returning NULL.");
    return NULL;
  }

  //____________________________________________________________________________
  StatusCode PhotonVertexSelectionTool::setZCommon(const xAOD::PhotonContainer &photons,
                                                   double &zCommon,
                                                   double &zCommonError)
  {
    // Clear values
    zCommon = zCommonError = 0.0;

    // Retrieve the EventInfo from TEvent
    const xAOD::EventInfo *eventInfo = 0;
    if (evtStore()->retrieve(eventInfo, "EventInfo").isFailure()) {
      ATH_MSG_WARNING("Couldn't retrieve EventInfo from TEvent, exiting.");
      return StatusCode::FAILURE;
    }

    // Beam position is the base for zCommon weighted average
    double beamPosZ      = eventInfo->beamPosZ();
    double beamPosSigmaZ = eventInfo->beamPosSigmaZ();

    zCommon       = beamPosZ/beamPosSigmaZ/beamPosSigmaZ;
    zCommonError  = 1.0     /beamPosSigmaZ/beamPosSigmaZ;

    // Include z-position pointed at by photons
    for (auto photon: photons) {
      if (passConvSelection(*photon)) {
        zCommon      += HPV_zvertex(*photon)/HPV_errz(*photon)/HPV_errz(*photon);
        zCommonError += 1.0                 /HPV_errz(*photon)/HPV_errz(*photon);
      } else {
        zCommon      += zvertex(*photon)/errz(*photon)/errz(*photon);
        zCommonError += 1.0             /errz(*photon)/errz(*photon);
      }
    }

    // Normalize by error (weighted average)
    zCommon     /= zCommonError;
    zCommonError = 1.0/sqrt(zCommonError);

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  StatusCode PhotonVertexSelectionTool::updatePointingAuxdata(xAOD::PhotonContainer &photons)
  {
    // Retrieve the EventInfo from TEvent
    const xAOD::EventInfo *eventInfo = 0;
    if (evtStore()->retrieve(eventInfo, "EventInfo").isFailure()) {
      ATH_MSG_WARNING("Couldn't retrieve EventInfo from TEvent, photons won't be decorated.");
      return StatusCode::FAILURE;
    }

    // Get beam position and data type
    float beamPosX =  eventInfo->beamPosX();
    float beamPosY =  eventInfo->beamPosY();
  
    // Loop over photons and add calo pointing auxdata
    std::pair<float, float> result;
    for (auto photon: photons) {
      // Get calo pointing variables
      result = getCaloPointing(photon->caloCluster()->e(),
                               photon->caloCluster()->etaBE(1),
                               photon->caloCluster()->etaBE(2),
                               photon->caloCluster()->phiBE(2),
                               photon->caloCluster()->p4().Theta(),
                               beamPosX,
                               beamPosY);

      // Set photon auxdata with new value
      zvertex(*photon) = result.first;
      errz(*photon)    = result.second;

      // Get conv pointing variables
      if (isSiliconConv(*photon)) {
        result = getConvPointing(photon->caloCluster()->etaBE(1),
                                 photon->caloCluster()->etaBE(2),
                                 photon->caloCluster()->phiBE(2),
                                 photon->vertex()->x(),
                                 photon->vertex()->y(),
                                 photon->vertex()->z(),
                                 beamPosX,
                                 beamPosY);
      }

      // Set photon auxdata with new value
      HPV_zvertex(*photon) = result.first;
      HPV_errz(*photon)    = result.second;

    }

    return StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  const xAOD::Vertex* PhotonVertexSelectionTool::getPrimaryVertexFromConv(const xAOD::PhotonContainer &photons)
  {
    std::vector<const xAOD::Vertex*> vertices;
    const xAOD::Vertex *conversionVertex = NULL, *primary = NULL;
    const xAOD::TrackParticle *tp = NULL;
    size_t NumberOfTracks = 0;

    for (auto photon: photons) {
      conversionVertex = photon->vertex();
      if (conversionVertex == NULL) return NULL;

      NumberOfTracks = conversionVertex->nTrackParticles();
      for (size_t i = 0; i < NumberOfTracks; ++i) {
        tp = conversionVertex->trackParticle(i);
        if (!passSiSelection(*tp)) continue;
        if (!passConvSelection(*tp, *photon)) continue;

        primary = tp->vertex();
        if (primary == NULL) continue;
        if (primary->vertexType() == xAOD::VxType::VertexType::PriVtx ||
            primary->vertexType() == xAOD::VxType::VertexType::PileUp) {
          if (std::find(vertices.begin(), vertices.end(), primary) == vertices.end()) {
            vertices.push_back(primary);
            continue;
          }
        }

      }
    }

    if (vertices.size() > 0) {
      if (vertices.size() > 1) {
        ATH_MSG_WARNING("Photons associated to different vertices! Returning lead photon association.");
      }
      return vertices[0];
    }
    return NULL;
  }

  //____________________________________________________________________________
  bool PhotonVertexSelectionTool::passConvSelection(const xAOD::TrackParticle &tp,
                                                    const xAOD::Photon &photon)
  {
    TLorentzVector track = getTrackAtFirstMeasurement(&tp);

    if (track.Pt() < m_convPtCut) return false;
    if (fabs(track.DeltaPhi(photon.caloCluster()->p4())) > m_deltaPhiCut) return false;

    return true;
  }

  //____________________________________________________________________________
  bool PhotonVertexSelectionTool::passConvSelection(const xAOD::Photon &photon)
  {
    const xAOD::Vertex *conversionVertex = photon.vertex();
    if (conversionVertex == NULL) return false;

    size_t NumberOfTracks = conversionVertex->nTrackParticles();
    for (size_t i = 0; i < NumberOfTracks; ++i) {
      if (!passSiSelection(*conversionVertex->trackParticle(i))) continue;
      if (!passConvSelection(*conversionVertex->trackParticle(i), photon)) continue;

      return true;
    }

    return false;
  }

  //____________________________________________________________________________
  bool PhotonVertexSelectionTool::passSiSelection(const xAOD::TrackParticle &tp)
  {
    uint8_t NumberOfHits = 0;

    tp.summaryValue(NumberOfHits, xAOD::numberOfPixelHits);
    if (NumberOfHits > 0) return true;

    tp.summaryValue(NumberOfHits, xAOD::numberOfSCTHits);
    if (NumberOfHits > 0) return true;

    return false;
  }

  //____________________________________________________________________________
  bool PhotonVertexSelectionTool::isSiliconConv(const xAOD::Photon &photon)
  {
    const xAOD::Vertex *conversionVertex = photon.vertex();
    if (conversionVertex == NULL) return false;
    
    size_t NumberOfTracks = conversionVertex->nTrackParticles();
    for (size_t i = 0; i < NumberOfTracks && i < 2; ++i) {
      if (!passSiSelection(*conversionVertex->trackParticle(i))) continue;

      return true;
    }
    
    return false;
  }

  //____________________________________________________________________________
  std::pair<float, float> PhotonVertexSelectionTool::getCaloPointing(const float &cl_e,
                                                                     const float &etas1,
                                                                     const float &etas2,
                                                                     const float &phis2,
                                                                     const float &cl_theta,
                                                                     const float &beamPosX,
                                                                     const float &beamPosY)
  {
    // TODO There used to be a check, so that forward photons weren't extrapolated...
    // Should this be added to the tool which calls this function?

    // Beam parameters
    double d_beamSpot   = hypot(beamPosY, beamPosX);
    double phi_beamSpot = atan2(beamPosY, beamPosX);

    // Shower depths
    std::pair<float, float> RZ1 = m_showerTool->getRZ(etas1, 1);
    std::pair<float, float> RZ2 = m_showerTool->getRZ(etas2, 2);

    // Calo cluster pointing calculation
    double phi_Calo         = atan2(sin(phis2), cos(phis2));
    double r0_with_beamSpot = d_beamSpot*cos(phi_Calo - phi_beamSpot);

    float zvertex = (RZ1.second*(RZ2.first - r0_with_beamSpot) - RZ2.second*(RZ1.first-r0_with_beamSpot)) / (RZ2.first - RZ1.first);
    float errz    = 0.5*(RZ2.first + RZ1.first)*(0.060/sqrt(cl_e*0.001)) / (sin(cl_theta)*sin(cl_theta));

    return std::make_pair(zvertex, errz);
  }

  //____________________________________________________________________________
  std::pair<float, float> PhotonVertexSelectionTool::getConvPointing(const float &etas1,
                                                                     const float &etas2,
                                                                     const float &phis2,
                                                                     const float &conv_x,
                                                                     const float &conv_y,
                                                                     const float &conv_z,
                                                                     const float &beamPosX,
                                                                     const float &beamPosY)
  {
    // TODO There used to be a check, so that forward photons weren't extrapolated...
    // Should this be added to the tool which calls this function?

    // Beam parameters
    double d_beamSpot   = hypot(beamPosY, beamPosX);
    double phi_beamSpot = atan2(beamPosY, beamPosX);

    // Shower depths
    std::pair<float, float> RZ1 = m_showerTool->getRZ(etas1, 1);

    // Photon conversion
    double conv_r = hypot(conv_x, conv_y);

    // Calo cluster pointing calculation
    double phi_Calo         = atan2(sin(phis2), cos(phis2));
    double r0_with_beamSpot = d_beamSpot*cos(phi_Calo - phi_beamSpot);
    
    float zvertex = (RZ1.second*(conv_r - r0_with_beamSpot) - conv_z*(RZ1.first - r0_with_beamSpot)) / (conv_r - RZ1.first);

    float dist_vtx_to_conv = hypot(conv_r - r0_with_beamSpot, conv_z - zvertex);
    float dist_conv_to_s1 = hypot(RZ1.first - conv_r, RZ1.second - conv_z);

    float error_etaS1 = 0.001; // FIXME
    float errz = 0.0;

    if (fabs(etas2) < 1.5) {
      // Barrel case
      float error_Z_Calo_1st_Sampling_barrel = error_etaS1*RZ1.first*fabs(cosh(etas1));
      errz = error_Z_Calo_1st_Sampling_barrel*dist_vtx_to_conv/dist_conv_to_s1;
    } else { 
      // Endcap case
      float error_R_Calo_1st_Sampling_endcap = error_etaS1*cosh(etas1)*RZ1.first*RZ1.first/fabs(RZ1.second);
      errz = error_R_Calo_1st_Sampling_endcap*fabs(sinh(etas1))*dist_vtx_to_conv/dist_conv_to_s1;
    }

    return std::make_pair(zvertex, errz);
  }

  //____________________________________________________________________________
  TLorentzVector PhotonVertexSelectionTool::getPhotonsVector(const xAOD::PhotonContainer &photons)
  {
    TLorentzVector v, v1;
    for (auto photon: photons) {
      v1.SetPtEtaPhiM(photon->e()/cosh(photon->caloCluster()->etaBE(2)),
                      photon->caloCluster()->etaBE(2),
                      photon->caloCluster()->phiBE(2),
                      0.0);
      v += v1;
    }
    return v;
  }

  //____________________________________________________________________________
  TLorentzVector PhotonVertexSelectionTool::getTrackAtFirstMeasurement(const xAOD::TrackParticle *tp)
  {
    TLorentzVector v;
    if (!tp) return v;

    static SG::AuxElement::ConstAccessor<std::vector<float> > accParameterPX("parameterPX");
    static SG::AuxElement::ConstAccessor<std::vector<float> > accParameterPY("parameterPY");

    if (!accParameterPX.isAvailable(*tp)) {
      ATH_MSG_WARNING("Could not find first parameter, returning pt at perigee");
      return tp->p4();
    }

    for (unsigned int i = 0; i < accParameterPX(*tp).size(); ++i) {
      if (tp->parameterPosition(i) == xAOD::FirstMeasurement) {
        v.SetPxPyPzE(accParameterPX(*tp)[i], accParameterPY(*tp)[i], 0.0, 0.0);
        return v;
      }
    }

    ATH_MSG_WARNING("Could not find first parameter, returning pt at perigee");
    return tp->p4();
  }

  //____________________________________________________________________________
  TLorentzVector PhotonVertexSelectionTool::getVertexMomentum(const xAOD::Vertex *vertex)
  {
    // Loop over all track particles until auxdata is filled correctly
    double px = 0.0, py = 0.0;
    for (size_t i = 0; i < vertex->nTrackParticles(); ++i) {
      TLorentzVector track = getTrackAtFirstMeasurement(vertex->trackParticle(i));
      px += track.Px();
      py += track.Py();
    }

    return TLorentzVector(px, py, 0, 0);
  }

  //____________________________________________________________________________
  double PhotonVertexSelectionTool::getVertexSumPt(const xAOD::Vertex *vertex, int power)
  {
    // Loop over all track particles until auxdata is filled correctly
    double pt = 0.0;
    for (size_t i = 0; i < vertex->nTrackParticles(); ++i) {
      TLorentzVector track = getTrackAtFirstMeasurement(vertex->trackParticle(i));
      pt += pow(track.Pt(), power);
    }
    return pt;
  }

} // namespace CP
