#ifndef PhotonVertexSelection_IPhotonVertexSelectionTool_h
#define PhotonVertexSelection_IPhotonVertexSelectionTool_h

// Framework includes
#include "AsgTools/IAsgTool.h"

// EDM includes
#include "xAODTracking/Vertex.h"
#include "xAODEgamma/PhotonContainer.h"

namespace CP {

  class IPhotonVertexSelectionTool : public virtual asg::IAsgTool {

    /// Declare the interface that the class provides
    ASG_TOOL_INTERFACE(CP::IPhotonVertexSelectionTool)

  public:
    /// Get the vertex associated with the passed photon collection
    StatusCode getVertex(const xAOD::PhotonContainer &photons, const xAOD::Vertex* &vertex);

    /// Given a list of photons, return the most likely vertex based on MVA likelihood
    StatusCode getVertex(xAOD::PhotonContainer &photons, const xAOD::Vertex* &vertex);

    /// Get vertex sum pt^power (hack until this is officialy decorated)
    double getVertexSumPt(const xAOD::Vertex *vertex, int power = 1);

  }; // class IPhotonVertexSelectionTool

} // namespace CP


#endif // PhotonVertexSelection_IPhotonVertexSelectionTool_h
