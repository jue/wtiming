#ifndef PhotonVertexSelection_PhotonVertexSelectionTool_h
#define PhotonVertexSelection_PhotonVertexSelectionTool_h

// Framework includes
#include "AsgTools/AsgTool.h"

// Local includes
#include "PhotonVertexSelection/IPhotonVertexSelectionTool.h"

// Forward declarations
namespace CP { class ShowerDepthTool; }
namespace TMVA { class Reader; }

namespace CP {

  /// Implementation for the photon vertex selection tool
  ///
  /// Takes a list of photons (for example, to two leading photons) and
  /// the most likely primary vertex, based on an MVA.
  ///
  /// @author Christopher Meyer <chris.meyer@cern.ch>
  ///
  class PhotonVertexSelectionTool : public virtual IPhotonVertexSelectionTool,
                                    public asg::AsgTool {

    /// Create a proper constructor for Athena
    ASG_TOOL_CLASS(PhotonVertexSelectionTool, CP::IPhotonVertexSelectionTool)

  private:
    /// Configuration variables
    std::string m_configFileMLP; 
    std::string m_configFileMLP2; 
    double      m_convPtCut;
    double      m_deltaPhiCut;

    /// Egamma helper tools
    CP::ShowerDepthTool *m_showerTool;

    /// MVA readers
    TMVA::Reader *m_mva1;
    TMVA::Reader *m_mva2;

    /// MVA attached discriminating variables
    float m_log_PV_pt_scal2;
    float m_log_PV_pt_scal;
    float m_PV_deltaphi;
    float m_PV_deltaz;

    /// Quick access to auxdata
    static SG::AuxElement::Accessor<float> zvertex;
    static SG::AuxElement::Accessor<float> errz;
    static SG::AuxElement::Accessor<float> HPV_zvertex;
    static SG::AuxElement::Accessor<float> HPV_errz;

  private:
    /// Calculate calorimeter pointing (should be moved to ShowerDepthTool)

    /// Calculate calorimeter HPV pointing (should be moved to ShowerDepthTool)
    std::pair<float, float> getConvPointing(const float &etas1,
                                            const float &etas2,
                                            const float &phis2,
                                            const float &conv_x,
                                            const float &conv_y,
                                            const float &conv_z,
                                            const float &beamPosX,
                                            const float &beamPosY);

    /// Check if the photon converted inside silicon
    bool isSiliconConv(const xAOD::Photon &photon);
    bool passSiSelection(const xAOD::TrackParticle &tp);

    /// Check if the photon passes conversion selection criteria
    bool passConvSelection(const xAOD::Photon &photon);
    bool passConvSelection(const xAOD::TrackParticle &tp, const xAOD::Photon &photon);

    /// Add calorimeter pointing decorations to photon list
    StatusCode updatePointingAuxdata(xAOD::PhotonContainer &photons);

    /// Set zCommon variables from beam spot and photon pointing
    StatusCode setZCommon(const xAOD::PhotonContainer &photons,
                          double &zCommon,
                          double &zCommonError);

    /// Get combined 4-vector of photon container
    TLorentzVector getPhotonsVector(const xAOD::PhotonContainer &photons);

    /// Get track momentum at first measurment
    TLorentzVector getTrackAtFirstMeasurement(const xAOD::TrackParticle *tp);

    /// Get vertex momentum (hack until this is officialy decorated)
    TLorentzVector getVertexMomentum(const xAOD::Vertex *vertex);

    /// Get possible vertex directly associated with photon conversions
    const xAOD::Vertex* getPrimaryVertexFromConv(const xAOD::PhotonContainer &photons);

  public:
    PhotonVertexSelectionTool(const std::string &name);
    virtual ~PhotonVertexSelectionTool();

    /// @name Function(s) implementing the asg::IAsgTool interface
    /// @{
    
    /// Function initialising the tool
    virtual StatusCode initialize();
      
    /// @}

    /// @name Function(s) implementing the IPhotonVertexSelection interface
    /// @{
    
    /// Given a list of photons, return the most likely vertex based on MVA likelihood
    StatusCode getVertex(xAOD::PhotonContainer &photons, const xAOD::Vertex* &vertex);

    /// Get vertex sum pt^power (hack until this is officialy decorated)
    double getVertexSumPt(const xAOD::Vertex *vertex, int power = 1);

    /// Get vertex with highest sumpt2
    const xAOD::Vertex* getHardestVertex();

    std::pair<float, float> getCaloPointing(const float &cl_e,
                                            const float &etas1,
                                            const float &etas2,
                                            const float &phis2,
                                            const float &cl_theta,
                                            const float &beamPosX,
                                            const float &beamPosY);
    /// @}

  }; // class PhotonVertexSelectionTool

} // namespace CP


#endif // PhotonVertexSelection_PhotonVertexSelectionTool_h
