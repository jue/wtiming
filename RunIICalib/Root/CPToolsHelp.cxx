#include <RunIICalib/MainCalib.h>

//Initialize CP Tools
bool MainCalib::CPToolsInit(xAOD::TEvent* event) {
    
  
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );

  
  //GRL
  if( isData ){
    m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
    std::vector<std::string> vecStringGRL;
    vecStringGRL.push_back( Form("%s/../RunIICalib/data/GRL/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml", RootCorePath.c_str() ) );
    vecStringGRL.push_back( Form("%s/../RunIICalib/data/GRL/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml", RootCorePath.c_str() ) );
    
    
    EL_RETURN_CHECK(GetName(), m_grl->setProperty("GoodRunsListVec",vecStringGRL));
    EL_RETURN_CHECK(GetName(), m_grl->setProperty("PassThrough",false));
    EL_RETURN_CHECK(GetName(), m_grl->initialize());
  }
  
  
  //Trigger Config
  m_trigConfigTool = new TrigConf::xAODConfigTool ("xAODConfigTool");
  m_trigConfigTool->msg().setLevel( MSG::FATAL );
  EL_RETURN_CHECK(GetName(),m_trigConfigTool->initialize());
  ToolHandle<TrigConf::ITrigConfigTool> trigHandle(m_trigConfigTool);
  EL_RETURN_CHECK(GetName(),trigHandle->initialize());

  
  //Trigger Decision Tool
  m_trigDecTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  m_trigDecTool->msg().setLevel( MSG::FATAL );
  EL_RETURN_CHECK(GetName(),m_trigDecTool->setProperty("ConfigTool",trigHandle));
  EL_RETURN_CHECK(GetName(),m_trigDecTool->setProperty("TrigDecisionKey","xTrigDecision"));
  EL_RETURN_CHECK(GetName(),m_trigDecTool->initialize());
  ToolHandle<Trig::TrigDecisionTool> m_trigDec(m_trigDecTool);
  
  //Muon Selection Tool
  m_muonSelTool = new CP::MuonSelectionTool("MuonSelectionTool");
  EL_RETURN_CHECK(GetName(),m_muonSelTool->setProperty("MaxEta", 2.7));   
  EL_RETURN_CHECK(GetName(),m_muonSelTool->setProperty("MuQuality", (int) xAOD::Muon::Quality::Loose));  
  EL_RETURN_CHECK(GetName(),m_muonSelTool->initialize());  
  
  //Muon Track Selection
  m_muTrkSelTool = new InDet::InDetTrackSelectionTool( "MuonTrackSelection" );
  EL_RETURN_CHECK(GetName(),m_muTrkSelTool->setProperty("CutLevel", "LooseMuon"));
  EL_RETURN_CHECK(GetName(),m_muTrkSelTool->setProperty("maxD0overSigmaD0", 6.) );
  EL_RETURN_CHECK(GetName(),m_muTrkSelTool->initialize() );
  
  //Muon Association to primary vertex
  m_mutrktovxtool = new CP::LooseTrackVertexAssociationTool("MuonLooseTrackVertexAssociationTool");
  EL_RETURN_CHECK(GetName(),m_mutrktovxtool->setProperty("dzSinTheta_cut", .5));
  EL_RETURN_CHECK(GetName(),m_mutrktovxtool->setProperty("d0_cut", 3.));
  EL_RETURN_CHECK(GetName(),m_mutrktovxtool->initialize() );
  
  //Muon Isolation Selection Tool
  m_muIso = new CP::IsolationSelectionTool("MuonIso");
  EL_RETURN_CHECK(GetName(),m_muIso->setProperty("MuonWP", "LooseTrackOnly"));
  EL_RETURN_CHECK(GetName(),m_muIso->initialize());
  EL_RETURN_CHECK(GetName(),m_muIso->addMuonWP("Loose"));
  EL_RETURN_CHECK(GetName(),m_muIso->addMuonWP("Tight"));
  EL_RETURN_CHECK(GetName(),m_muIso->addMuonWP("Gradient"));
  EL_RETURN_CHECK(GetName(),m_muIso->addMuonWP("GradientLoose"));
  

  //Jet Cleaning Tool
  m_jetcleaningTool = new JetCleaningTool("JetCleaningTool");
  EL_RETURN_CHECK(GetName(), m_jetcleaningTool->setProperty("CutLevel", "LooseBad") ); 
  EL_RETURN_CHECK(GetName(), m_jetcleaningTool->setProperty("DoUgly", false) );
  EL_RETURN_CHECK(GetName(), m_jetcleaningTool->initialize() );
  
  //Jet Vertex Tagger Tool
  m_jvtTool = new JetVertexTaggerTool("jvtTool");
  EL_RETURN_CHECK(GetName(), m_jvtTool->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root"));
  EL_RETURN_CHECK(GetName(), m_jvtTool->initialize());
  

  
  //Electron LH
  std::string confDir = "ElectronPhotonSelectorTools/offline/mc15_20160512/";
  m_LHToolMedium = new AsgElectronLikelihoodTool ("m_LHToolMedium"); //MediumLH
  EL_RETURN_CHECK(GetName(), m_LHToolMedium->setProperty("primaryVertexContainer","PrimaryVertices"));
  EL_RETURN_CHECK(GetName(), m_LHToolMedium->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2016_CutBL_Smooth.conf") );
  EL_RETURN_CHECK(GetName(), m_LHToolMedium->initialize() );
  
  
  //Electron Isolation Selection Tool
  m_elIso = new CP::IsolationSelectionTool("ElectronIso");
  EL_RETURN_CHECK(GetName(),m_elIso->setProperty("ElectronWP", "LooseTrackOnly"));
  EL_RETURN_CHECK(GetName(),m_elIso->initialize());
  EL_RETURN_CHECK(GetName(),m_elIso->addElectronWP("Loose"));
  EL_RETURN_CHECK(GetName(),m_elIso->addElectronWP("Tight"));
  EL_RETURN_CHECK(GetName(),m_elIso->addElectronWP("Gradient"));
  EL_RETURN_CHECK(GetName(),m_elIso->addElectronWP("GradientLoose"));
  

  //Electron Association to primary vertex
  m_eltrktovxtool = new CP::LooseTrackVertexAssociationTool("ElectronsLooseTrackVertexAssociationTool");
  EL_RETURN_CHECK(GetName(),m_eltrktovxtool->setProperty("dzSinTheta_cut", .5));
  EL_RETURN_CHECK(GetName(),m_eltrktovxtool->setProperty("d0_cut", 3.));
  EL_RETURN_CHECK(GetName(),m_eltrktovxtool->initialize() );

  
  //Electron Track Selection
  m_elTrkSelTool = new InDet::InDetTrackSelectionTool( "ElectronTrackSelection" );
  EL_RETURN_CHECK(GetName(),m_elTrkSelTool->setProperty("CutLevel", "LooseElectron"));
  EL_RETURN_CHECK(GetName(),m_elTrkSelTool->setProperty("maxD0overSigmaD0", 6.) );
  EL_RETURN_CHECK(GetName(),m_elTrkSelTool->initialize() );
  

  //small-R jets JES calibration
//  TString config = "JES_MC15cRecommendation_May2016.config"; 
//  TString calibSeq;
//  if( isData ) 
//    calibSeq = "JetArea_Residual_Origin_EtaJES_GSC_Insitu";
//  else 
//    calibSeq = "JetArea_Residual_Origin_EtaJES_GSC";
//  m_jetCalibration = new JetCalibrationTool(GetName(),"AntiKt4EMTopo", config, calibSeq, isData);
//  EL_RETURN_CHECK(GetName(),m_jetCalibration->initializeTool(GetName()));
  TString calibSeq = "JetArea_Residual_Origin_EtaJES_GSC";                                                                                                                                                         
  if( isData )                                                                                                                                                                                                     
	  calibSeq = "JetArea_Residual_Origin_EtaJES_GSC_Insitu";                                                                                                                                                        
  m_jetCalibration = new JetCalibrationTool( "JetCalibrationTool" );                                                                                                                                               
  EL_RETURN_CHECK(GetName(),m_jetCalibration->setProperty("JetCollection","AntiKt4EMTopo") );                                                                                                                      
  EL_RETURN_CHECK(GetName(),m_jetCalibration->setProperty("ConfigFile", "JES_data2016_data2015_Recommendation_Dec2016.config" ) );                                                                                 
  EL_RETURN_CHECK(GetName(),m_jetCalibration->setProperty("CalibSequence", calibSeq.Data() ) );                                                                                                                    
  EL_RETURN_CHECK(GetName(),m_jetCalibration->setProperty("IsData", isData) );                                                                                                                                     
  EL_RETURN_CHECK(GetName(),m_jetCalibration->initializeTool(GetName())); 


  //MET maker tool
  m_metMaker = new met::METMaker("METMaker");
  EL_RETURN_CHECK( GetName(), m_metMaker->setProperty("DoRemoveMuonJets", true) );
  EL_RETURN_CHECK( GetName(), m_metMaker->setProperty("DoSetMuonJetEMScale", true) );
  EL_RETURN_CHECK( GetName(), m_metMaker->setProperty("ORCaloTaggedMuons", true) );
  EL_RETURN_CHECK( GetName(), m_metMaker->initialize() );
  

  //Shower Depth Tool
  m_depthTool = new CP::ShowerDepthTool( );
  m_depthTool->initialize();
  Info( GetName(), "ShowerDepthTool initialized" );
  
  //Vertex Tool
  m_vertexTool = new CP::PhotonVertexSelectionTool( "vertexTool" );
  EL_RETURN_CHECK( GetName(), m_vertexTool->initialize() );
  
  
  //Overlap removal tool
  m_orToolBox = new ORUtils::ToolBox();
  ORUtils::ORFlags orFlags("OverlapRemovalTool", "selected", "ORselected");
  orFlags.bJetLabel = "isBJet"; 
  orFlags.boostedLeptons = true;  // use sliding dR cone ORs
  orFlags.doPhotons = false;      // enable/disable objects
  orFlags.doTaus = false;
  EL_RETURN_CHECK(GetName(), ORUtils::recommendedTools(orFlags, *m_orToolBox));
  EL_RETURN_CHECK( GetName(),m_orToolBox->initialize());
  


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Tools for MC only below this line
  if( isData )
    return true;
  
  
  //Pileup //added back but not being used at the moment!
  m_pileupreweighting = new CP::PileupReweightingTool("PileupReweightingTool");
  std::vector<std::string> vecStringPileupConf, vecStringPileupCalc;
  vecStringPileupCalc.push_back( Form( "%s/../RunIICalib/data/Pileup/ilumicalc_histograms_HLT_e26_lhtight_iloose_267638-271744.root", RootCorePath.c_str() ) );
  vecStringPileupConf.push_back( Form( "%s/../RunIICalib/data/Pileup/pileUP.mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar.root", RootCorePath.c_str() ) ); 
  vecStringPileupConf.push_back( Form( "%s/../RunIICalib/data/Pileup/pileUp.mc15_13TeV.testForEXOT11.root", RootCorePath.c_str() ) );
  vecStringPileupConf.push_back( Form( "%s/../RunIICalib/data/Pileup/pileUp.mc15_13TeV.testForEXOT11signals.root", RootCorePath.c_str() ) );
  double dataOneOverSF = 1.16;
  EL_RETURN_CHECK(GetName(), m_pileupreweighting->setProperty("DefaultChannel", 410000)); //FIXME
  EL_RETURN_CHECK(GetName(), m_pileupreweighting->setProperty("ConfigFiles", vecStringPileupConf));
  EL_RETURN_CHECK(GetName(), m_pileupreweighting->setProperty("LumiCalcFiles", vecStringPileupCalc));
  EL_RETURN_CHECK(GetName(), m_pileupreweighting->initialize());
  

  //Muon Calibration and Smearing Tools
  m_muonCalibrationAndSmearingTool = new CP::MuonCalibrationAndSmearingTool("MuonCalibrationTool");
  EL_RETURN_CHECK(GetName(),m_muonCalibrationAndSmearingTool->initialize());
  

  //Electron Reco Tool
  m_effToolReco = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_Reco");
  std::vector< std::string > correctionFileNameListReco;
  correctionFileNameListReco.push_back("ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v2/offline/efficiencySF.offline.RecoTrk.root");
  EL_RETURN_CHECK(GetName(),m_effToolReco->setProperty("CorrectionFileNameList", correctionFileNameListReco));
  EL_RETURN_CHECK(GetName(),m_effToolReco->setProperty("ForceDataType", 1));
  EL_RETURN_CHECK(GetName(),m_effToolReco->initialize());
  
  
  //Electron Efficiency 
  //Medium
  m_elEffMediumLHTool = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_MediumLH");
  std::vector< std::string > correctionFileNameListMedium;
  correctionFileNameListMedium.push_back("ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v3/offline/efficiencySF.offline.MediumLLH_v11.root" ); 
  EL_RETURN_CHECK(GetName(),m_elEffMediumLHTool->setProperty("CorrectionFileNameList",correctionFileNameListMedium));
  EL_RETURN_CHECK(GetName(),m_elEffMediumLHTool->setProperty("ForceDataType",1));
  EL_RETURN_CHECK(GetName(),m_elEffMediumLHTool->initialize());


  //Electron Trigger Efficiency
  //Medium
  m_trigToolMediumLH = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_trigMediumLH");
  std::vector< std::string > correctionFileNameListTrigMediumLH;
  correctionFileNameListTrigMediumLH.push_back("ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v2/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e24_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.LooseAndBLayerLLH_d0z0_v11_isolFixedCutLoose.root");
  EL_RETURN_CHECK( GetName(), m_trigToolMediumLH->setProperty("CorrectionFileNameList", correctionFileNameListTrigMediumLH) );
  EL_RETURN_CHECK( GetName(), m_trigToolMediumLH->setProperty("ForceDataType", 1 ) );
  EL_RETURN_CHECK( GetName(), m_trigToolMediumLH->initialize() );

  //Electron Calibration and Smearing
  m_elCalibrationAndSmearingTool = new CP::EgammaCalibrationAndSmearingTool("elCalibrationAndSmearingTool"); 
  EL_RETURN_CHECK(GetName(),m_elCalibrationAndSmearingTool->setProperty("ESModel", "es2016PRE"));
  EL_RETURN_CHECK(GetName(),m_elCalibrationAndSmearingTool->setProperty("decorrelationModel", "1NP_v1"));
  EL_RETURN_CHECK(GetName(),m_elCalibrationAndSmearingTool->initialize());

  
  return true;
  
}



//Close Tools
void MainCalib::CPToolsFin(){
  
  if( isData && m_grl ){ delete m_grl; m_grl = 0; }
  if( m_jetcleaningTool ){ delete m_jetcleaningTool; m_jetcleaningTool = 0; }
  if( m_jvtTool ){ delete m_jvtTool; m_jvtTool = 0; }
  if( m_trigConfigTool ){ delete m_trigConfigTool; m_trigConfigTool = 0; }
  if( m_trigDecTool ){ delete m_trigDecTool; m_trigDecTool = 0; }
  if( m_muonSelTool ){ delete m_muonSelTool; m_muonSelTool = 0; }    
  if( m_muTrkSelTool ){ delete m_muTrkSelTool; m_muTrkSelTool = 0; }
  if( m_mutrktovxtool ){ delete m_mutrktovxtool; m_mutrktovxtool = 0; }
  if( m_muIso ){ delete m_muIso; m_muIso = 0; }
  if( m_LHToolMedium ){ delete m_LHToolMedium; m_LHToolMedium = 0; }
  if( m_elIso ){ delete m_elIso; m_elIso = 0; }
  if( m_eltrktovxtool ){ delete m_eltrktovxtool; m_eltrktovxtool = 0; }
  if( m_elTrkSelTool ){ delete m_elTrkSelTool; m_elTrkSelTool = 0; }
  if( m_metMaker ){ delete m_metMaker; m_metMaker = 0; }
  if( m_depthTool ){ delete m_depthTool; m_depthTool = 0; }
  if( m_vertexTool ){ delete m_vertexTool; m_vertexTool = 0; }
  if ( m_orToolBox ) { delete m_orToolBox; m_orToolBox = 0;}


  //MC only
  if( isData )
    return;

  if( m_pileupreweighting ){ delete m_pileupreweighting; m_pileupreweighting = 0; }
  if( m_muonCalibrationAndSmearingTool ){ delete m_muonCalibrationAndSmearingTool; m_muonCalibrationAndSmearingTool = 0; }
  if( m_elEffMediumLHTool ){ delete m_elEffMediumLHTool; m_elEffMediumLHTool = 0; }
  if( m_elCalibrationAndSmearingTool ){ delete m_elCalibrationAndSmearingTool; m_elCalibrationAndSmearingTool = 0; }
  if( m_trigToolMediumLH ){ delete m_trigToolMediumLH; m_trigToolMediumLH = 0; }
  if( m_effToolReco ){ delete m_effToolReco; m_effToolReco  = 0; }

  return;

}
