#include <RunIICalib/MainCalib.h>

ClassImp(MainCalib)


MainCalib :: MainCalib () {

}


EL::StatusCode MainCalib :: setupJob (EL::Job& job){

  job.useXAOD();
  EL_RETURN_CHECK( GetName(), xAOD::Init() );
  

  // Output xAOD for W/Z slimming
  if( set_slimW == 1 || set_slimZ == 1 ){
    EL::OutputStream out ("WTiming_slimmed","xAOD");
    job.outputAdd(out);
  }
  
  return EL::StatusCode::SUCCESS;
  
}


EL::StatusCode MainCalib :: histInitialize (){
  
  // book the histograms
  bookHist();
  Info( GetName(), "Histograms successfully booked" );
  
  // add the output
  addHistOutput();
  Info( GetName(), "Histograms initialization finished" );
  //add Tree
  addOutTree();
   Info( GetName(), "Output Tree successfully added to Output");

  
  //Duplication Protection Reset
  m_seenEvents.clear();

  
  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: fileExecute (){

  
  if( set_slimW != 1 && set_slimZ != 1 )
    return EL::StatusCode::SUCCESS;

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: changeInput (bool firstFile){

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: initialize (){

  //Total Events
  xAOD::TEvent* event = wk()->xaodEvent();
  
  Info( GetName(), "Number of events = %lli", event->getEntries() );

  
  //RootCore path
  const char* tmp_path = "$ROOTCOREBIN";
  RootCorePath = gSystem->ExpandPathName( tmp_path );

  // Count number of events
  m_eventCounter = 0;
  sumOfWeights = 0;
  
  // Make list of runNum for WTiming
  Info( GetName(), Form("Pass Number: %d", set_passNumber) );
  setRunNumberList();
  if( set_slimW != 1 && set_slimZ != 1 )  {
  setChannelList();   

  Trig::WebBunchCrossingTool *bcTool= new Trig::WebBunchCrossingTool("WebBunchCrossingTool"); 
  EL_RETURN_CHECK(GetName(), bcTool ->setProperty("OutputLevel", MSG::INFO));
  EL_RETURN_CHECK(GetName(), bcTool ->setProperty("ServerAddress", "atlas-trigconf.cern.ch"));
  m_bcTool = bcTool;
  EL_RETURN_CHECK(GetName(), m_bcTool->initialize());
  }
  if( set_slimW != 1 && set_slimZ != 1 )
	  return EL::StatusCode::SUCCESS;


  //Initialize CP tools if needed
  if( !CPToolsInit(event) ) 
    return EL::StatusCode::FAILURE;
  Info( GetName(), "\nCP Tools Initialization Finished" );
  

  // Output xAOD for W/Z slimming
  if( set_slimW == 1 || set_slimZ == 1 ){
    TFile *file = wk()->getOutputFile("WTiming_slimmed");
    EL_RETURN_CHECK( "initialize()", event->writeTo(file) );
    event->setAuxItemList("PrimaryVerticesAux.","vertexType.trackParticleLinks.z");
  }
  
  
  return EL::StatusCode::SUCCESS;
  
}



EL::StatusCode MainCalib :: execute (){


  //Event
  xAOD::TEvent* event = wk()->xaodEvent();
  xAOD::TStore* store = wk()->xaodStore();
  
  // W/Z Timing already selected events
  if( set_slimW != 1 && set_slimZ != 1 && !doWTiming() )
    return StatusCode::SUCCESS;
  if( set_slimW != 1 && set_slimZ != 1 )
    return StatusCode::SUCCESS;
  
  //Duplication Removal
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );
  

  if( isData && !m_seenEvents.insert( std::make_pair( eventInfo->auxdata<unsigned int>("runNumber"), eventInfo->auxdata<unsigned long long>("eventNumber") ) ).second ){
    Info( GetName(), "Duplicated Event Removed : RunNumber = %u, EventNumber = %u", eventInfo->auxdata<unsigned int>("runNumber"), eventInfo->auxdata<unsigned long long>("eventNumber") );
    return StatusCode::SUCCESS;
  }
  
  

  //Event Counting
  if( set_debug )
    Info( GetName(), "Event number = %i, isData = %i", m_eventCounter, isData );
  if( m_eventCounter < 2 )
    Info( GetName(), "Starting the execution..." );
  if( m_eventCounter %10000== 0 )
    Info( GetName(), "Event number = %i", m_eventCounter );
  m_eventCounter++;
  h_eventCount->Fill(3);

  
  h_cutflow->Fill( 0 ); // total events 
  if( set_slimW == 1) h_cutflow_W->Fill(0); // total events
  if( set_slimZ == 1) h_cutflow_Z->Fill(0); // total events
    

  //Container Names
  evt_cont_name = "EventInfo";
  muons_cont_name = "Muons";
  ele_cont_name = "Electrons";
  jet_cont_name = "AntiKt4EMTopoJets";


  //Event weights
  wgt      = 1.000;
  mcweight = 1.000;
  puweight = 1.000;
  
  
  //Set OR decoration
  if(set_slimZ||set_slimW){
  if( applyOR && !setORdeco(event) )
    return EL::StatusCode::SUCCESS;
  if ( set_debug ) Info( GetName(), "Set OR decorations.");
  if( set_slimW == 1) h_cutflow_W->Fill(1, wgt); 
  if( set_slimZ == 1) h_cutflow_Z->Fill(1, wgt); 
  

  // Get Signal Muons
  GoodObjects goodMuons;
  if( !getSignalMuons(store, event, goodMuons ) ) 
    return EL::StatusCode::SUCCESS;
  if( set_debug ) Info( GetName(), "Processed Muons" );
  if( set_slimW == 1) h_cutflow_W->Fill(2, wgt); 
  if( set_slimZ == 1) h_cutflow_Z->Fill(2, wgt); 

  
  // Get Signal Electrons
  GoodObjects goodElectrons;
  if( !getSignalElectrons(event, goodElectrons ) )
    return EL::StatusCode::SUCCESS;
  if( set_debug ) Info( GetName(), "Processed Electrons" );
  if( set_slimW == 1) h_cutflow_W->Fill(3, wgt); 
  if( set_slimZ == 1) h_cutflow_Z->Fill(3, wgt); 

  
  // Set Event Weight 
  if( !isData )setEventWeight(event);
  sumOfWeights += mcweight;
  h_eventCount->Fill(6, mcweight);
  if( set_slimW == 1) h_cutflow_W->Fill(4, wgt); 
  if( set_slimZ == 1) h_cutflow_Z->Fill(4, wgt); 
  

  //Clean the event
  if( isData && !passClean(event) )
    return EL::StatusCode::SUCCESS;
  if( set_debug ) Info( GetName(), "Clean the Event" );
  if( set_slimW == 1) h_cutflow_W->Fill(5, wgt); 
  if( set_slimZ == 1) h_cutflow_Z->Fill(5, wgt); 
  
  
  //Analysis
  if( (set_slimW == 1 || set_slimZ == 1 ) && !doWZSlim( goodElectrons, goodMuons ) ) //W-Timing
    return EL::StatusCode::SUCCESS;
  }
  
  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: postExecute (){

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: finalize (){

  xAOD::TEvent* event = wk()->xaodEvent();

  Info( GetName(), "Finalizing..." );
  
  
  // Just plots, no xAOD
  if( set_slimW != 1  && set_slimZ != 1)
    return EL::StatusCode::SUCCESS;
  
  // W slimmed xAOD
  if( set_slimW == 1 ){

    int ib[5] = {1, 6, 8, 9, 10 };
    string slim_cuts[5] = { "All           ", 
			    "Event Clean   ", 
			    "Preselection  ",
			    "W cuts        ", 
			    "Saved         " };

      Info( GetName(), "\n\n\nW Slimming CutFlow" );

      for( int i = 0; i < 5; i++ )
        cout << setw(20) << "Cut: " << slim_cuts[i] << "; Events: " << h_cutflow_W->GetBinContent(ib[i]) << endl;
  }
  // Z slimmed xAOD
  else if( set_slimZ == 1 ){ 
    std::string slim_cuts[8] = { "All           ", "Preselection  ", 
				 "Event Clean   ", "Trigger       ",
				 "Select Letpons", "Vertex        ", 
				 "Invariant Mass", "Opp charge    "};
    Info( GetName(), "\n\n\nZ Slimming CutFlow" );
    for( int i=1; i<=8; i++)
      std::cout << "Cut: " << slim_cuts[i-1] << "; Events: " 
		<< h_cutflow_Z->GetBinContent(i) << std::endl;
  }
  TFile *file = wk()->getOutputFile("WTiming_slimmed");
  EL_RETURN_CHECK("finalize()", event->finishWritingTo(file));

  
  // close tools
  CPToolsFin();
  
  return EL::StatusCode::SUCCESS;

}



EL::StatusCode MainCalib :: histFinalize (){

  Info( GetName(), "Finalizing Histograms..." );
  // Save histograms to ouput
  if( set_slimW==1 || set_slimZ == 1){  
    TFile *file = wk()->getOutputFile("WTiming_slimmed");
    if (!file) 
      return EL::StatusCode::SUCCESS;
    file->cd();

    if ( set_slimW == 1 )         h_cutflow_W->Write();
    if ( set_slimZ == 1 )         h_cutflow_Z->Write();
    if ( h_vMass_WZ )              h_vMass_WZ->Write();
  }

  return EL::StatusCode::SUCCESS;

}
