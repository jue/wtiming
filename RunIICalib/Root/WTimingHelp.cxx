#include <RunIICalib/MainCalib.h>

// Make smaller xAOD with only selected events
bool MainCalib::doWZSlim( GoodObjects &goodElectrons,
		GoodObjects &goodMuons){

	if( set_debug ) Info( GetName(), "W-Timing processing" );


	// Common cuts for W/Z
	if( !commonCutsWZ(goodElectrons, goodMuons) )
		return false;
	if( set_debug ) Info( GetName(), "Passed all W-Timing common cuts" );
	if( set_slimW == 1) h_cutflow_W->Fill(7, wgt);

	// For W, set MET and MT cut
	if( set_slimW == 1 && !specificCutsW() ) 
		return false;
	if( set_debug ) Info( GetName(), "Passed all W-Timing W specific cuts" );
	if( set_slimW == 1) h_cutflow_W->Fill(8, wgt);

	// For Z, invariant mass cut
	if( set_slimZ == 1 && !specificCutsZ())
		return false;
	if( set_debug ) Info( GetName(), "Passed all W-Timing Z specific cuts" );

	// Save containers to ouput
	if( ! saveAuxContainers() )
		return false;
	if( set_debug ) Info( GetName(), "Saved W-Timing aux variables" );
	if( set_slimW == 1) h_cutflow_W->Fill(9, wgt); 

	return true;
}


// Run on the smaller xAOD with selected events only
// Apply corrections for each pass
// Fill plots
bool MainCalib::doWTiming(){

	//	OfflineTimingTool ott;

	//Retrieve Electrons to store variables
	xAOD::TEvent* event = wk()->xaodEvent();
	wgt = 1.0;

	const xAOD::ElectronContainer* electrons = 0;
	//EL_RETURN_CHECK( GetName(), event->retrieve( electrons, ((string)ele_cont_name).c_str() ) );
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );
	EL_RETURN_CHECK( GetName(), event->retrieve( electrons, "GoodElectrons" ) );
	xAOD::ElectronContainer::const_iterator el_itr = electrons->begin();
	xAOD::ElectronContainer::const_iterator el_end = electrons->end();

	unsigned int rN = eventInfo->auxdata<unsigned int>("runNumber");
	t_bcid = eventInfo->auxdata<unsigned int>("bcid");  


	// counters for z plots
	int el_num = 1; 

	TLorentzVector el1, el2;
	double el_t1 = -99;
	double el_t2 = -99;
	double el_e1 = -99;
	double el_e2 = -99;

	// Apply corrections to each electron
	for( ; el_itr != el_end; el_itr++ ){

		// Make sure electron flagged for this analysit_s
		//if( ( *el_itr )->auxdecor< int >( "forWTiming" ) == 0 ) continue;

		// Get OnlineId for electron
		unsigned long int onlId = (*el_itr)->auxdata<unsigned long int>("maxEcell_onlId");
		onlId = onlId >> 32; //Is stored as 64bit, last 32 bits area zero

		// Get Gain
		int gain = (*el_itr)->auxdata<int>("maxEcell_gain");
		double t_correction;
		
		bool t_valid;
		if(set_passNumber<10)
		if (gain > 2 || gain < 0) continue;
		h_vMass_WZ->Fill((*el_itr)->auxdata<float>("lvmass")/GeV, wgt);

		// Pass 0 find Avg FEB time by Run
		// Apply time of flight correction
		t_correction = tofCorrection(**el_itr); // This is t_measured-t_tof
		// Pass 1 find Avg FEB time
		// apply avg FEB time per run corr
		if( set_passNumber > 0 ){
			int ft = getFtNo(onlId);
			t_correction = applyPass0Corr(ft, t_correction, gain);
		}

		// Pass 2 find Avg Channel Time
		//
		// apply avg FEB time corr
		if( set_passNumber > 1 ){
			int feb = getFebNo(onlId);
			t_correction = applyPass1Corr(feb, t_correction, gain);
		}

		// Pass 3 find Cell Energy by Slot
		// apply avg ch time corr
		if( set_passNumber > 2){
			int ch  = getChannelNo(onlId);
			t_correction = applyPass2Corr(ch, t_correction, gain);
		}

		// Pass 4 find Cell Angular Position
		// apply avg energy time corr
		if( set_passNumber > 3){
			int slot;
			if(gain==0)
				slot = getSubslotNo(onlId);
			else
				slot = getSlotNo(onlId); 

			double en = (*el_itr)->auxdata<float>("maxEcell_energy") / GeV;
			t_correction = applyPass3Corr(en, slot, t_correction, gain);
			if(set_debug)
				cout<<"correctionforslot"<<gain<<"\t"<<slot<<"\t"<<t_correction<<endl;
		}

		// Pass 5 find Energy Fraction
		// apply angular cell position time corr
		if( set_passNumber > 4){
			// Calculate dphi/deta
			// float etas2 = (*el_itr)->caloCluster()->etaBE(2);
			// float phis2 = (*el_itr)->caloCluster()->phiBE(2);
			float etas2 = (*el_itr)->auxdata<float>("caloCluster_etas2");
			float phis2 = (*el_itr)->auxdata<float>("caloCluster_phis2");

			float cellX = (*el_itr)->auxdata<float>("maxEcell_x");
			float cellY = (*el_itr)->auxdata<float>("maxEcell_y");
			float cellZ = (*el_itr)->auxdata<float>("maxEcell_z");

			double cellR     = sqrt( cellX*cellX + cellY*cellY + cellZ*cellZ);
			double cellphi   = TMath::ATan2( cellY, cellX );
			double celltheta = TMath::ACos( cellZ/cellR );
			double celleta   = -TMath::Log( TMath::Tan(celltheta/2) );

			double deta = (celleta - etas2)/0.025; // 0.025 units
			double dphi = cellphi - phis2;
			if( dphi > TMath::Pi() )
				dphi = dphi - 2*TMath::Pi();
			else if ( dphi < -TMath::Pi() )
				dphi = dphi + 2*TMath::Pi();

			dphi = dphi/0.0245; // 0.0245 units

			int slot = getSlotNo(onlId);   
			t_correction = applyPass4Corr(dphi, deta, slot, t_correction, gain);
		}

		// Pass 6 find bunch position
		// apply energy frac time corr
		if( set_passNumber > 5){
			float f1  = (*el_itr)->auxdata<float>("f1");
			float f3  = (*el_itr)->auxdata<float>("f3");
			int slot  = getSlotNo(onlId);   
			t_correction = applyPass5Corr(f1, f3, slot, t_correction, gain);
		}
		//pass 7, channel correction 
		// bunch postion correction
		if( set_passNumber >6){
			int slot = getSlotNo(onlId);   
			t_bcid = eventInfo->auxdata<unsigned int>("bcid");
			if(m_bcTool->gapBeforeTrain( t_bcid )>500)
				t_dis_from_front=m_bcTool->distanceFromFront( t_bcid );
			else
				t_dis_from_front=m_bcTool->distanceFromFront( t_bcid)*2+m_bcTool->distanceFromTail( t_bcid)+m_bcTool->gapBeforeTrain( t_bcid);	
			Info( "wtiming::setBunchPosition", Form("distance %d, fromtai: %d,gap: %d", m_bcTool->distanceFromFront( t_bcid ),m_bcTool->distanceFromTail( t_bcid), m_bcTool->gapBeforeTrain( t_bcid)));
			if(slot!=8&&slot!=9&&t_dis_from_front<=3000)
				t_correction = applyPass6Corr(slot,t_dis_from_front, t_correction, gain);

		}
		// Pass 8, find avg ft corrretion
		// apply 2nd Avg Channel Time
		if( set_passNumber > 7){
			int ch  = getChannelNo(onlId);
			t_correction = applyPass7Corr(ch, t_correction, gain);
		}
		// Pass 8, apply 2nd Avg FT time
		//		if( set_passNumber > 8){
		//			int ft = getFtNo(onlId);
		//			t_correction = applyPass0Corr(ft, t_correction, gain);
		//		}
		//		//test the offlinetool
		// Get the Run Number
		//	       tie(t_valid, t_correction) = ott.getCorrectedTime<const xAOD::Electron>(**el_itr, rN, (*el_itr)->auxdata<float>("PV_z"), t_dis_from_front);
		//	       cout<<"from ott"<<t_valid<<"\t"<<t_correction<<endl;


		if( set_passNumber == 10 ){
			if(el_num == 1){
				el_t1 = t_correction;
				el_e1 = (*el_itr)->auxdata<float>("maxEcell_energy")/GeV;
				el1.SetPtEtaPhiM( (*el_itr)->auxdata<float>("calib_pt"),
						(*el_itr)->auxdata<float>("calib_eta"),
						(*el_itr)->auxdata<float>("calib_phi"),
						(*el_itr)->auxdata<float>("calib_m"));
			}
			if(el_num == 2){
				el_t2 = t_correction;
				el_e2 = (*el_itr)->auxdata<float>("maxEcell_energy")/GeV;
				el2.SetPtEtaPhiM( (*el_itr)->auxdata<float>("calib_pt"),
						(*el_itr)->auxdata<float>("calib_eta"),
						(*el_itr)->auxdata<float>("calib_phi"),
						(*el_itr)->auxdata<float>("calib_m"));
			}
			el_num++;
		}
		// Print info about large outliers

		//    if( gain == 0 && t_correction >  1.0 ||t_correction <-1.0  ){
		//      std::cout << "strict_high"<<"\t"<<onlId<<"\t"<<getFebNo(onlId)<<"\t"<<getChannelNo(onlId)<<"\t"<<t_correction<<std::endl; 
		//          }
		// Make Plots
		if(!savetree){
			if (gain!=2)
			if( !makeWTimingPlots( **el_itr, t_correction ))
				Info( GetName(), "Issue making plots!");
		}
		else{
			if( !makeWTimingTrees( **el_itr, t_correction ) )
				Info( GetName(), "Issue making trees!");
		}
	}//end of electron

	// Z histos that need both electrons
	if( set_passNumber == 10){
		// set leading/subleading
		double t_lead, t_sub;
		if( el_e1 > el_e2){
			t_lead = el_t1;
			t_sub  = el_t2;
		}else{
			t_lead = el_t2;
			t_sub  = el_t1;
		}
	h_invariantMassZee->Fill( (el1+el2).M()/GeV, wgt);  
		// fill the histos
		// At least 20 GeV
		if( el_e1 > 20 && el_e2 > 20){
			h_t1vst2Zee->Fill(t_lead, t_sub, wgt); // temporarily putting inside this 20 GeV cut!
			h_t1plust2Zee->Fill(t_lead+t_sub, wgt);
			h_t1minust2Zee->Fill(t_lead - t_sub, wgt);
			h_t1Zee->Fill( t_lead, wgt);
			h_t2Zee->Fill( t_sub, wgt);
		}
	}

	if(savetree){ 
		tree->Fill();
	}
	return true;

}


// Apply TOF correction
double MainCalib::tofCorrection(const xAOD::Electron &el){

	// MaxEnergy Cell info
	double el_x = el.auxdata<float>("maxEcell_x");
	double el_y = el.auxdata<float>("maxEcell_y");
	double el_z = el.auxdata<float>("maxEcell_z");
	double el_t = el.auxdata<float>("maxEcell_time");
	int    gain = el.auxdata<int>("maxEcell_gain");
	unsigned long int onlId = el.auxdata<unsigned long int>("maxEcell_onlId");
	onlId = onlId >> 32; //Is stored as 64bit, last 32 bits area zero

	// Speed of light in mm/ns
	const double c_mmns = 299.792458;

	// Distances for cell and Cell minus PV_z
	double r_cell = sqrt( el_x*el_x + el_y*el_y + el_z*el_z);
	double r_path = sqrt( el_x*el_x + el_y*el_y + (el_z - PV_z)*(el_z - PV_z));

	// TOF correction
	double t_diff = (r_path - r_cell)/c_mmns;

	// Plots
//	h_timeVsPathDiff[gain]->Fill( (r_path-r_cell), el_t, wgt);
//	h_dtVsPathDiff[gain]->Fill( (r_path-r_cell), (el_t - t_diff) ,wgt);

	return (el_t-t_diff);
}


double MainCalib::applyPass0Corr(int ft, double t_corr, int gain){

	// Need Event info
	xAOD::TEvent* event = wk()->xaodEvent();
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );

	// Get the Run Number
	unsigned int rN = eventInfo->auxdata<unsigned int>("runNumber");
	// Find index for runNumber
	int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();
	//CJ///
	//apply to differnet iovs, just change 1+pos to 1+(previous runs)+pos
	// Apply correction for each run separately
	if( pos >= runNumberList.size() ) {
		Info( GetName(),  "Warning!! Unexpected RunNumber!" );
		return t_corr;
	}
	else{
		pos = pos + 0;
		// For Pass 1 and above subtract the Pass 0 Offsets
		if( set_passNumber > 0){
			double dt;
		//	if( gain == 2)
		//		dt = std::stod( pass0CorrectionsL[ft][1+pos]);
		//	else{
				dt = (gain == 0 ? std::stod( pass0CorrectionsH[ft][1+pos] ) : std::stod( pass0CorrectionsM[ft][1+pos] ) );
				t_corr -= dt; 
				if(set_passNumber > 8){
					dt = (gain == 0 ? std::stod( pass8CorrectionsH[ft][1+pos] ) : std::stod( pass8CorrectionsM[ft][1+pos] ) );
					t_corr -= dt;
				}
		//	}
		}
	}

	return t_corr;
}



// Apply avg FEB correcctions
// (Calculated after pass 1)
double MainCalib::applyPass1Corr(int feb, double t_corr, int gain){

	// For Pass 2 and above, subtract pass 1 offsets
	if( set_passNumber > 1){
		double dt = (gain == 0 ? std::stod( pass1CorrectionsH[feb] ) : std::stod( pass1CorrectionsM[feb] ) );
		t_corr -= dt;
	}

	return t_corr;

}




// Apply the channel by channel avg corrections
// (Calculated after pass 2)
//

double MainCalib::applyPass2Corr(int ch, double t_corr, int gain){
	// Need Event info
	xAOD::TEvent* event = wk()->xaodEvent();
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );
	// Get the Run Number
	unsigned int rN = eventInfo->auxdata<unsigned int>("runNumber");
	// Find index for runNumber
	int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();
	// Apply correction for each iov separately
	if( pos >= runNumberList.size() ) {
		Info( GetName(),  "Warning!! Unexpected RunNumber!" );
		return t_corr;
	}
	// For Pass 3 and above, subtract pass 2 offsets
	if( set_passNumber > 2){
		int iov_num=5;
		for(int k=0; k<5;k++)
			if( set_debug)
				cout<<"iov"<<iovNumberList[k]<<endl;
		double dt;
		if(pos<iovNumberList[0]) iov_num=1;
		else if(pos>=iovNumberList[0]&&pos<(iovNumberList[0]+iovNumberList[1])) iov_num=2;
		else if(pos>=(iovNumberList[0]+iovNumberList[1])&&pos<(iovNumberList[0]+iovNumberList[1]+iovNumberList[2])) iov_num=3;
		else if(pos>=(iovNumberList[0]+iovNumberList[1]+iovNumberList[2])&&pos<(iovNumberList[0]+iovNumberList[1]+iovNumberList[2]+iovNumberList[3]))iov_num=4;
		if(set_debug)
			Info( GetName(), Form("Channel correction for iov: %d", iov_num));
	//	if( gain ==2)
	//		dt = std::stod( pass2CorrectionsL[ch][iov_num] );
	//	else
			dt = (gain == 0 ? std::stod( pass2CorrectionsH[ch][iov_num] ) : std::stod( pass2CorrectionsM[ch][iov_num] ) );
		if( set_debug)
			cout<<"debug_findme"<<"\t"<<iov_num<<"\t"<<pass2CorrectionsH[ch][iov_num]<<"\t"<<ch<<endl;
		t_corr -= dt; 
	}
	return t_corr;
}

// Apply the energy avg time corrections
// (Calculated after pass 3)
double MainCalib::applyPass3Corr(double en, int slot, double t_corr, int gain){

	// For Pass 4 and above, subtract pass 3 offsets
	double dt;
	if( set_passNumber > 3){
		// [slot][0] slot index [slot][1-2] fit range [slot][3-8] p0-p6 fit params
		double minX = (gain == 0 ? std::stod( pass3CorrectionsH[slot][0+1] ) : std::stod( pass3CorrectionsM[slot][0+1] ) );
		double bond = (gain == 0 ? std::stod( pass3CorrectionsH[slot][0+1+1] ) : std::stod( pass3CorrectionsM[slot][0+1+1] ) );
		double maxX = (gain == 0 ? std::stod( pass3CorrectionsH[slot][1+1+1] ) : std::stod( pass3CorrectionsM[slot][1+1+1] ) );
		double p0 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][0+1+1+2] ) : std::stod( pass3CorrectionsM[slot][0+1+1+2] ) );
		double p1 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][1+1+1+2] ) : std::stod( pass3CorrectionsM[slot][1+1+1+2] ) );
		double p2 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][2+1+1+2] ) : std::stod( pass3CorrectionsM[slot][2+1+1+2] ) );
		double p3 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][3+1+1+2] ) : std::stod( pass3CorrectionsM[slot][3+1+1+2] ) );
		double p4 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][4+1+1+2] ) : std::stod( pass3CorrectionsM[slot][4+1+1+2] ) );
		double p5 = (gain == 0 ? std::stod( pass3CorrectionsH[slot][5+1+1+2] ) : std::stod( pass3CorrectionsM[slot][5+1+1+2] ) );
		double p6, p7, p8, p9;
		if(gain==1){
			p6 =  std::stod( pass3CorrectionsM[slot][2+2+2+4] );
			p7 =  std::stod( pass3CorrectionsM[slot][3+2+2+4] );
			p8 =  std::stod( pass3CorrectionsM[slot][4+2+2+4] );
			p9 =  std::stod( pass3CorrectionsM[slot][5+2+2+4] );
		}


		// Constant correction outside range of fit
		if( en < minX ) en = minX;
		if( en > maxX ) en = maxX;
		if(gain==0)
			dt = p0 + p1*en + p2*en*en + p3*en*en*en + p4*en*en*en*en + p5*en*en*en*en*en;
		if(gain==1){
			if(en<=bond){
				dt = p0 + p1*en + p2*en*en + p3*en*en*en + p4*en*en*en*en + p5*en*en*en*en*en;
			}
			else{
				dt = p6 + p7*en + p8*en*en + p9*en*en*en; 
			}
		}

		t_corr -= dt; 
		if(gain==0)
		cout<<t_corr<<endl;
	}

	return t_corr;
}


// Apply the dphi/deta corrections
// (Calculated after pass 4)
double MainCalib::applyPass4Corr(double dphi, double deta, int slot, double t_corr, int gain){

	// For Pass 5 and above, subtract pass 4 offsets
	if( set_passNumber > 4){
		// [slot][0] slot index [slot][1-5] dphi fit params
		double pminX = (gain == 0 ? std::stod( pass4CorrectionsH[slot][0+1] ) : std::stod( pass4CorrectionsM[slot][0+1] ) );
		double pmaxX = (gain == 0 ? std::stod( pass4CorrectionsH[slot][1+1] ) : std::stod( pass4CorrectionsM[slot][1+1] ) );   
		double p0 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][0+1+4] ) : std::stod( pass4CorrectionsM[slot][0+1+4] ) );
		double p1 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][1+1+4] ) : std::stod( pass4CorrectionsM[slot][1+1+4] ) );
		double p2 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][2+1+4] ) : std::stod( pass4CorrectionsM[slot][2+1+4] ) );
		double p3 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][3+1+4] ) : std::stod( pass4CorrectionsM[slot][3+1+4] ) );
		double p4 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][4+1+4] ) : std::stod( pass4CorrectionsM[slot][4+1+4] ) );
		// [slot][0] slot index [slot][6-10] deta fit params
		double eminX = (gain == 0 ? std::stod( pass4CorrectionsH[slot][0+1+2] ) : std::stod( pass4CorrectionsM[slot][0+1+2] ) );
		double emaxX = (gain == 0 ? std::stod( pass4CorrectionsH[slot][1+1+2] ) : std::stod( pass4CorrectionsM[slot][1+1+2] ) );
		double e0 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][0+1+4+5] ) : std::stod( pass4CorrectionsM[slot][0+1+4+5] ) );
		double e1 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][1+1+4+5] ) : std::stod( pass4CorrectionsM[slot][1+1+4+5] ) );
		double e2 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][2+1+4+5] ) : std::stod( pass4CorrectionsM[slot][2+1+4+5] ) );
		double e3 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][3+1+4+5] ) : std::stod( pass4CorrectionsM[slot][3+1+4+5] ) );
		double e4 = (gain == 0 ? std::stod( pass4CorrectionsH[slot][4+1+4+5] ) : std::stod( pass4CorrectionsM[slot][4+1+4+5] ) );

		// Correction
		double dtp = p0 + p1*dphi + p2*dphi*dphi + p3*dphi*dphi*dphi + p4*dphi*dphi*dphi*dphi;
		double dte = e0 + e1*deta + e2*deta*deta + e3*deta*deta*deta + e4*deta*deta*deta*deta;
		// Constant correction outside the fit range
		if( dphi < -0.5 ) dtp = pminX;
		if( dphi > 0.5 )  dtp = pmaxX;
		if( deta < -0.5 ) dte = eminX;
		if( deta > 0.5 )  dte = emaxX;

		t_corr -= (dtp + dte); 
	}

	return t_corr;
}



// Apply the energy frac corrections
// (Calculated after pass 5)
double MainCalib::applyPass5Corr(float f1, float f3, int slot, double t_corr, int gain){

	// For Pass 6 and above, subtract pass 5 offsets
	if( set_passNumber > 5){
		// [slot][0] slot index [slot][1-2] f1 fit params
		double f1_min = (gain == 0 ? std::stod( pass5CorrectionsH[slot][0+1] ) : std::stod( pass5CorrectionsM[slot][0+1] ) );
		double f1_max = (gain == 0 ? std::stod( pass5CorrectionsH[slot][1+1] ) : std::stod( pass5CorrectionsM[slot][1+1] ) );
		double f1_p0 = (gain == 0 ? std::stod( pass5CorrectionsH[slot][0+1+4] ) : std::stod( pass5CorrectionsM[slot][0+1+4] ) );
		double f1_p1 = (gain == 0 ? std::stod( pass5CorrectionsH[slot][1+1+4] ) : std::stod( pass5CorrectionsM[slot][1+1+4] ) );
		// [slot][0] slot index [slot][3-4] f3 fit params
		double f3_min = (gain == 0 ? std::stod( pass5CorrectionsH[slot][0+1+2] ) : std::stod( pass5CorrectionsM[slot][0+1+2] ) );
		double f3_max = (gain == 0 ? std::stod( pass5CorrectionsH[slot][1+1+2] ) : std::stod( pass5CorrectionsM[slot][1+1+2] ) );
		double f3_p0 = (gain == 0 ? std::stod( pass5CorrectionsH[slot][0+1+4+2] ) : std::stod( pass5CorrectionsM[slot][0+1+4+2] ) );
		double f3_p1 = (gain == 0 ? std::stod( pass5CorrectionsH[slot][1+1+4+2] ) : std::stod( pass5CorrectionsM[slot][1+1+4+2] ) );

		// constant correciton outside fit range
		if( f1 < f1_min ) f1 = f1_min;
		if( f1 > f1_max ) f1 = f1_max;
		if( f3 < f3_min ) f3 = f3_min;
		if( f3 > f3_max ) f3 = f3_max;

		double dt1 = f1_p0 + f1_p1*f1;
		double dt3 = f3_p0 + f3_p1*f3;
		t_corr -= (dt1 + dt3); 
	}

	return t_corr;
}



//
// Apply bunch postion avg corrections
// (Calculated after Pass 6)
//

double MainCalib::applyPass6Corr(int slot, int dist, double t_corr, int gain){

	// For Pass 7 and above, subtract Pass 7 offsets
	if( set_passNumber > 6){
		dist=dist/25;
		double dt;
	
		if(slot!=9&&slot!=8)
			dt = (gain == 0 ? std::stod( pass6CorrectionsH[slot][1+dist] ) : std::stod( pass6CorrectionsM[slot][1+dist] ) );
		if(set_debug)
			cout<<t_dis_from_front<<"\t"<<dt<<"\t"<<gain<<"\t"<<slot<<endl;
		t_corr -= dt; 
	}

	return t_corr;
}

//
// Apply the channel by channel avg corrections
// (Calculated after Pass 7)
//

double MainCalib::applyPass7Corr(int ch, double t_corr, int gain){

	// For Pass 8 and above, subtract Pass 7 offsets
	if( set_passNumber > 7){
		double dt = (gain == 0 ? std::stod( pass7CorrectionsH[ch] ) : std::stod( pass7CorrectionsM[ch] ) );
		t_corr -= dt; 
	}

	return t_corr;
}

double MainCalib::applyPassCorr_notused(double mu, double t_corr, int gain){

	if( set_passNumber > 10){
		double f_minp = (gain == 0 ? std::stod(  passCorrections_notusedH[0][0] ) : std::stod(   passCorrections_notusedM[0][0] ) );
		double f_maxp = (gain == 0 ? std::stod(  passCorrections_notusedH[0][1] ) : std::stod(   passCorrections_notusedM[0][1] ) );
		//    double p[12];
		//    for(int k=0;k<12;k++){
		double p[5];
		for(int k=0;k<5;k++){
			p[k] = (gain == 0 ? std::stod(  passCorrections_notusedH[0][2+k] ) : std::stod(   passCorrections_notusedM[0][2+k] ) );
		}
		// constant correciton outside fit range
		if( mu < f_minp ) mu = f_minp;
		if( mu > f_maxp ) mu = f_maxp;


		double dt;
		dt = p[0] + p[1]*mu + p[2]*mu*mu + p[3]*mu*mu*mu  + p[4]*mu*mu*mu*mu ;//+ p[5]*mu*mu*mu*mu*mu+ p[6]*mu*mu*mu*mu*mu*mu+ p[7]*mu*mu*mu*mu*mu*mu*mu;
		t_corr -= 0;//dt; 
	}

	return t_corr;
	}



	void MainCalib::setChannelList(){ 

		// Update this config file 
		ifstream f1(Form( "%s/../RunIICalib/data/config/channel.txt", RootCorePath.c_str() ));
		//list of channel that have have a larger energy compared to the other channel in the same slot. 
		ifstream f2(Form( "%s/../RunIICalib/data/config/large_channel.txt", RootCorePath.c_str() ));
		int ch;  

		// Store each run into local vector 
		while( f1 >> ch ){ 
			channelList.push_back(ch); 
		}   
		f1.close(); 
		// Print Loaded Run Numbers 
		Info( GetName(), Form("Total Number of channels: %lu", channelList.size()) ); 
		Info( GetName(), Form("Channel %d: %d", 0, channelList[0]) ); 
		Info( GetName(), Form("Channel %lu: %d", channelList.size()-1, channelList.back() ) ); 

		while( f2 >> ch ){ 
			channelList_for_subslot.push_back(ch); 
		}   
		f2.close(); 
		// Print Loaded Run Numbers 
		Info( GetName(), Form("Total Number of channels: %lu", channelList_for_subslot.size()) ); 
		Info( GetName(), Form("Channel %d: %d", 0, channelList_for_subslot[0]) ); 
		Info( GetName(), Form("Channel %lu: %d", channelList_for_subslot.size()-1, channelList_for_subslot.back() ) ); 

		return; 
	} 




	// Read list of run numbers for book keeping
	void MainCalib::setRunNumberList(){
		ifstream f(Form( "%s/../RunIICalib/data/config/iov.txt", RootCorePath.c_str() )) ;
		int iov_n;
		while(f>>iov_n)
			iovNumberList.push_back(iov_n);
		f.close();

		// Update this config file
		ifstream f1(Form( "%s/../RunIICalib/data/config/RunNumberList.txt", RootCorePath.c_str() )) ;
		int runNum;

		// Store each run into local vector
		if( f1.is_open() ){
			while( f1 >> runNum ){
				runNumberList.push_back(runNum);
			}
			f1.close();
		}
		else{
			cerr<<"File : config/RunNumberList.txt is missing "<<endl;
			exit(6);
		}

		// Print Loaded Run Numbers
		Info( GetName(), Form("Total Number of Runs: %lu", runNumberList.size()) );
		Info( GetName(), Form("Run %d: %d", 0, runNumberList[0]) );
		Info( GetName(), Form("Run %lu: %d", runNumberList.size()-1, runNumberList.back() ) );

		std::string configHigh[9]   = {"runByRunFTOffsetsH.dat",
			"AvgFebOffsetsH.dat",
			"AvgChOffsetsH.dat",
			"AvgEnSlFitH.dat",
			"AvgdPhidEtaSlFitH.dat",
			"Avgf1f3SlFitH.dat",
			//	 "AvgmuFitH.dat",
			"AvgBunchOffsets0.dat",
			"AvgChOffsetsp6H.dat",
			"runByRunFTOffsetsH_2nd.dat"
		};

		std::string configMedium[9] = {"runByRunFTOffsetsM.dat",
			"AvgFebOffsetsM.dat",
			"AvgChOffsetsM.dat",
			"AvgEnSlFitM.dat",
			"AvgdPhidEtaSlFitM.dat",
			"Avgf1f3SlFitM.dat",
			//	 "AvgmuFitM.dat",
			"AvgBunchOffsets1.dat",
			"AvgChOffsetsp6M.dat",
			"runByRunFTOffsetsM_2nd.dat"};
		//  std::string configLow[7]    = {"config/runByRunFTOffsetsL.dat",
		//                                 "config/AvgFebOffsetsL.dat"};
		// Decide how many corrections to load
		int nCorrToLoad = (set_passNumber == 10 ? 8 : set_passNumber - 1); // only max of 8 corrections to load
		if( set_debug == 1 )
			Info( GetName(), "Corrections to load %d", nCorrToLoad );


		// Loop over each correction file and load to local vector
		for( int nCorr = 0; nCorr <= nCorrToLoad; nCorr++ ){
			// high and medium gain files
			ifstream f2( Form( "%s/../RunIICalib/data/config/%s", RootCorePath.c_str() ,configHigh[nCorr].c_str()  ) );
			ifstream f3( Form( "%s/../RunIICalib/data/config/%s", RootCorePath.c_str() ,configMedium[nCorr].c_str() ) );
			// to read each line and separate the corrections
			std::string line;
			std::vector< std::string > corrections;
			// High gain corrections
			while( std::getline(f2, line) ){
				boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
				switch( nCorr){
					case 0: 
						pass0CorrectionsH.push_back(corrections); break;
					case 1:
						pass1CorrectionsH.push_back(corrections[1]); break;
					case 2:
						pass2CorrectionsH.push_back(corrections); break;
					case 3:
						pass3CorrectionsH.push_back(corrections); break;
					case 4:
						pass4CorrectionsH.push_back(corrections); break;
					case 5:
						pass5CorrectionsH.push_back(corrections); break;
					case 6:
						pass6CorrectionsH.push_back(corrections); break;
					case 11:
						passCorrections_notusedH.push_back(corrections); break;
					case 7:
						pass7CorrectionsH.push_back(corrections[1]); break; 
					case 8: 
						pass8CorrectionsH.push_back(corrections); break;
				}
			} // end loading high gain
			f2.close();

			if( set_debug )
				Info( GetName(), "Ended with high gain" );

			// Medium gain corrections
			while( std::getline(f3, line) ){
				boost::split( corrections, line, boost::is_any_of(" "), boost::token_compress_on);
				switch( nCorr){
					case 0: 
						pass0CorrectionsM.push_back(corrections); break;
					case 1:
						pass1CorrectionsM.push_back(corrections[1]); break;
					case 2:
						pass2CorrectionsM.push_back(corrections); break;
					case 3:
						pass3CorrectionsM.push_back(corrections); break;
					case 4:
						pass4CorrectionsM.push_back(corrections); break;
					case 5:
						pass5CorrectionsM.push_back(corrections); break;
					case 6:
						pass6CorrectionsM.push_back(corrections); break;
					case 11:
						passCorrections_notusedM.push_back(corrections); break;
					case 7:
						pass7CorrectionsM.push_back(corrections[1]); break;
					case 8: 
						pass8CorrectionsM.push_back(corrections); break;
				}
			} // end loading medium gain
			f3.close();

			if( set_debug )
				Info( GetName(), "Ended with medium gain" );

			// Print how many corrections loaded
			switch( nCorr ){
				case 0:
					Info( GetName(), Form("Pass 0 correction High FT[%d] run[%d]: %s",13,3,pass0CorrectionsH[13][3+1].c_str() ) );
					Info( GetName(), Form("Pass 0 correction Med  FT[%d] run[%d]: %s",13,3,pass0CorrectionsM[13][3+1].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass0CorrectionsH.size(),pass0CorrectionsM.size()) ); break;
				case 1:
					Info( GetName(), Form("Pass 1 correction High feb[%d]: %s",19,pass1CorrectionsH[19].c_str() ) );
					Info( GetName(), Form("Pass 1 correction Med  feb[%d]: %s",19,pass1CorrectionsM[19].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass1CorrectionsH.size(),pass1CorrectionsM.size()) ); break;
				case 2:
					Info( GetName(), Form("Pass 2 correction High ch[%d] iov[%d]: %s",1009, 2, pass2CorrectionsH[1009][2].c_str() ) );
					Info( GetName(), Form("Pass 2 correction Med  ch[%d] iov[%d]: %s",1009, 2, pass2CorrectionsM[1009][2].c_str() ) );
					// Info( GetName(), Form("Pass 2 correction Low  ch[%d]: %s",1009,pass2CorrectionsL[1009].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High, %lu Med and %lu Low Corrections",
								nCorr,pass2CorrectionsH.size(),pass2CorrectionsM.size()) ); break;
				case 3:
					Info( GetName(), Form("Pass 3 correction High sl[%d] p[%d]: %s",18, 3, pass3CorrectionsH[18][3+1+2].c_str() ) );
					Info( GetName(), Form("Pass 3 correction Med  sl[%d] p[%d]: %s",18, 3, pass3CorrectionsM[18][3+1+2].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass3CorrectionsH.size(),pass3CorrectionsM.size()) ); break;
				case 4:
					Info( GetName(), Form("Pass 4 correction High sl[%d] p[%d]: %s",5, 2, pass4CorrectionsH[5][2+1+4].c_str() ) );
					Info( GetName(), Form("Pass 4 correction Med  sl[%d] p[%d]: %s",5, 7, pass4CorrectionsM[5][2+1+4+5].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass4CorrectionsH.size(),pass4CorrectionsM.size()) ); break;
				case 5:
					Info( GetName(), Form("Pass 5 correction High sl[%d] p[%d]: %s",2, 0, pass5CorrectionsH[2][0+1].c_str() ) );
					Info( GetName(), Form("Pass 5 correction Med  sl[%d] p[%d]: %s",2, 0, pass5CorrectionsM[2][0+1].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass5CorrectionsH.size(),pass5CorrectionsM.size()) ); break;
				case 6:
					Info( GetName(), Form("Pass 6 correction High sl[%d] dist[%d]: %s",13,3,pass6CorrectionsH[13][3+1].c_str() ) );
					Info( GetName(), Form("Pass 6 correction Med  sl[%d] dist[%d]: %s",13,3,pass6CorrectionsM[13][3+1].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass6CorrectionsH.size(),pass6CorrectionsM.size()) ); break;
				case 7:
					Info( GetName(), Form("Pass 7 correction High ch[%d]: %s",1009,pass7CorrectionsH[1009].c_str() ) );
					Info( GetName(), Form("Pass 7 correction Med  ch[%d]: %s",1009,pass7CorrectionsM[1009].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass7CorrectionsH.size(),pass7CorrectionsM.size()) ); break;
				case 8:
					Info( GetName(), Form("Pass 8 correction High FT[%d] run[%d]: %s",13,3,pass8CorrectionsH[13][3+1].c_str() ) );
					Info( GetName(), Form("Pass 8 correction Med  FT[%d] run[%d]: %s",13,3,pass8CorrectionsM[13][3+1].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,pass8CorrectionsH.size(),pass8CorrectionsM.size()) ); break;
				case 11:
					Info( GetName(), Form("Pass 6 correction High p[%d]: %s",1,passCorrections_notusedH[0][3].c_str() ) );
					Info( GetName(), Form("Pass 6 correction Med  p[%d]: %s",1,passCorrections_notusedM[0][3].c_str() ) );
					Info( GetName(), Form("Successfully Loaded Pass %d corrections, %lu High and %lu Med Corrections",
								nCorr,passCorrections_notusedH.size(),passCorrections_notusedM.size()) ); break;
			} // end printing loaded constants


			if( set_debug )
				Info( GetName(), "Printed Loaded Constants" );


		} // end loop over loading configuration files

		return;
	}



	// Make sure 3 Tracks from PV
	bool MainCalib::vertexTracks(){

		// Retrieve event
		xAOD::TEvent * event = wk()->xaodEvent();

		// Count number of vertices with 3 tracks
		int nVtx3Trks = 0;

		// Get the primary vertices 
		const xAOD::VertexContainer* primVtxCont = 0;
		EL_RETURN_CHECK( GetName(), event->retrieve( primVtxCont, "PrimaryVertices") );

		for ( const xAOD::Vertex* vtx : *primVtxCont){
			if( vtx->vertexType() != xAOD::VxType::PriVtx) continue; // Find the PV
			if( vtx->nTrackParticles() > 2 ) nVtx3Trks++; // Ask for 3 tracks
			PV_z = vtx->z();
			PV_mult = vtx->nTrackParticles();
		}

		if ( nVtx3Trks < 1 ) return false;

		return true;
	}


	// After selection, save appropriate variables
	bool MainCalib::saveAuxContainers(){

		//Retrieve the event to store variables
		xAOD::TEvent* event = wk()->xaodEvent();
		const xAOD::EventInfo* eventInfo = 0;
		EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );


		// Good Electrons will be stored here
		xAOD::ElectronContainer* g_els = new xAOD::ElectronContainer();
		xAOD::AuxContainerBase* g_elsAux = new xAOD::AuxContainerBase();
		g_els->setStore(g_elsAux);


		// Get electron container
		const xAOD::ElectronContainer* electrons = 0;
		EL_RETURN_CHECK( GetName(), event->retrieve( electrons, ((string)ele_cont_name).c_str() ) );
		xAOD::ElectronContainer::const_iterator el_itr = electrons->begin();
		xAOD::ElectronContainer::const_iterator el_end = electrons->end();


		// Load the PV
		const xAOD::VertexContainer* primVtxCont = 0;
		EL_RETURN_CHECK( GetName(), event->retrieve( primVtxCont, "PrimaryVertices") );
		xAOD::VertexContainer::const_iterator ver_itr = primVtxCont->begin();
		xAOD::VertexContainer::const_iterator ver_end = primVtxCont->end();


		// Loop over the electrons
		for( ; el_itr!=el_end; el_itr++){

			// Make sure electron flagged for this analysis
			if( ( *el_itr )->auxdecor< int >( "forWTiming" ) == 0 ) continue;

			// Make new electron, copying the variables we want
			xAOD::Electron* ee = new xAOD::Electron();
			g_els->push_back( ee);

			// Save the variables we are interested in
			(ee)->auxdata<unsigned long int>("maxEcell_onlId") = (*el_itr)->auxdata<unsigned long int>("maxEcell_onlId");
			(ee)->auxdata<int>("maxEcell_gain")       = (*el_itr)->auxdata<int>("maxEcell_gain");
			(ee)->auxdata<float>("maxEcell_energy")   = (*el_itr)->auxdata<float>("maxEcell_energy");
			(ee)->auxdata<float>("maxEcell_time")     = (*el_itr)->auxdata<float>("maxEcell_time");
			(ee)->auxdata<float>("maxEcell_x")        = (*el_itr)->auxdata<float>("maxEcell_x");
			(ee)->auxdata<float>("maxEcell_y")        = (*el_itr)->auxdata<float>("maxEcell_y");
			(ee)->auxdata<float>("maxEcell_z")        = (*el_itr)->auxdata<float>("maxEcell_z");
			(ee)->auxdata<float>("caloCluster_etas2") = (*el_itr)->caloCluster()->etaBE(2);
			(ee)->auxdata<float>("caloCluster_etas1") = (*el_itr)->caloCluster()->etaBE(1);
			(ee)->auxdata<float>("caloCluster_phis2") = (*el_itr)->caloCluster()->phiBE(2);
			(ee)->auxdata<float>("caloCluster_eta")   = (*el_itr)->caloCluster()->eta();
			(ee)->auxdata<float>("caloCluster_phi")   = (*el_itr)->caloCluster()->phi();
			(ee)->auxdata<float>("caloCluster_e")     = (*el_itr)->caloCluster()->e();
			(ee)->auxdata<float>("calib_pt")          = (*el_itr)->auxdata<float>("calib_pt");
			(ee)->auxdata<float>("calib_eta")         = (*el_itr)->auxdata<float>("calib_eta");
			(ee)->auxdata<float>("calib_phi")         = (*el_itr)->auxdata<float>("calib_phi");
			(ee)->auxdata<float>("calib_m")           = (*el_itr)->auxdata<float>("calib_m");
			(ee)->auxdata<float>("charge")            = (*el_itr)->auxdata<float>("charge");
			(ee)->auxdata<float>("f1")                = (*el_itr)->auxdata<float>("f1");
			(ee)->auxdata<float>("f3")                = (*el_itr)->auxdata<float>("f3");
			(ee)->auxdata<float>("Reta")              = (*el_itr)->auxdata<float>("Reta");
			if( set_slimW == 1) {
				(ee)->auxdata<float>("lvmass")            = lvmass;
				(ee)->auxdata<float>("METsum")            = METsum;
			}


			// Store the PV data
			for ( ; ver_itr != ver_end; ver_itr++){
				if( (*ver_itr)->vertexType() != xAOD::VxType::PriVtx) continue; // Find the PV
				ee->auxdecor< float >( "PV_mult") = (*ver_itr)->nTrackParticles();
				ee->auxdecor< float >( "PV_z")    = (*ver_itr)->z();
			}

			// Depth EM2 taking into account misalignment vs eta
			float etas2 = ( *el_itr )->caloCluster()->etaBE(2);
			float phi   = ( *el_itr )->caloCluster()->phi();
			ee->auxdecor< float >( "depth" ) = m_depthTool->getCorrectedShowerDepthEM2( etas2, phi, isData);

			// Vertex - calo pointing
			float cl_e     = (*el_itr)->caloCluster()->e()/TMath::CosH((*el_itr)->caloCluster()->eta());
			float etas1    = (*el_itr )->caloCluster()->etaBE(1);
			float phis2    = (*el_itr )->caloCluster()->phiBE(2);
			float cl_theta = 2.*TMath::CosH( TMath::Exp( -1. * (*el_itr)->caloCluster()->eta() ) );
			float beamPosX = eventInfo->beamPosX();
			float beamPosY = eventInfo->beamPosY();
			std::pair<float,float> verexz = m_vertexTool->getCaloPointing( cl_e, etas1, etas2, phis2, cl_theta, beamPosX, beamPosY );
			ee->auxdecor< float >( "zvertex" ) = verexz.first;
			ee->auxdecor< float >( "errz" )    = verexz.second;

		} // end loop over electrons

		// Save the deep copy
		EL_RETURN_CHECK("execute()", event->record( g_els, "GoodElectrons"));
		EL_RETURN_CHECK("ececute()", event->record( g_elsAux, "GoodElectronsAux."));

		// copy full event info container
		EL_RETURN_CHECK("execute()", event->copy("EventInfo"));
		// EL_RETURN_CHECK("execute", event->copy("Electrons"));
		EL_RETURN_CHECK("execute", event->copy("PrimaryVertices"));
		event->fill();

		return true;
	}
