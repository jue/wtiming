#include <RunIICalib/MainCalib.h>

//Electron Reconstruction Efficiency
double MainCalib::elRecoEff( const xAOD::Electron &el ){
  
  elrecoeff = 1.0;
  
  if( !isData ){
    double elLHeff = 1.0, eltrigLHeff = 1.0;
    
    if( (int)set_el_LH == 1 ){
      m_elEffMediumLHTool->getEfficiencyScaleFactor( el, elLHeff);
      m_trigToolMediumLH->getEfficiencyScaleFactor( el, eltrigLHeff);
    }
    else{
      cout<<"Provide other working points efficiency for electrons "<<endl;
      exit(6);
    }
    
    m_effToolReco->getEfficiencyScaleFactor( el, elrecoeff);
  
    elrecoeff *= elLHeff*eltrigLHeff;
  }

  return elrecoeff;

}
  

//Electron Calibration and Smearing
double MainCalib::elCalibPt( const xAOD::Electron &el ){

  double calibPt = -999.;

  if( isData )
    calibPt = el.auxdata<float>("pt"); 

  else{
    xAOD::Electron *tempel = 0;
    if ( m_elCalibrationAndSmearingTool->correctedCopy( el, tempel ) == CP::CorrectionCode::Error) {
      Error(GetName(), "Failed to apply electron calibration. Exiting."); 
      return EL::StatusCode::FAILURE;
    }
    calibPt = tempel->pt();
    delete tempel;
  }
  
  return calibPt;

}


//Muon Calibration and Smearing                                                                                                               
double MainCalib::muCalibPt( xAOD::Muon &mu ){

  double calibPt = -999.;

  if( isData ) 
    calibPt = mu.auxdata<float>("pt");
  else{
    if ( m_muonCalibrationAndSmearingTool->applyCorrection(mu) == CP::CorrectionCode::Error ) {
      Error(GetName(), "Failed to apply muon calibration. Exiting."); 
      return EL::StatusCode::FAILURE;
    }
    calibPt = mu.pt();
  }

  return calibPt;

}


//small-R jets calibration
double MainCalib::jetCalibPt( const xAOD::Jet &jet ){

  double calibpt = -999.; 

  xAOD::Jet* tempjet = 0;
  if( m_jetCalibration->calibratedCopy(jet,tempjet) == CP::CorrectionCode::Error){
    Error(GetName(), "Failed to apply jet calibration. Exiting.");
    return EL::StatusCode::FAILURE; 
  }
  calibpt = tempjet->pt();
  delete tempjet;
  
  return calibpt;

}


