#include <RunIICalib/MainCalib.h>

void MainCalib::bookHist(){

  //general plots + object debugging plots
  h_cutflow = new TH1F( "h_cutflow", "Cut Flow;Selection;Events", 20, 0, 20 );
  h_wcutflow = new TH1F( "h_wcutflow", "Cut Flow;Selection;Events", 20, 0, 20 );
  h_elcutflow = new TH1F( "h_elcutflow", "Electrons Cut Flow;Selection;Events", 30, 0, 30 );
  h_mucutflow = new TH1F( "h_mucutflow", "Muons Cut Flow;Selection;Events", 30, 0, 30 );
  h_elwcutflow = new TH1F( "h_elwcutflow", "Electrons Cut Flow;Selection;Events", 30, 0, 30 );
  h_muwcutflow = new TH1F( "h_muwcutflow", "Muons Cut Flow;Selection;Events", 30, 0, 30 );
  h_fjcutflow = new TH1F( "h_fjcutflow", "Fat Jets Cut Flow;Selection;Events", 20, 0, 20 );
  h_mud0s = new TH1F( "h_mud0s", ";d_{0}/#sigmad_{0};Events", 400, -20, 20 );
  h_muz0 = new TH1F( "h_muz0", ";z_{0};Events", 400, -20, 20 );
  h_eld0s = new TH1F( "h_eld0s", ";d_{0}/#sigmad_{0};Events", 400, -20,20 );
  h_elz0 = new TH1F( "h_elz0", ";z_{0};Events", 400, -20, 20 );

  //event counts!
  h_eventCount = new TH1F("h_eventCount", "h_eventCount", 6, 1, 7);
  h_eventCount -> GetXaxis() -> SetBinLabel(1, "nEvents initial");
  h_eventCount -> GetXaxis() -> SetBinLabel(2, "nEvents selected in");
  h_eventCount -> GetXaxis() -> SetBinLabel(3, "nEvents selected out");
  h_eventCount -> GetXaxis() -> SetBinLabel(4, "sumOfWeights initial");
  h_eventCount -> GetXaxis() -> SetBinLabel(5, "sumOfWeights selected in");
  h_eventCount -> GetXaxis() -> SetBinLabel(6, "sumOfWeights selected out");

  //object scale factors
  h_muSF = new TH1F("h_muSF", "h_muSF", 20, 0, 2);
  h_elSF = new TH1F("h_elSF", "h_elSF", 20, 0, 2);
  h_wgt = new TH1F("h_wgt", "h_wgt", 20, 0, 2);
  h_mcweight = new TH1F("h_mcweight", "h_mcweight", 20, 0, 2);
  h_el_passOR = new TH1F("h_el_passOR", "h_el_passOR", 2, 0, 2);
  h_mu_passOR = new TH1F("h_mu_passOR", "h_mu_passOR", 2, 0, 2);
  h_jet_passOR = new TH1F("h_jet_passOR", "h_jet_passOR", 2, 0, 2);


  //Precision W timing
  // for slimming
  h_vMass_WZ    = new TH1F(Form("h_vMass_WZ"),";M [GeV];Events", 150., 0., 150. );
  if( set_slimW == 1)
    h_cutflow_W = new TH1F( "h_cutflow_W", "W Slimming Cut Flow;Selection;Events", 20, 0, 20 );
  if( set_slimZ == 1)
    h_cutflow_Z = new TH1F( "h_cutflow_Z", "Z Slimming Cut Flow;Selection;Events", 20, 0, 20 );
  
  // no slimming!
  if( set_slimW != 1 && set_slimZ != 1){
    // high(0)/medium(1) gain
    std::string gain[2] = {"High","Medium"};
    // Zplots
    h_invariantMassZee   = new TH1F(Form("h_invariantMassZee"),";M_{e^+e^-};Events", 150., 0., 150. );
    h_t1vst2Zee          = new TH2F(Form("h_t1vst2Zee"),";Leading Electron Time[ns];SubLeading Electron Time[ns]", 1000, -10., 10.,1000, -10.,10. );
    h_t1plust2Zee        = new TH1F(Form("h_t1plust2Zee"),";t_{1}+t_{2}[ns]",1000, -10.,10. );
    h_t1minust2Zee       = new TH1F(Form("h_t1minust2Zee"),";t_{1}-t_{2}[ns]",1000, -10.,10. );
    h_t1Zee              = new TH1F(Form("h_t1Zee"),";t_{1}[ns]",1000, -10.,10. );
    h_t2Zee              = new TH1F(Form("h_t2Zee"),";t_{1}[ns]",1000, -10.,10. );
    
    for(int g = 0; g < 2; g++){
      h_transverseMassWenu[g] = new TH1F(Form("h_transverseMassWenu%s",gain[g].c_str()), ";M_{e^{#pm} #nu}", 150., 0., 150. );
      h_numElectrons[g]       = new TH1F(Form("h_numElectrons%s",gain[g].c_str()), "Number of Electrons;Num;Counts",10,0,10);
      h_electronPt[g]         = new TH1F(Form("h_electronPt%s",gain[g].c_str()), "Leading Electron Pt;Leading electron P_{T} [GeV]; electrons/1 GeV",300,0,300);
      // MaxEcell histograms
      h_maxEcellEnergy[g]     = new TH1F(Form("h_maxEcellEnergy%s",gain[g].c_str()), "Energy of cell with max Energy in cluster;Cell Energy [GeV];Cells/2GeV",50,0,100);
      // MET histograms
      h_met[g]                = new TH1F(Form("h_met%s",gain[g].c_str()), "Missing Transverse Energy;#slash{E}_{T} [GeV];entries/1 GeV",300,0,300);
      // Z vertex
      h_electronZvertex[g]    = new TH1F(Form("h_electronZvertex%s",gain[g].c_str()), "Electron Zvertex;Zvertex -Z_{PV} [mm]; electrons/2 mm",500,-2000,2000);
      h_dZVsTrackMult[g]      = new TH2F(Form("h_dZVsTrackMult%s",gain[g].c_str()), "Electron Zvertex;Track Multiplicity on PV;Zvertex -Z_{PV} [mm]",300,0,300,500,-2000,2000);
      h_absDZVsTrackMult[g]   = new TH2F(Form("absDZVsTrackMult%s",gain[g].c_str()), "Absolute Value Electron Zvertex;Track Multiplicity on PV;|Zvertex-Z_{PV}| [mm]",300,0,300,500,0.,2000);
      // Timing
      for( int i=0; i<22; i++){
	h_cellTimeAll[g][i]     = new TH1F(Form("h_cellTimeAll%s%d",gain[g].c_str(),i), "Associated layer 2 cell time slot [ns];Time [ns]; Events / 0.02 ns", 1000,-25,25);
	h_cellTimeVsEta[g][i]   = new TH2F(Form("h_cellTimeVsEta%s%d",gain[g].c_str(),i), "Cell Time vs eta for slot;#eta_{e}; Time [ns]",125,-2.5,2.5,500,-5,5);
	h_cellTimeVsPhi[g][i]   = new TH2F(Form("h_cellTimeVsPhi%s%d",gain[g].c_str(),i), "Cell Time vs phi for slot; #phi_{e}; Time [ns]",32,-3.1416,3.1416,500,-5,5);
	h_cellTimeVsPVz[g][i]   = new TH2F(Form("h_cellTimeVsPVz%s%d",gain[g].c_str(),i), "CellTime vs z(PV);Associated z_{PV} [mm]; Time [ns]",1000,-500,500,500,-5,5);
	h_cellTimeVsf1[g][i]    = new TH2F(Form("h_cellTimeVsf1%s%d",gain[g].c_str(),i), "Cell Time vs f1 for all gains;f1_{e}; Time [ns]",400,-0.05,0.6,500,-5,5);
	h_cellTimeVsf3[g][i]    = new TH2F(Form("h_cellTimeVsf3%s%d",gain[g].c_str(),i), "Cell Time vs f3 for all gains;f3_{e}; Time [ns]",400,-0.05,0.2,500,-5,5);
	h_cellTimeVsdEta[g][i]  = new TH2F(Form("h_cellTimeVsdEta%s%d",gain[g].c_str(),i), "Cell Time vs deta for slot;#delta#eta[0.025 units]; Time [ns]",100,-1,1,500,-5,5);
	h_cellTimeVsdPhi[g][i]  = new TH2F(Form("h_cellTimeVsdPhi%s%d",gain[g].c_str(),i), "Cell Time vs dphi for slot;#delta#phi[0.0245 units]; Time [ns]",400,-2,2,500,-5,5);
	h_bunch_pos[g][i]	= new TH2F(Form("h_timeVsbunchpos%s%d",gain[g].c_str(),i), "Time VS Bunch position; Bunch Position[bcid]; Time [ns]",120,-0.5,2999.5,1000,-25,25);   
      }
      h_cellTimeVsrEta[g]     = new TH2F(Form("h_cellTimeVsrEta%s",gain[g].c_str()), "Cell Time vs reta for all gains;reta_{e}; Corrected Cell Time [ns]",100,0,1.5,250,-25,25);
      h_efracDistVsTime[g]    = new TH2F(Form("h_efracDistVsTime%s",gain[g].c_str()), "EfracVsTime;E_{max}/E_{cl};Time [ns]",100,0,1,1000,-10,10);
      h_cellTimeVsdZ0[g]      = new TH2F(Form("h_cellTimeVsdZ0%s",gain[g].c_str()), "Cell Time vs Distance from PV;z_{e}^{track}-z_{PV} [mm];Corrected CellTime [ns]",1000,-500,500,500,-5,5);
      // Depth
      h_timeVsDepth[g]        = new TH2F(Form("h_timeVsDepth%s",gain[g].c_str()), "Time VS Depth; Electron Depth [mm]; Time [ns]",300,1400,2000,250,-25,25);
      h_timeVsDepth_etaLT08[g]= new TH2F(Form("h_timeVsDepth_etaLT08%s",gain[g].c_str()), "Corrected Time VS el_depth; Electron Depth [mm]; Time [ns]",300,1400,2000,250,-25,25);
      h_timeVsDepth_etaGT08[g]= new TH2F(Form("h_timeVsDepth_etaGT08%s",gain[g].c_str()), "Corrected Time VS el_depth; Electron Depth [mm]; Time [ns]",300,1400,2000,250,-25,25);
      h_timeVsDepthProfile[g] = new TProfile(Form("timeVsDepthProfile%s",gain[g].c_str()), "Corrected Time VS el_depth; Electron Depth [mm]; Time [ns]",400,1400,1800);
      h_cellEnergyVsDepth[g]  = new TH2F(Form("h_cellEnergyVsDepth%s",gain[g].c_str()), "Cell Energy Vs Depth;Electron Depth [mm]; Max E cell Energy [GeV]",600,1400,2000,200,0,200);
      h_cellFracVsDepth[g]    = new TH2F(Form("h_cellFracVsDepth%s",gain[g].c_str()), "Cell Frac Vs Depth;  Electron Depth [mm]; Max E cell Energy/Cluster Energy",600,1400,2000,200,0,1);
      h_mtVsDepth[g]          = new TH2F(Form("h_mtVsDepth%s",gain[g].c_str()), "Transverse Mass Vs Depth; Electron Depth [mm]; M_{T} [GeV]",300,1400,2000,150,0,150);
      h_etaVsDepth[g]         = new TH2F(Form("h_etaVsDepth%s",gain[g].c_str()), "Electron eta Vs Depth; Electron Depth [mm]; Electron #eta",300,1400,2000,50,-2,2);
      h_phiVsDepth[g]         = new TH2F(Form("h_phiVsDepth%s",gain[g].c_str()), "Electron phi Vs Depth; Electron Depth [mm]; Electron #eta",300,1400,2000,100,-5,5);
      // Significance histograms
      h_pointingSig[g]        = new TH1F(Form("h_pointingSig%s",gain[g].c_str()), "Pointing significance;zVertex/Err;Entries/0.5#sigma",400,-100,100);
      
      // Path diff
      h_timeVsPathDiff[g]     = new TH2F(Form("h_timeVsPathDiff%s",gain[g].c_str()),"Time vs Path length difference; Difference in Flight Path[mm]; Time[ns]",500,-250,250,500, -25, 25);
      h_dtVsPathDiff[g]       = new TH2F(Form("h_dtVsPathDiff%s",gain[g].c_str()),"tofCorrTime vs Path length difference; Difference in Flight Path[mm]; dTOF[ns]",500,-250,250,500, -25, 25);
      
      // dphi/deta
      h_dphi[g]               = new TH1F(Form("h_dphi%s",gain[g].c_str()),"dPhi;#delta#phi[0.0245 units]; ",400, -2,2);
      h_deta[g]               = new TH1F(Form("h_deta%s",gain[g].c_str()),"dEta;#delta#eta[0.025 units]; ",100, -1,1);
      //Pass 0
      for(int i=0; i<NRUNS; i++){
	h_RunAvgFTTime[g][i]  = new TH2F(Form("h_RunAvgFTTime_%s%d",gain[g].c_str(),i),Form("Avg FT Time for Run[%d]; FT Number; Time[ns]",i),114,-0.5,113.5,500,-5,5);
//	h_RunAvgFebTime[g][i]  = new TH2F(Form("h_RunAvgFebTime_%s%d",gain[g].c_str(),i),Form("Avg Feb Time for Run[%d]; FEB Number; Time[ns]",i),620,-0.5,619.5,500,-5,5);
//	h_RunAvgChannelTime[g][i] = new TH2F(Form("h_RunChannelTime_%s%d",gain[g].c_str(),i),Form("Avg Channel Time for Run[%d]; Channel Number; Time[ns]",i),Nch,-0.5,Nch-0.5,1500,-20,20);
      }
      //Pass 1
      h_AvgFebTime[g]          = new TH2F(Form("h_AvgFebTime_%s",gain[g].c_str()),"Average time vs FEB; FEB; Time [ns]", 620,-0.5,619.5,500,-5,5);
      //Pass 2
      if(set_passNumber == 3|| set_passNumber == 2 || set_passNumber == 7 || set_passNumber == 8){    
	h_AvgChannelTime[g]      = new TH2F(Form("h_AvgChannelTime_%s",gain[g].c_str()),"Average time vs Channel; Channel; Time [ns]", 79360,-0.5,79359.5,500,-25,25);
      }
      //Pass 3
      
    //  for(int s=0; s<22; s++){
      for(int s=0; s<22+7*(1-g); s++){
	h_AvgEnergyTime[g][s]  = new TH2F(Form("h_AvgEnergyTime_%s_%d",gain[g].c_str(),s),Form("Average time vs Energy Slot %d; Energy[GeV]; Time [ns]",s), 250,0,250,500,-5,5);
      }
        h_timeVsmu[g]           = new TH2F(Form("h_timeVsmu%s",gain[g].c_str()), "Time VS Mu; Average interation per crossing; Time [ns]",1000,-10,60,1000,-25,25);
    }
  }
 
  return;

}

void MainCalib::addHistOutput(){

  //general plots + object debugging plots
  wk()->addOutput( h_cutflow );
  wk()->addOutput( h_wcutflow );
  wk()->addOutput( h_elcutflow );
  wk()->addOutput( h_mucutflow );
  wk()->addOutput( h_elwcutflow );
  wk()->addOutput( h_muwcutflow );
  wk()->addOutput( h_fjcutflow );
  wk()->addOutput( h_mud0s );
  wk()->addOutput( h_muz0 );
  wk()->addOutput( h_eld0s );
  wk()->addOutput( h_elz0 );

  wk()->addOutput(h_eventCount);
  wk()->addOutput(h_muSF);
  wk()->addOutput(h_elSF);
  wk()->addOutput(h_wgt);
  wk()->addOutput(h_mcweight);

  wk()->addOutput(h_el_passOR);
  wk()->addOutput(h_mu_passOR);
  wk()->addOutput(h_jet_passOR);
  

  // slimming plots
  wk()->addOutput(h_vMass_WZ);
  if( set_slimW == 1)
    wk()->addOutput(h_cutflow_W);
  if( set_slimZ == 1)
    wk()->addOutput(h_cutflow_Z);
  if( set_slimW != 1 && set_slimZ !=1 ){
    // Z plots
    wk()->addOutput(h_invariantMassZee);
    wk()->addOutput(h_t1vst2Zee);
    wk()->addOutput(h_t1plust2Zee);
    wk()->addOutput(h_t1minust2Zee);
    wk()->addOutput(h_t1Zee);
    wk()->addOutput(h_t2Zee);
    for(int g=0; g<2; g++){
      wk()->addOutput(h_transverseMassWenu[g]);
      // Basic Info
      wk()->addOutput(h_numElectrons[g]);
      wk()->addOutput(h_electronPt[g]);
      // Max Ecell histograms
      wk()->addOutput(h_maxEcellEnergy[g]);
      // MET
      wk()->addOutput(h_met[g]);
      // Z vertex
      wk()->addOutput(h_electronZvertex[g]);
      wk()->addOutput(h_dZVsTrackMult[g]);
      wk()->addOutput(h_absDZVsTrackMult[g]);
      // Timing
      for(int i=0; i<22; i++){
	wk()->addOutput(h_cellTimeAll[g][i]);
	wk()->addOutput(h_cellTimeVsEta[g][i]);
	wk()->addOutput(h_cellTimeVsPhi[g][i]);
	wk()->addOutput(h_cellTimeVsPVz[g][i]);
	wk()->addOutput(h_cellTimeVsf1[g][i]);
	wk()->addOutput(h_cellTimeVsf3[g][i]);
	wk()->addOutput(h_cellTimeVsdEta[g][i]);
	wk()->addOutput(h_cellTimeVsdPhi[g][i]);

	wk()->addOutput( h_bunch_pos[g][i]);
      }
      wk()->addOutput(h_cellTimeVsrEta[g]);
      wk()->addOutput(h_efracDistVsTime[g]);
      wk()->addOutput(h_cellTimeVsdZ0[g]);
      //new
      wk()->addOutput(h_timeVsmu[g]);
      // Depth
      wk()->addOutput(h_timeVsDepth[g]);
      wk()->addOutput(h_timeVsDepth_etaLT08[g]);
      wk()->addOutput(h_timeVsDepth_etaGT08[g]);
      wk()->addOutput(h_timeVsDepthProfile[g]);
      wk()->addOutput(h_cellEnergyVsDepth[g]);
      wk()->addOutput(h_cellFracVsDepth[g]);
      wk()->addOutput(h_mtVsDepth[g]);
      wk()->addOutput(h_etaVsDepth[g]);
      wk()->addOutput(h_phiVsDepth[g]);
      // significance histograms
      wk()->addOutput(h_pointingSig[g]);
      // TOF
      wk()->addOutput(h_timeVsPathDiff[g]);
      wk()->addOutput(h_dtVsPathDiff[g]);
      
      //dphi/deta
      wk()->addOutput(h_dphi[g]);
      wk()->addOutput(h_deta[g]);
      //Pass0
      for(int j=0; j<NRUNS; j++){
//	wk()->addOutput(h_RunAvgFebTime[g][j]);
//	wk()->addOutput(h_RunAvgChannelTime[g][j]);
	wk()->addOutput( h_RunAvgFTTime[g][j]);
      }
      //Pass1
      wk()->addOutput(h_AvgFebTime[g]);
      //Pass2
      if(set_passNumber == 3|| set_passNumber == 2 || set_passNumber == 7 || set_passNumber == 8){
	wk()->addOutput(h_AvgChannelTime[g]);
      }
      //Pass3
    //  for(int s=0; s<22; s++){
    for(int s=0; s<22+7*(1-g); s++){
	wk()->addOutput(h_AvgEnergyTime[g][s]);
      }
    }
  }

  return;
  
}

void MainCalib::addOutTree(){
  // define what braches will go in that tree 
  
  TFile *outputFile = wk()->getOutputFile( outputName );
  tree = new TTree( treename.c_str(), treename.c_str() );
  tree->SetDirectory( outputFile );

  tree->Branch( "dis_from_front", &t_dis_from_front);
  tree->Branch( "bcid", &t_bcid); 
 
  tree->Branch( "f1", &t_f1);
  tree->Branch( "f3", &t_f3);
  tree->Branch( "channel", &t_channel);
  tree->Branch( "slot", &t_slot);
  tree->Branch( "time", &t_time);
  tree->Branch( "time1", &t_time1);
  tree->Branch( "time2", &t_time2);
  tree->Branch( "wgt", &t_wgt);
  tree->Branch( "mu", &t_mu);
  tree->Branch( "eta", &t_caloCluster_eta);
  tree->Branch( "phi", &t_caloCluster_phi);
  tree->Branch( "average_mu", &t_average_mu);   
  tree->Branch( "electronPt", &t_electronPt);
  tree->Branch( "ft", &t_ft);
  tree->Branch( "feb", &t_feb);
  tree->Branch( "gain", &t_gain);
  tree->Branch( "energy", &t_energy);
  tree->Branch( "energy1", &t_energy1);
  tree->Branch( "energy2", &t_energy2);
  tree->Branch( "run", &t_run);
  tree->Branch( "deta", &t_deta);
  tree->Branch( "dphi", &t_dphi);



  wk()->addOutput(tree);
  return;

	// Find index of Run number
}

