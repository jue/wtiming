#include <RunIICalib/MainCalib.h>

bool MainCalib::setORdeco(xAOD::TEvent* event){

  static const ort::inputDecorator_t selectDec("selected");
  static const ort::inputDecorator_t isBJetDec("isBJet");

  // Get electrons
  const xAOD::ElectronContainer* electrons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve(electrons, "Electrons") );
  for( const xAOD::Electron* el : *electrons ){
    selectDec(*el) = '0';
    //Preselect Electrons
    if( getPreselEl(event, *el ) )
      selectDec(*el) = '1';
  }
  
  // Get muons
  const xAOD::MuonContainer* muons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve(muons, "Muons") );
  for( const xAOD::Muon* mu : *muons ){
    selectDec(*mu) = '0';
        
    // Select loose muons
    if( getPreselMu(event, *mu) )
      selectDec(*mu) = '1';
  }
  
  // Get jets
  const xAOD::JetContainer* jets = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve(jets, "AntiKt4EMTopoJets") );

  for( const xAOD::Jet* jet : *jets ){
    selectDec(*jet) = '0';
    isBJetDec(*jet) = '0';
    //Preselect
    if( !getPreselJet( *jet ) )
      continue;
    selectDec(*jet) = '1';
  }

  //Apply OR
  if ( set_debug ) Info("MainCalib::setORdeco()", "About to remove overlaps with ASG tool...");
  EL_RETURN_CHECK( GetName(), m_orToolBox->masterTool->removeOverlaps(electrons, muons, jets, nullptr, nullptr, nullptr) );
  
  return true;

}


// If MC, adjust the event weight
bool MainCalib::setEventWeight(xAOD::TEvent* event){
  
  const xAOD::EventInfo* eventInfo = 0;

  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, ((string)evt_cont_name).c_str() ) );

  // Change weight for MC
  mcweight *= eventInfo->mcEventWeight();
  puweight = m_pileupreweighting->getCombinedWeight( *eventInfo );
  
  wgt = mcweight * puweight;
  
  return true;

}



// Set Lepton vector
bool MainCalib::setLeptonVector( GoodObjects const &goodElectrons,
				 GoodObjects const &goodMuons ){

  if( ngoodMu > 0 ) //Muons veto
    return false;

  if( set_slimW == 1 && ngoodEl != 1 ) //WTiming with electrons
    return false;
  if( set_slimZ == 1  && ngoodEl < 2 ) //ZTiming with electrons
    return false;
  
  // Set lepton lorentz vector to correct lepton
  h_elcutflow->Fill(20, wgt );
  vLepton.SetPtEtaPhiM( (*goodElectrons.at(0)).Pt(),
			(*goodElectrons.at(0)).Eta(),
			(*goodElectrons.at(0)).Phi(),
			(*goodElectrons.at(0)).M() );
  if( !isData ) {
    wgt *= elrecoeff;
    h_elSF -> Fill(elrecoeff);
  }
  
  return true;
  
}


// Set the Neutrino Vector
void MainCalib::setNeutrinoVector(){
  
  // Set lv Mass
  lvmass = 0.0;
  lvmass = sqrt( 2.* vLepton.Pt() * METsum * ( 1. - cos( vLepton.DeltaPhi(vMET) ) ) );
  
  const double M_W = 80385.; 
  
  // Lepton Vector
  double lx = vLepton.Px();
  double ly = vLepton.Py();
  double lz = vLepton.Pz();

  //Neutrino Vector
  double nx = vMET.Px();
  double ny = vMET.Py();
  double nz;

  // Find nz using M_W
  double a =  4.*lx*lx + 4.*ly*ly;
  double b = -4.*M_W*M_W*lz - 8.*lx*nx*lz - 8.*ly*ny*lz;
  double c = 4.*ly*ly*nx*nx+ 4.*lz*lz*nx*nx+ 4.*lx*lx*ny*ny+ 4.*lz*lz*ny*ny- 8.*lx*nx*ly*ny- 4.*lx*nx*M_W*M_W- 4.*ly*ny*M_W*M_W- M_W*M_W*M_W*M_W;

  double discriminant = b*b - 4.*a*c;
  if (discriminant < 0.) discriminant = 0.;

  double nz1 = 0.5*(-b + TMath::Sqrt(discriminant)) / a;
  double nz2 = 0.5*(-b - TMath::Sqrt(discriminant)) / a;
  if (fabs(nz1) < fabs(nz2))
    nz = nz1;
  else
    nz = nz2;

  Neutrino.SetPxPyPzE( nx,
                       ny,
                       nz,
                       TMath::Sqrt(nx*nx + ny*ny + nz*nz));

  return;

}


