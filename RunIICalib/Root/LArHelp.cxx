#include <RunIICalib/MainCalib.h>

int MainCalib::getFtNo(unsigned long int onlId){
  
  if( onlId >> 26 != 14){
    Info( GetName(), Form("Not a valid LAr Online Id:%lx",onlId) );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;  // positive (1=A)/negative(0=C)
  int ft   = ( onlId >> 19 )&0x1F; // feedthrough
  int benc = ( onlId >> 25 )&0x1;  // barrel(0)/endcap(1)

  int bin = ft;

  if(!pn) bin = ft + 32;

  if(benc){
    bin = 64 + ft;
    if(!pn) bin = bin + 25;
  }

  return bin;
}

int MainCalib::getSlotNo(unsigned long int onlId){
  
  if( onlId >> 26 != 14){
    Info( GetName(), "Not a valid LAr Online Id" );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;
  int sl   = ((onlId >> 15 )&0xF)+1;
  int benc = ( onlId >> 25 )&0x1;

  int bin = sl - 11;

  if(!pn) bin = bin + 4;
  
  if( sl == 10 ){
    bin = 8;
    if(!pn) bin = 9;
  }

  if(benc){
    if( sl < 10 || sl > 15){
      Info( GetName(), Form("Warning! Unexpected EMEC Slot: %d",sl));
    }
    bin = 10 + sl -10; // was -1
    if(!pn) bin = bin + 6; // was +15
  }

  return bin;

}
int MainCalib::getSubslotNo(unsigned long int onlId){
	int channel = getChannelNo(onlId);

	unsigned int ch = (unsigned int)channel;
	unsigned int chpos_subslot = std::find ( channelList_for_subslot.begin(),channelList_for_subslot.end(),ch)-channelList_for_subslot.begin();

	int slot  = getSlotNo(onlId);   
	if( chpos_subslot>=0 && chpos_subslot<channelList_for_subslot.size() ){
		switch(slot){
			case 12: slot = 22;break;
			case 13: slot = 23;break;
			case 14: slot = 24;break;
			case 15: slot = 25;break;
			case 19: slot = 26;break;
			case 20: slot = 27;break;
			case 21: slot = 28;
		}
	}
	return slot;
}

int MainCalib::getFebNo(unsigned long int onlId){
 
  // 620 Febs in total
  // EMB:  Sl 10-14 (5) x A/C (2) x 32FT = 320
  // EMEC: Sl 10-15 (6) x A/C (2) X 25FT = 300
  if( onlId >> 26 != 14){
    Info( GetName(), "Not a valid LAr Online Id" );
    return -1;
  }

  int pn   = ( onlId >> 24 )&0x1;
  int ft   = ( onlId >> 19 )&0x1F;
  int sl   = ((onlId >> 15 )&0xF)+1;
  int benc = ( onlId >> 25 )&0x1;

  int bin = (sl - 11)*32 + ft;
  
  if(!pn) bin = bin + 128;

  if( sl == 10){
    bin = 256 + ft;
    if(!pn) bin = bin + 32;
  }

  if(benc){
    bin = 256 + 64 + (sl - 10)*25 + ft; // was -1
    if(!pn) bin = bin + 6*25; // was 375 
  }

  return bin;

}

int MainCalib::getChannelNo(unsigned long int onlId){

  // 620 Febs x 128 ch/feb = 79360 ch
  int fn = getFebNo(onlId);
  int ch = (onlId >> 8)&0x7F;
  int bin = fn * 128 + ch;  
  
  return bin;
}


unsigned long int getOnlId( int benc, int pn, int ft, int slot, int ch){

    unsigned long int onlId = 0;
 
    onlId += ch << 8;
    onlId += (slot - 1)<<15;
    onlId += ft << 19;
    onlId += pn << 24;
    onlId += benc << 25;
    onlId += 14 << 26;

    return onlId;

}


