#include <RunIICalib/MainCalib.h>

//Clean Event
bool MainCalib::passClean(xAOD::TEvent* event){

  const xAOD::EventInfo* eventInfo = 0;

  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, ((string)evt_cont_name).c_str() ) );

  //GRL 
  if( !m_grl->passRunLB(*eventInfo) )
    return false;
    
  //Tile Corruption
  if( eventInfo->errorState(xAOD::EventInfo::EventFlagSubDet::Tile) == xAOD::EventInfo::Error )
    return false;
  
  //LAr noise burst
  if( eventInfo->errorState(xAOD::EventInfo::EventFlagSubDet::LAr) == xAOD::EventInfo::Error )
    return false;
  
  //TTC restart
  if( eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000 )
    return false;
  
  
  return true;
  
}


// Trigger cut
bool MainCalib::passTrigger(){

  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;

  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, ((string)evt_cont_name).c_str() ) );

  int npass = 0;
  TString temp0trig;
  string temp1trig;

  for( int i1 = 0; i1 < ntrigs; i1++ ){
    temp0trig = triggers[i1];
    temp1trig = (string)temp0trig;

    if( m_trigDecTool->isPassed( temp1trig.c_str() ) )
      npass++;
  }
  
  if( npass == 0 )
    return false;
  else
    return true;
  
}


// MET Cut
bool MainCalib::passMET(){

  if ( METsum < set_cut_METsum ) 
    return false;
  else
    return true;

}


//Preselect Muons
bool MainCalib::getPreselMu( xAOD::TEvent* event, const xAOD::Muon &mu ){

  float d0sigBL = 999., z0 = 999., sintheta = 999.;

  if( fabs( mu.eta() ) > set_mu_eta )return false;
  h_mucutflow->Fill( 2 );

  //  
  static const ort::inputAccessor_t calibpt("ORselected");
  double mupt = mu.pt();
  if( calibpt.isAvailable( mu ) ) 
    mupt = mu.auxdecor< float >( "calib_pt" );

  if( mupt < set_mu_pt ) return false;  
  h_mucutflow->Fill( 3 );
  
  //Quality        
  mu.auxdecor< float >( "MuonSpectrometerPt" ) = mupt; //temporarily because they are missing
  mu.auxdecor< float >( "InnerDetectorPt" ) = mupt;    //temporarily because they are missing  

  xAOD::Muon::Quality muonQuality = m_muonSelTool->getQuality( mu );
  if( set_mu_wp < 0 && muonQuality > xAOD::Muon::VeryLoose )return false;
  if( set_mu_wp == 0 && muonQuality > xAOD::Muon::Loose )return false;
  if( set_mu_wp == 1 && muonQuality > xAOD::Muon::Medium )return false;
  if( set_mu_wp == 2 && muonQuality > xAOD::Muon::Tight )return false;
  h_mucutflow->Fill( 4 );

  //Isolation 
  if( set_mu_isolevel > -1 ){
    Root::TAccept isoAccept =  m_muIso->accept( mu );
    if( set_mu_isolevel == 0 && !isoAccept.getCutResult("LooseTrackOnly") ) return false; //the default WP is LooseTrackOnly
    if( set_mu_isolevel == 1 && !isoAccept.getCutResult("Loose") )return false;
    if( set_mu_isolevel == 2 && !isoAccept.getCutResult("Tight") )return false;
    if( set_mu_isolevel == 3 && !isoAccept.getCutResult("Gradient") )return false; 
    if( set_mu_isolevel == 4 && !isoAccept.getCutResult("GradientLoose") )return false;
  }
  h_mucutflow->Fill( 5 );


  //Track Quaity
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, ((string)evt_cont_name).c_str() ) );
  
  const xAOD::TrackParticle * trackPart = 0;
  trackPart = mu.trackParticle(xAOD::Muon::TrackParticleType::InnerDetectorTrackParticle);
  if( !trackPart )
    trackPart = mu.trackParticle(xAOD::Muon::TrackParticleType::Primary);
  if( !trackPart )
    return false;
  if( !m_muTrkSelTool->accept( trackPart ) && set_mu_trkquality == 1 )return false;
  h_mucutflow->Fill( 6 );


  //PV 
  const xAOD::Vertex * primVtx = 0;
  const xAOD::VertexContainer * primVtxCont = 0; 
  EL_RETURN_CHECK( GetName(), event->retrieve( primVtxCont, "PrimaryVertices" ) ); 
  
  for( const xAOD::Vertex * vtx : *primVtxCont ){ //loop on primary vertices 
    if( vtx->vertexType() == xAOD::VxType::PriVtx ) 
      primVtx = vtx;  
  }                         
  
  if( !m_mutrktovxtool->isCompatible( *trackPart, *primVtx) )return false;
  h_mucutflow->Fill( 7 );
  
  d0sigBL = xAOD::TrackingHelpers::d0significance(trackPart,eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
  z0 = trackPart->z0() + trackPart->vz() - primVtx->z();
  sintheta = sin( trackPart->theta() );

  //d0sig/z0 cuts
  if( fabs( d0sigBL ) > set_mu_d0sBL )return false;
  h_mucutflow->Fill( 8 );
  
  if( fabs( z0*sintheta ) > set_mu_z0sintheta )return false;
  h_mucutflow->Fill( 9 );

  
  return true;
  
}



//Preselect Electrons
bool MainCalib::getPreselEl(xAOD::TEvent* event, const xAOD::Electron &el ){

  float d0sigBL = 999., z0 = 999., sintheta = 999.; 

  if( fabs( el.caloCluster()->eta() ) > set_el_eta )return false; 
  h_elcutflow->Fill( 2 );
  
  if( set_el_crack > 0 && fabs( el.caloCluster()->eta() ) > 1.37 && fabs( el.caloCluster()->eta() ) < 1.52 )return false;
  h_elcutflow->Fill( 3 );

  //
  static const ort::inputAccessor_t calibpt("ORselected");
  if( calibpt.isAvailable( el ) && el.auxdecor< float >( "calib_pt" ) < set_el_pt ) return false;
  if( !calibpt.isAvailable( el ) && el.pt() < set_el_pt )return false;
  h_elcutflow->Fill( 4 );

  //
  if( !(el.author(xAOD::EgammaParameters::AuthorElectron)) && !(el.author(xAOD::EgammaParameters::AuthorAmbiguous)) )
    return false;
  h_elcutflow->Fill( 5 );
  
  //
  if( set_el_LH == 1 && !m_LHToolMedium->accept( el ) )
    return false;
  else if( set_el_LH != 1 ){ 
    cerr<<"Not supported LH "<<set_el_LH<<endl;
    exit(6);
  }

  h_elcutflow->Fill( 6 );
  
  //Isolation
  if( set_el_isolevel > -1 ){
    Root::TAccept isoAccept =  m_elIso->accept( el );
    if( isoAccept.getCutResult("LooseTrackOnly") )temp = true;
    if( set_el_isolevel == 0 && !isoAccept.getCutResult("LooseTrackOnly") ) return false;
    if( set_el_isolevel == 1 && !isoAccept.getCutResult("Loose") )return false;
    if( set_el_isolevel == 2 && !isoAccept.getCutResult("Tight") )return false;
    if( set_el_isolevel == 3 && !isoAccept.getCutResult("Gradient") )return false; 
    if( set_el_isolevel == 4 && !isoAccept.getCutResult("GradientLoose") )return false;
  }
  h_elcutflow->Fill( 7 );
  
  //Track Quality 
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, ((string)evt_cont_name).c_str() ) );
  
  const xAOD::TrackParticle * trackPart = 0;
  trackPart = el.trackParticle();
  if( !trackPart )
    return false;
  
  if( !m_elTrkSelTool->accept( trackPart ) && (int)set_el_trkquality == 1 )
    return false;    
  h_elcutflow->Fill( 8 );
  

  //PV 
  const xAOD::Vertex * primVtx = 0;
  const xAOD::VertexContainer * primVtxCont = 0; 
  EL_RETURN_CHECK( GetName(), event->retrieve( primVtxCont, "PrimaryVertices" ) ); 
  
  for( const xAOD::Vertex * vtx : *primVtxCont ){ //loop on primary vertices 
    if( vtx->vertexType() == xAOD::VxType::PriVtx ) 
      primVtx = vtx; 
  }                         
  if( !m_eltrktovxtool->isCompatible(*trackPart, *primVtx) ) return false;
  h_elcutflow->Fill( 9 );
  
  d0sigBL = xAOD::TrackingHelpers::d0significance(trackPart,eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
  z0 = trackPart->z0() + trackPart->vz() - primVtx->z();
  sintheta = sin( trackPart->theta() );
  
  //d0sig/z0 cuts 
  if( fabs( d0sigBL ) > set_el_d0sBL )return false;
  h_elcutflow->Fill( 10 );
  
  if( fabs( z0*sintheta ) > set_el_z0sintheta )return false;
  h_elcutflow->Fill( 11 );
  

  return true;

}



//Preselect Jets 0.4 //includes JVT cut
bool MainCalib::getPreselJet( const xAOD::Jet &jet ){

  if( fabs( jet.eta() ) > set_j_eta )
    return false;
  if( jetCalibPt(jet) < set_j_pt )
    return false;

  float jvt = m_jvtTool->updateJvt(jet);
  if( jet.pt() < 60.*GeV && jet.pt() > 20.*GeV && fabs(jet.eta()) < 2.4 ) {
    if( jvt < 0.59 ) 
      return false;
  }

  
  return true;
  
}


// Cuts for W Timing event selection
// For both W/Z events
bool MainCalib::commonCutsWZ( GoodObjects &goodElectrons,
                              GoodObjects &goodMuons){

  //Trigger
  if( !passTrigger() )
    return false;
  if( set_slimW == 1) h_cutflow_W->Fill( 3, wgt );
  if( set_slimZ == 1) h_cutflow_Z->Fill( 3, wgt );
  if( set_debug ) Info( GetName(), "Passed W-Timing trigger" );

  //Lepton Selection & Set Lepton vector & Lepton Efficencies
  if( !setLeptonVector( goodElectrons, goodMuons) )
    return false;
  if( set_slimW == 1) h_cutflow_W->Fill( 4, wgt );
  if( set_slimZ == 1) h_cutflow_Z->Fill( 4, wgt ); // at least 2 good electrons
  if( set_debug ) Info( GetName(), "Passed W-Timing lepton veto" );

  //Vtx Cut
  PV_z = -999.; PV_mult = -999.;
  if( !vertexTracks() )
    return false;
  if( set_slimW == 1) h_cutflow_W->Fill( 5, wgt );
  if( set_slimZ == 1) h_cutflow_Z->Fill( 5, wgt );
  if( set_debug ) Info( GetName(), "Passed W-Timing vtx cut" );

  return true;
}


// For W event selection
bool MainCalib::specificCutsW(){

  //MET
  xAOD::TEvent* event = wk()->xaodEvent();
  if( !getMET(event) )
    return false;
  if( set_debug ) Info( GetName(), "W-Timing rebuild MET" );

  //MET cut
  if( !passMET() )
    return false;
  if( set_slimW == 1) h_cutflow_W->Fill( 6, wgt );
  if( set_debug ) Info( GetName(), "Passed W-Timing passed MET cut" );

  //Set Neutrino Vector
  setNeutrinoVector();
  if( set_debug ) Info( GetName(), "W-Timing Neutrino Vector Set" );

  //MT cut
  if( lvmass/GeV < 40. || lvmass/GeV > 140. )
    return false;
  h_vMass_WZ->Fill( lvmass/GeV, wgt);
  if( set_slimW == 1) h_cutflow_W->Fill( 7, wgt );
  if( set_debug ) Info( GetName(), "Passed W-Timing Transverse Mass Cut" );

  return true;

}


// For Z event selection
bool MainCalib::specificCutsZ(){

  // Get the event
  xAOD::TEvent* event = wk()->xaodEvent();

  // Get the electrons
  const xAOD::ElectronContainer* electrons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( electrons, ((string)ele_cont_name).c_str() ) );
  xAOD::ElectronContainer::const_iterator el_itr = electrons->begin();
  xAOD::ElectronContainer::const_iterator el_end = electrons->end();

  // Store info about each electron
  TLorentzVector el1, el2;
  int el_num = 0;
  float ch1 = -99;
  float ch2 = -99;

  // Loop over the electrons
  for( ; el_itr != el_end; el_itr++){

    if( (*el_itr)->auxdecor< int >( "forWTiming" ) != 1 )
      continue;

    el_num++;

    if( el_num == 1){
      el1.SetPtEtaPhiM( (*el_itr)->auxdata<float>("calib_pt"),
                        (*el_itr)->auxdata<float>("calib_eta"),
                        (*el_itr)->auxdata<float>("calib_phi"),
                        (*el_itr)->auxdata<float>("calib_m"));
      ch1 = (*el_itr)->auxdata<float>("charge");
    }
    else if( el_num == 2){
      el2.SetPtEtaPhiM( (*el_itr)->auxdata<float>("calib_pt"),
                        (*el_itr)->auxdata<float>("calib_eta"),
                        (*el_itr)->auxdata<float>("calib_phi"),
                        (*el_itr)->auxdata<float>("calib_m"));
      ch2 = (*el_itr)->auxdata<float>("charge");
    }
  }

  // Exactly 2 good electrons!
  if( el_num != 2 )
    return false;
  if( set_slimZ == 1) h_cutflow_Z->Fill( 15, wgt );

  // Invariant mass must be between 68-108 GeV
  if( (el1+el2).M()/GeV < 68 || (el1+el2).M()/GeV > 108 )
    return false;
  if( set_slimZ == 1) h_cutflow_Z->Fill( 6, wgt );
  if( set_debug ) Info( GetName(), "Passed W-Timing Z invariant mass cut" );

  // Must have opposite charges:
  if( fabs(ch1 + ch2) > 0.01 )
    return false;
  if( set_slimZ == 1) h_cutflow_Z->Fill( 7, wgt );
  if( set_debug ) Info( GetName(), "Passed W-timing Z opposite charge cut" );

  // Fill Invariant Mass plot
  //h_invariantMassZee->Fill( (el1+el2).M()/GeV, wgt);
  h_vMass_WZ->Fill( (el1+el2).M()/GeV, wgt);

  return true;
}

