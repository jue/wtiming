#include <RunIICalib/MainCalib.h>

// Save the signal Electrons
bool MainCalib::getSignalElectrons(xAOD::TEvent* event, GoodObjects &goodElectrons ){

  // Reset Counter
  ngoodEl = 0;

  // Retrieve Electrons
  const xAOD::ElectronContainer* electrons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( electrons, ((string)ele_cont_name).c_str() ) );

  static const ort::inputAccessor_t selectAcc("ORselected");
  
  if( set_debug )Info( GetName(), "Processing electrons %d", electrons->size() );

  // Loop over electrons
  for( auto el : *electrons ){
       
    el->auxdecor< int >( "forWTiming" ) = 0;
    el->auxdecor< int >( "forMETRebuild" ) = 0;
    
    if( set_debug)Info( GetName(), "Processing electron : before cuts ");

    //Overlap removal
    if( selectAcc.isAvailable(*el) && !selectAcc(*el) )
      continue;
    if( set_debug)Info( GetName(), "Processing electron : after OR ");

    
    //pt/eta/phi (calibrated)
    double pt  = elCalibPt( *el );
    double eta = el->eta();
    double phi = el->phi();
  
    el->auxdecor< float >( "calib_pt" )  = pt; 

    
    //Preselect Electrons
    if( !getPreselEl(event, *el ) )
      continue;

    if( el->auxdata<float>("maxEcell_energy") < 5.*GeV )
      continue;

    
    //Store variables
    el->auxdecor< float >( "calib_eta" ) = eta;
    el->auxdecor< float >( "calib_phi" ) = phi;
    el->auxdecor< float >( "calib_m" )   = 0.511;

    el->auxdecor< int >( "forMETRebuild" ) = 1;
    el->auxdecor< int >( "forWTiming" ) = 1;
    

    // Store lorentz vector 
    goodElectrons.push_back( new TLorentzVector() );
    goodElectrons.at( ngoodEl )->SetPtEtaPhiM( pt, eta, phi, 0.511 );
    ngoodEl++;
    
    if( set_debug )
      Info( GetName(), "Signal Electron %f pt, %f eta, %f phi", pt / GeV, eta, phi ); 
    
  }
  
  return true;
  
}


// Save the signal Muons
bool MainCalib::getSignalMuons(xAOD::TStore* store, xAOD::TEvent* event, GoodObjects &goodMuons ){
  
  // Reset Counter
  ngoodMu = 0;

  //Retrieve muons
  const xAOD::MuonContainer* muons = 0;

  EL_RETURN_CHECK( GetName(), event->retrieve( muons, ((string)muons_cont_name).c_str() ) );

  // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
  xAOD::MuonContainer::iterator mu_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator mu_end = (muons_shallowCopy.first)->end();
  
  static const ort::inputAccessor_t selectAcc("ORselected");

  // Loop over muons
  for( ; mu_itr != mu_end; mu_itr++ ){ 

    //Initialize variable for METrebuild
    ( *mu_itr )->auxdecor< int >( "forMETRebuild" ) = 0;
    
    //Overlap removal
    if( selectAcc.isAvailable(**mu_itr) && !selectAcc(**mu_itr) )
      continue;


    //pt/eta/phi (Calibrated)
    double pt  = muCalibPt( **mu_itr );
    double eta = (*mu_itr)->eta();
    double phi = (*mu_itr)->phi();
    
    ( *mu_itr )->auxdecor< float >( "calib_pt" ) = pt;

    //Select signal muons
    if( !getPreselMu( event, **mu_itr ) )
      continue;

    //keep info for MET rebuilding
    ( *mu_itr )->auxdecor< int >( "forMETRebuild" ) = 1;
    
    
    //Store the vector
    goodMuons.push_back( new TLorentzVector() );
    goodMuons.at( ngoodMu )->SetPtEtaPhiM( pt,eta, phi, 105.7);
    ngoodMu++;
    
  }

  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  
  return true;

}


// Save the MET
bool MainCalib::getMET(xAOD::TEvent* event){
  
  // Reset METsum
  METsum = 0, METphi = -9999.;
  
    
  //Get all other containers 
  const xAOD::ElectronContainer* electrons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve(electrons, ((string)ele_cont_name).c_str() ) );
  
  const xAOD::MuonContainer* muons = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( muons, ((string)muons_cont_name).c_str() ) );
  
  const xAOD::JetContainer* jets = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( jets, ((string)jet_cont_name).c_str() ) );
  
  //retrieve the MET association map
  const xAOD::MissingETAssociationMap* metMap = 0;
  const xAOD::MissingETContainer* inputMETCoreContainer = 0;
  EL_RETURN_CHECK( GetName(), event->retrieve( metMap, "METAssoc_AntiKt4EMTopo" ) ); 
  EL_RETURN_CHECK( GetName(), event->retrieve( inputMETCoreContainer, "MET_Core_AntiKt4EMTopo" ) ); 
  metMap->resetObjSelectionFlags();
  
  //retrieve new MET container
  xAOD::MissingETContainer* newMetContainer = new xAOD::MissingETContainer();
  xAOD::MissingETAuxContainer* newMetAuxContainer = new xAOD::MissingETAuxContainer();
  newMetContainer->setStore(newMetAuxContainer);
  
  if( electrons ){ //These are calibrated electrons (MC) that have been previously flagged as "forMETRebuild"
    ConstDataVector<xAOD::ElectronContainer> metEle(SG::VIEW_ELEMENTS);
    if( set_debug ) Info( GetName(), "Rebuilding MET: electrons.");
    xAOD::Electron* tempel = 0;
    for( const auto& ele : *electrons ){
      if( ele->auxdecor<int>("forMETRebuild") == 1 ){
	if ( isData ) metEle.push_back(ele);
	else {
	  tempel = 0;
	  CP::CorrectionCode c = m_elCalibrationAndSmearingTool->correctedCopy( *ele, tempel);
	  if( c != CP::CorrectionCode::Ok ) Error(GetName(), "Failed to apply electron calibration. Exiting.");
	  metEle.push_back(tempel); 
	}
	//if( set_debug )Info( GetName(), "Electron for METRebuild : eta = %f, phi = %f, pT = %f, calib eta = %f, phi = %f, pT = %f",ele->caloCluster()->eta(), ele->caloCluster()->phi(), ele->caloCluster()->pt() / GeV, tempel->eta(), tempel->phi(), tempel->pt() / GeV ); 
      }
    }
    EL_RETURN_CHECK( GetName(), m_metMaker->rebuildMET("RefEle", xAOD::Type::Electron, newMetContainer,  metEle.asDataVector(), metMap, MissingETBase::UsageHandler::PhysicsObject));
    delete tempel;
  }
  
  if( muons ){ //Calibrated muons previously flagged as "forMETRebuild"
    if( set_debug ) 
      Info( GetName(), "Rebuilding MET: muons.");

    ConstDataVector<xAOD::MuonContainer> metMuo(SG::VIEW_ELEMENTS);
    xAOD::Muon* tempmu = 0;
    
    for( const auto& muo : *muons ){
      if( muo->auxdecor<int>("forMETRebuild") == 1 ){
	if( set_debug ) 
	  Info( GetName(), "Muons for METRebuild : eta = %f, pT = %f",muo->eta(), muo->pt() / GeV );
	if( isData ) 
	  metMuo.push_back(muo);
	else {
	  tempmu = 0;
	  CP::CorrectionCode c = m_muonCalibrationAndSmearingTool->correctedCopy( *muo, tempmu );
	  if( c != CP::CorrectionCode::Ok ) Error(GetName(), "Failed to apply muon calibration. Exiting.");
	  metMuo.push_back(tempmu);
	}
      }
    }
    EL_RETURN_CHECK(GetName(), m_metMaker->rebuildMET("Muons", xAOD::Type::Muon, newMetContainer,  metMuo.asDataVector(), metMap, MissingETBase::UsageHandler::PhysicsObject)); 
    delete tempmu;
  }
  
  //Jets Shallow Copy
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > calibJets = xAOD::shallowCopyContainer( *jets );
  xAOD::setOriginalObjectLink(*jets,*calibJets.first);
  
  xAOD::JetContainer::iterator calibJets_itr = (calibJets.first)->begin();
  xAOD::JetContainer::iterator calibJets_end = (calibJets.first)->end();
  
  for( ; calibJets_itr != calibJets_end; ++calibJets_itr ) {
    if( !isData )
      EL_RETURN_CHECK(GetName(), m_jetCalibration->applyCalibration(**calibJets_itr) );
    float newjvt = m_jvtTool->updateJvt(**calibJets_itr);
    (*calibJets_itr)->auxdecor<float>("Jvt") = newjvt; //needed for MET
    if( set_debug ) cout<<"calibrating jets ..... "<<endl;
  }
  
  
  if( calibJets.first ){ //all jets are accounted for - no preselection!
    //Using calo-based MET
    //Current recommendation for soft term is TST and JVT cut on
    bool doJVTCut = true;
    if( set_debug ) cout<<"METrebuild jets ..... "<<endl;
    EL_RETURN_CHECK(GetName(), m_metMaker->rebuildJetMET("RefJet", "PVSoftTrk", newMetContainer, jets, inputMETCoreContainer, metMap, doJVTCut));
  }//fixme
  
  delete calibJets.first;
  delete calibJets.second;
  
  EL_RETURN_CHECK( GetName(), m_metMaker->buildMETSum("FinalTrk" , newMetContainer, MissingETBase::Source::Track ));
  
  
  METsum = (*newMetContainer)["FinalTrk"]->met();
  METphi = (*newMetContainer)["FinalTrk"]->phi();
  
  delete newMetContainer;
  delete newMetAuxContainer;
  
  // Save lorentz vector
  vMET.SetPtEtaPhiM( METsum, 0.0, METphi, 0.0);
  if( set_debug ) Info( GetName(), "MET sum = %f, phi = %f", METsum / GeV, METphi );
  
  return true;

}


