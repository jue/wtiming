#include <RunIICalib/MainCalib.h>
bool MainCalib::makeWTimingTrees( const xAOD::Electron &el, double t_corr ){  

	xAOD::TEvent* event = wk()->xaodEvent();
	const xAOD::EventInfo* eventInfo = 0;

	EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );
	// Get Event Info
	unsigned int rN = eventInfo->auxdata<unsigned int>("runNumber");
	int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();


	float etas2 = el.auxdata<float>("caloCluster_etas2");
	float phis2 = el.auxdata<float>("caloCluster_phis2");

	float cellX = el.auxdata<float>("maxEcell_x");
	float cellY = el.auxdata<float>("maxEcell_y");
	float cellZ = el.auxdata<float>("maxEcell_z");
	float phi   = el.auxdata<float>("caloCluster_phi");
	double cellR = sqrt( cellX*cellX + cellY*cellY + cellZ*cellZ);

	double cellphi = TMath::ATan2( cellY, cellX );
	double celltheta = TMath::ACos( cellZ/cellR );
	double celleta = -TMath::Log( TMath::Tan(celltheta/2) );

	double deta = (celleta - etas2)/0.025; // 0.025 units
	double dphi = cellphi - phis2;
	if( dphi > TMath::Pi() )
		dphi = dphi - 2*TMath::Pi();
	else if ( dphi < -TMath::Pi() )
		dphi = dphi + 2*TMath::Pi();

	dphi = dphi/0.0245; // 0.0245 units
	if( dphi > 0  || dphi < -2 )
		Info( GetName(), Form("DPHI UNEXPECTED: %f, CellPhi: %f, Phis2: %f",dphi,cellphi,phis2) );

	// Get OnlId/Feb/Slot/Channel
	unsigned long int onlId = el.auxdata<unsigned long int>("maxEcell_onlId");
	onlId = onlId >> 32; //Is stored as 64bit, last 32 bits area zero
	t_slot = getSlotNo(onlId);

	//GetBranchCrosing info
	
//	t_bcid = eventInfo->auxdata<unsigned int>("bcid");
//	int gapbtrain = m_bcTool->gapBeforeTrain( t_bcid );
//	if(gapbtrain>500)
//		t_dis_from_front=m_bcTool->distanceFromFront( t_bcid );
//	else
//	        t_dis_from_front=m_bcTool->distanceFromFront( t_bcid)*2+m_bcTool->distanceFromTail( t_bcid)+m_bcTool->gapBeforeTrain( t_bcid);	
//		
//		Info( GetName(), Form("distance %d, fromtai: %d,gap: %d", m_bcTool->distanceFromFront( t_bcid ),m_bcTool->distanceFromTail( t_bcid), m_bcTool->gapBeforeTrain( t_bcid)));
	
	t_feb = getFebNo(onlId);
	t_channel = getChannelNo(onlId);
	t_ft = getFtNo(onlId);
	t_energy= el.auxdata<float>("maxEcell_energy") / GeV;
	t_f1 = el.auxdata<float>("f1");
	t_f3 = el.auxdata<float>("f3");
	t_run=pos;
	t_wgt=wgt;
	t_time=t_corr;
	// Gain
	t_gain = el.auxdata<int>("maxEcell_gain");
	t_dphi=dphi;
	t_deta=deta;
	t_caloCluster_eta=el.auxdata<float>("caloCluster_eta");
	t_caloCluster_phi=el.auxdata<float>("caloCluster_phi");
	t_electronPt=el.auxdata<float>("caloCluster_e") / GeV;

	t_mu= eventInfo->auxdata<float>("actualInteractionsPerCrossing");
	t_average_mu=eventInfo->auxdata<float>("averageInteractionsPerCrossing");              
}


bool MainCalib::makeWTimingPlots( const xAOD::Electron &el, double t_corr ){


	// Get Event Info
	xAOD::TEvent* event = wk()->xaodEvent();
	const xAOD::EventInfo* eventInfo = 0;
	EL_RETURN_CHECK( GetName(), event->retrieve( eventInfo, "EventInfo" ) );

	// Find index of Run number
	unsigned int rN = eventInfo->auxdata<unsigned int>("runNumber");
	int pos = std::find( runNumberList.begin(), runNumberList.end(), rN) - runNumberList.begin();
	//pile up
	//	double mu= eventInfo->auxdata<float>("averageInteractionsPerCrossing");
//	double mu= eventInfo->auxdata<float>("actualInteractionsPerCrossing");
//	if(mu<0) mu=-mu/0.140569;


	// Gain
	int gain = el.auxdata<int>("maxEcell_gain");

	// Get OnlId/Feb/Slot/Channel
	unsigned long int onlId = el.auxdata<unsigned long int>("maxEcell_onlId");
	onlId = onlId >> 32; //Is stored as 64bit, last 32 bits area zero
	int slot = getSlotNo(onlId);
	int feb = getFebNo(onlId);
	int channel = getChannelNo(onlId);
	int ft = getFtNo(onlId);
	t_bcid = eventInfo->auxdata<unsigned int>("bcid");
	int gapbtrain = m_bcTool->gapBeforeTrain( t_bcid );
	if(gapbtrain>500)
		t_dis_from_front=m_bcTool->distanceFromFront( t_bcid );
	else
	        t_dis_from_front=m_bcTool->distanceFromFront( t_bcid)*2+m_bcTool->distanceFromTail( t_bcid)+m_bcTool->gapBeforeTrain( t_bcid);	
		
		Info( GetName(), Form("distance %d, fromtai: %d,gap: %d", m_bcTool->distanceFromFront( t_bcid ),m_bcTool->distanceFromTail( t_bcid), m_bcTool->gapBeforeTrain( t_bcid)));

	///put channel with large energy H/M gain threshold to other subslots.

	unsigned int ch = (unsigned int)channel;
	if(!dochannel){
		if(doclean&&(channel ==24814||channel==57264))
			cout<<"we don't want the bad channel"<<endl;
		else{
			//W mass
			if( set_passNumber < 10)
				h_transverseMassWenu[gain]->Fill( el.auxdata<float>("lvmass")/GeV, wgt );

			// Energy
			h_maxEcellEnergy[gain]->Fill( el.auxdata<float>("maxEcell_energy") / GeV, wgt );

			// Zvertex
			/*
			   float cl_e     = el.caloCluster()->e() / TMath::CosH(el.caloCluster()->eta());
			   float etas2    = el.caloCluster()->etaBE(2);
			   float etas1    = el.caloCluster()->etaBE(1);
			   float phis2    = el.caloCluster()->phiBE(2);
			   float cl_theta = 2.*TMath::CosH( TMath::Exp( -1. * el.caloCluster()->eta() ) );
			   float beamPosX = eventInfo->beamPosX();
			   float beamPosY = eventInfo->beamPosY();
			   std::pair<float,float> verexz = m_vertexTool->getCaloPointing( cl_e, etas1, etas2, phis2, cl_theta, beamPosX, beamPosY );
			   float zvertex  = verexz.first;
			   float errz     = verexz.second;
			   */
			float PV_z    = el.auxdata<float>("PV_z");
			float PV_mult = el.auxdata<float>("PV_mult");
			float zvertex = el.auxdata<float>("zvertex");
			float errz    = el.auxdata<float>("errz");

		//	h_electronZvertex[gain]->Fill( zvertex - PV_z, wgt );
		//	h_dZVsTrackMult[gain]->Fill( PV_mult, zvertex - PV_z, wgt );
		//	h_absDZVsTrackMult[gain]->Fill( PV_mult, fabs(zvertex - PV_z), wgt );

			// Calculate dphi/deta
			float etas2 = el.auxdata<float>("caloCluster_etas2");
			float phis2 = el.auxdata<float>("caloCluster_phis2");

			float cellX = el.auxdata<float>("maxEcell_x");
			float cellY = el.auxdata<float>("maxEcell_y");
			float cellZ = el.auxdata<float>("maxEcell_z");

			double cellR = sqrt( cellX*cellX + cellY*cellY + cellZ*cellZ);
			double cellphi = TMath::ATan2( cellY, cellX );
			double celltheta = TMath::ACos( cellZ/cellR );
			double celleta = -TMath::Log( TMath::Tan(celltheta/2) );

			double deta = (celleta - etas2)/0.025; // 0.025 units
			double dphi = cellphi - phis2;
			if( dphi > TMath::Pi() )
				dphi = dphi - 2*TMath::Pi();
			else if ( dphi < -TMath::Pi() )
				dphi = dphi + 2*TMath::Pi();

			dphi = dphi/0.0245; // 0.0245 units
			if( dphi > 2  || dphi < -2 )
				Info( GetName(), Form("DPHI UNEXPECTED: %f, CellPhi: %f, Phis2: %f",dphi,cellphi,phis2) );

			//dphi/deta
		//	h_dphi[gain]->Fill( dphi, wgt);
		//	h_deta[gain]->Fill( deta, wgt);

			// Timing
			if(slot < 22 && slot >= 0){
				h_cellTimeAll[gain][slot]->Fill( t_corr, wgt );
				h_bunch_pos[gain][slot]->Fill(t_dis_from_front, t_corr,wgt);
				// after correct for avg cell time, only include t<4ns
				if( set_passNumber < 3 || fabs( t_corr) < 4. ){
			//		h_cellTimeVsEta[gain][slot]->Fill( el.auxdata<float>("caloCluster_eta"), t_corr, wgt );
			//		h_cellTimeVsPhi[gain][slot]->Fill( el.auxdata<float>("caloCluster_phi"), t_corr, wgt );
					h_cellTimeVsf1[gain][slot]->Fill( el.auxdata<float>("f1"), t_corr, wgt );
					h_cellTimeVsf3[gain][slot]->Fill( el.auxdata<float>("f3"), t_corr, wgt );
					h_cellTimeVsdEta[gain][slot]->Fill( deta, t_corr, wgt );
					h_cellTimeVsdPhi[gain][slot]->Fill( dphi, t_corr, wgt );
				}
			}
			else{
				Info(GetName(),Form("Slot is inccorect: %d" ,slot));
			}
		//	h_cellTimeVsrEta[gain]->Fill( el.auxdata<float>("Reta"), t_corr, wgt );
		//	h_efracDistVsTime[gain]->Fill( el.auxdata<float>("maxEcell_energy") / el.auxdata<float>("caloCluster_e"), t_corr, wgt);
		//	h_cellTimeVsPVz[gain][slot]->Fill( PV_z, t_corr, wgt );
		//	h_cellTimeVsdZ0[gain]->Fill( zvertex - PV_z, t_corr, wgt );

		//	// Depth
		//	//float phi = el.caloCluster()->phi();
		//	//float depth = m_depthTool->getCorrectedShowerDepthEM2( etas2, phi, isData );
		//	float depth = el.auxdata<float>("depth");

		//	h_timeVsDepth[gain]->Fill( depth, t_corr, wgt );
		//	if( el.auxdata<float>("caloCluster_eta") < 0.8 )
		//		h_timeVsDepth_etaLT08[gain]->Fill( depth, t_corr, wgt );
		//	else
		//		h_timeVsDepth_etaGT08[gain]->Fill( depth, t_corr, wgt );
		//	h_timeVsDepthProfile[gain]->Fill( depth, t_corr, wgt );
		//	h_cellEnergyVsDepth[gain]->Fill( depth, el.auxdata<float>("maxEcell_energy") / GeV, wgt );
		//	h_cellFracVsDepth[gain]->Fill( depth, el.auxdata<float>("maxEcell_energy") / el.auxdata<float>("caloCluster_e"), wgt );
		//	if( set_passNumber < 8)
		//		h_mtVsDepth[gain]->Fill( depth, lvmass / GeV, wgt );
		//	h_etaVsDepth[gain]->Fill( depth, el.auxdata<float>("caloCluster_eta"), wgt );
		//	h_phiVsDepth[gain]->Fill( depth, el.auxdata<float>("caloCluster_phi"), wgt );

		//	// Significance
		//	h_pointingSig[gain]->Fill( zvertex / errz, wgt );

		//	h_electronPt[gain]->Fill( el.auxdata<float>("caloCluster_e") / GeV, wgt ) ;
		//	if( set_passNumber < 8)
		//		h_met[gain]->Fill( el.auxdata<float>("METsum") / GeV, wgt );

			if( set_debug )
				Info( GetName(), "In the plotting macro : FEB %d, corrected t %f, Pos %d, RunNumberList size %d, Gain %d", feb, t_corr, pos, (int)runNumberList.size(), gain );

			if( !h_RunAvgFebTime[gain][pos] || !h_RunAvgFTTime[gain][pos] ){
				cerr<<"Histograms have not been defined ... exiting"<<endl;
				exit(6);
			}

			// Pass 0

			//if( pos < runNumberList.size())  h_RunAvgFebTime[gain][pos]->Fill( feb, t_corr, wgt);
			if( pos < runNumberList.size())  h_RunAvgFTTime[gain][pos]->Fill( ft, t_corr, wgt);

			// Pass 1
			h_AvgFebTime[gain]->Fill(feb, t_corr, wgt);

			// Pass 2
			if( set_passNumber == 2||set_passNumber == 3 || (set_passNumber == 7 && fabs(t_corr) < 4.)||set_passNumber == 8 ){
				h_AvgChannelTime[gain]->Fill(channel, t_corr, wgt);
			}
			// Pass 3
			if( set_passNumber < 5 || fabs(t_corr) < 4.||set_passNumber==10){
				int slot;
				if(gain==0)
				slot = getSubslotNo(onlId);
				else{
				slot = getSlotNo(onlId); 
				}
				h_AvgEnergyTime[gain][slot]->Fill( el.auxdata<float>("maxEcell_energy") / GeV, t_corr, wgt);
			}
		}//end of exclude bad channel
	}//end of channel plot
	else{
		unsigned int chpos = std::find ( channelList.begin(),channelList.end(),ch)-channelList.begin();

		if(pos < runNumberList.size() && chpos>=0 && chpos<channelList.size()){
			cout<<"findid"<<channel<<"\t"<<onlId<<endl;

			h_RunAvgChannelTime[gain][pos]->Fill( chpos, t_corr, wgt);
			h_AvgChannelTime[gain]->Fill(chpos, t_corr, wgt);
			h_maxEcellEnergy[gain]->Fill( el.auxdata<float>("maxEcell_energy") / GeV, wgt );
			h_transverseMassWenu[gain]->Fill( el.auxdata<float>("lvmass")/GeV, wgt );
		}
	}
	return true;

}












