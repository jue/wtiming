#ifndef RunIICalib_MainCalib_H
#define RunIICalib_MainCalib_H

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>
#include "EventLoopGrid/PrunDriver.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/Message.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"

#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/TauJet.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETComposition.h"

#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"

#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODBTagging/BTagging.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"

#include "xAODCore/ShallowCopy.h"
#include "xAODCore/ShallowAuxContainer.h"

//CutBookkeepers
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

//GRL
#include "GoodRunsLists/GoodRunsListSelectionTool.h"

//Pile up
#include "PileupReweighting/PileupReweightingTool.h"

//Jets
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"

//Trigger
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"

//Muon Calibration and Smearing Tools 
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

//Muon Selection Tool
#include "MuonSelectorTools/MuonSelectionTool.h"

//ID Track Selection Tool
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

//Track Vertex Association Tool 
#include "TrackVertexAssociationTool/LooseTrackVertexAssociationTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

//Muon Isolation Selection Tool
#include "IsolationSelection/IsolationSelectionTool.h"

//LH Electron Identification
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"

//Electron Calibration and Smearing Tool
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

//Electron Efficiency
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"

//JetVertexTagger
#include "JetMomentTools/JetVertexTaggerTool.h"

//Jet Substructure
#include "JetSubStructureUtils/BosonTag.h"
#include "JetSubStructureUtils/BoostedXbbTag.h"

//MET
//#include "METUtilities/METRebuilder.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/METSystematicsTool.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticleContainer.h"

//Vertex Container
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODTracking/Vertex.h"

//Systematics
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicVariation.h" 
#include "PATInterfaces/SystematicsUtil.h"

//Shower Depth
#include "IsolationCorrections/ShowerDepthTool.h"

//Photon Vertex 
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"

//Truth
#include "xAODTruth/TruthEventContainer.h"

//Overlap Removal
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalTool.h"
#include "AssociationUtils/ORToolBox.h"

//bucnh crossing
#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"   
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"

// Output xAOD
#include "EventLoop/OutputStream.h"
#include <tuple>


#include <iostream>
#include <fstream>
#include <TH1F.h>
#include <TH2F.h>
#include <TProfile.h>
#include "TFile.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include <TTree.h>

using namespace std;

//Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP ) \
  do {					\
    if( ! EXP.isSuccess() ) {		\
      Error( CONTEXT,			     \
	     XAOD_MESSAGE( "Failed to execute: %s" ),	\
	     #EXP );					\
      return EL::StatusCode::FAILURE;			\
    }							\
  } while( false )

class MainCalib : public EL::Algorithm{
  
  
 public:
  
  //Externally Set Variables
  bool savetree=false;
  int set_debug; 
  bool doCalibrate;
  bool doSyst;
  bool set_blinding;
  int set_analysis;
  int set_preselect;
  int set_qcd;


  bool isData;
  bool doclean=true;//!
  bool dochannel=false;//!

  double set_mu_eta;
  double set_mu_pt;
  int set_mu_wp;
  int set_mu_isolevel;
  int set_mu_trkquality;
  double set_mu_d0sBL;
  double set_mu_z0sintheta;
  
  double set_el_eta;
  int set_el_crack;
  double set_el_pt;
  int set_el_LH;
  int set_el_isolevel;
  int set_el_trkquality;
  double set_el_d0sBL;
  double set_el_z0sintheta;
  
  double set_cut_METsum;

  double set_j_pt ;
  double set_j_eta;
  

  // WTiming
  int set_passNumber;
  int set_slimW;
  int set_slimZ;

  string triggers[20];
  int ntrigs;

  
  //Duplication Check
  std::set< std::pair< unsigned int, unsigned int > > m_seenEvents; //!
  typedef std::tuple<uint32_t, unsigned long long, uint32_t> RunEventMet; //!
  std::set<RunEventMet> m_setRunEventMet; //!

  //
  string RootCorePath; //!

  int m_eventCounter; //!
  int ngoodEl; //!
  int ngoodMu; //!

  double wgt; //!
  double mcweight; //!
  double puweight; //!
  double sumOfWeights; //!

  double deta; //!
  double dphi; //!
  double METsum; //!
  double METphi; //!

  double lvmass; //!
  
  TString muons_cont_name;//!
  TString ele_cont_name; //!
  TString evt_cont_name; //!
  TString jet_cont_name; //!

  TLorentzVector vMET; //!
  TLorentzVector vLepton; //!
  TLorentzVector Neutrino;//!

  double elrecoeff; //!
  bool temp; //!
  

  // Keep list of good objects for each event
  typedef std::vector<TLorentzVector*> GoodObjects;

  const float GeV = 1000.; // Gev
  const bool applyOR = true; //! 

  
  //W-Timing Variables
  float PV_mult, PV_z;
  std::vector<int> channelList; //! 
  std::vector<int> channelList_for_subslot; //! 
  std::vector<int> iovNumberList; //!
  std::vector<int> runNumberList; //!
  std::vector< std::vector<std::string> > pass0CorrectionsH; //!
  std::vector< std::vector<std::string> > pass0CorrectionsM; //!
  std::vector< std::vector<std::string> > pass0CorrectionsL; //!
  std::vector< std::string > pass1CorrectionsH; //!
  std::vector< std::string > pass1CorrectionsM; //!
  std::vector< std::string > pass1CorrectionsL; //!
  std::vector< std::vector<std::string> > pass2CorrectionsH; //!
  std::vector< std::vector<std::string> > pass2CorrectionsM; //!
  std::vector< std::vector<std::string> > pass2CorrectionsL; //!
  std::vector< std::vector<std::string> > pass3CorrectionsH; //!
  std::vector< std::vector<std::string> > pass3CorrectionsM; //!
  std::vector< std::vector<std::string> > pass3CorrectionsL; //!
  std::vector< std::vector<std::string> > pass4CorrectionsH; //!
  std::vector< std::vector<std::string> > pass4CorrectionsM; //!
  std::vector< std::vector<std::string> > pass4CorrectionsL; //!
  std::vector< std::vector<std::string> > pass5CorrectionsH; //!
  std::vector< std::vector<std::string> > pass5CorrectionsM; //!
  std::vector< std::vector<std::string> > pass5CorrectionsL; //!
  std::vector< std::vector<std::string> > pass6CorrectionsH; //!
  std::vector< std::vector<std::string> > pass6CorrectionsM; //!
  std::vector< std::vector<std::string> > pass6CorrectionsL; //!
  std::vector< std::string > pass7CorrectionsH; //!
  std::vector< std::string > pass7CorrectionsM; //!
  std::vector< std::string > pass7CorrectionsL; //!
  std::vector< std::vector<std::string> > passCorrections_notusedH; //!
  std::vector< std::vector<std::string> > passCorrections_notusedM; //!
  std::vector< std::vector<std::string> > passCorrections_notusedL; //!
  std::vector< std::vector<std::string> > pass8CorrectionsH; //!
  std::vector< std::vector<std::string> > pass8CorrectionsM; //!
  std::vector< std::vector<std::string> > pass8CorrectionsL; //!
  std::vector< std::string > lowGainShift; //!
  static const int NRUNS = 150; //!
 // static const int NRUNS = 1; //!
  static const int Nch = 1;

 public:
  
  //CP tools
  GoodRunsListSelectionTool *m_grl;                                     //!
  CP::PileupReweightingTool *m_pileupreweighting;                       //!
  JetCleaningTool* m_jetcleaningTool;                                   //!
  JetVertexTaggerTool* m_jvtTool;                                       //!
  TrigConf::xAODConfigTool *m_trigConfigTool;                           //!
  Trig::TrigDecisionTool *m_trigDecTool;                                //!
  CP::MuonSelectionTool *m_muonSelTool;                                 //!
  InDet::InDetTrackSelectionTool *m_muTrkSelTool;                       //!
  CP::LooseTrackVertexAssociationTool *m_mutrktovxtool;                 //!
  CP::IsolationSelectionTool *m_muIso;                                  //!
  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; //!
  AsgElectronLikelihoodTool *m_LHToolMedium;                            //!
  CP::EgammaCalibrationAndSmearingTool *m_elCalibrationAndSmearingTool; //!
  AsgElectronEfficiencyCorrectionTool *m_elEffMediumLHTool;             //!
  AsgElectronEfficiencyCorrectionTool *m_effToolReco;                   //!
  AsgElectronEfficiencyCorrectionTool *m_trigToolMediumLH;              //!
  CP::IsolationSelectionTool *m_elIso;                                  //!
  CP::LooseTrackVertexAssociationTool *m_eltrktovxtool;                 //!
  InDet::InDetTrackSelectionTool *m_elTrkSelTool;                       //!
  met::METMaker* m_metMaker;                                            //!
  //OverlapRemovalTool *m_overlapRemovalTool;                           //!
  CP::ShowerDepthTool *m_depthTool;                                     //!
  ORUtils::ToolBox * m_orToolBox;                                       //!
  JetCalibrationTool *m_jetCalibration;                                 //!
  Trig::IBunchCrossingTool* m_bcTool; //!
  //Trig::WebBunchCrossingTool* bcTool;//! 

  //
  TH1F *h_test; //!
  TH1F *h_cutflow; //!
  TH1F *h_wcutflow; //!
  TH1F *h_elcutflow; //!
  TH1F *h_mucutflow; //!
  TH1F *h_elwcutflow; //!
  TH1F *h_muwcutflow; //!
  TH1F *h_fjcutflow; //!

  TH1F *h_eventCount; //!
  TH1F *h_wgt; //!
  TH1F *h_mcweight; //!
  TH1F *h_muSF; //!
  TH1F *h_elSF; //!

  TH1F *h_el_passOR; //!
  TH1F *h_mu_passOR; //!
  TH1F *h_jet_passOR; //!

  TH1F *h_mud0s; //!
  TH1F *h_muz0; //!
  TH1F *h_eld0s; //!
  TH1F *h_elz0; //!

  //Precission WTiming
  TH1F *h_cutflow_W;                 //!
  TH1F *h_cutflow_Z;                 //!
  TH1F *h_vMass_WZ;                  //!
  TH1F *h_invariantMassZee;          //!
  TH2F *h_t1vst2Zee;                 //!
  TH1F *h_t1plust2Zee;               //!
  TH1F *h_t1minust2Zee;              //!
  TH1F *h_t1Zee;                     //!
  TH1F *h_t2Zee;                     //!
  TH1F *h_transverseMassWenu[2];     //!
  TH1F *h_numElectrons[2];           //!
  TH1F *h_electronPt[2];             //!
  TH1F *h_maxEcellEnergy[2];         //!
  TH1F *h_met[2];                    //!
  TH1F *h_electronZvertex[2];        //!
  TH2F *h_dZVsTrackMult[2];          //!
  TH2F *h_absDZVsTrackMult[2];       //!
  TH1F *h_cellTimeAll[2][22];        //!
  TH2F *h_cellTimeVsEta[2][22];      //!
  TH2F *h_cellTimeVsPhi[2][22];      //!
  TH2F *h_cellTimeVsrEta[2];         //!
  TH2F *h_cellTimeVsf1[2][22];       //!
  TH2F *h_cellTimeVsf3[2][22];       //!
  TH2F *h_cellTimeVsdEta[2][22];     //!
  TH2F *h_cellTimeVsdPhi[2][22];     //!
  TH2F *h_efracDistVsTime[2];        //!
  TH2F *h_cellTimeVsPVz[2][22];      //!
  TH2F *h_cellTimeVsdZ0[2];          //!
  TH2F *h_timeVsDepth[2];            //!
  TH2F *h_timeVsDepth_etaLT08[2];    //!
  TH2F *h_timeVsDepth_etaGT08[2];    //!
  TProfile *h_timeVsDepthProfile[2]; //!
  TH1F *h_dphi[2];                   //!
  TH1F *h_deta[2];                   //!
  TH2F *h_cellEnergyVsDepth[2];      //!
  TH2F *h_cellFracVsDepth[2];        //!
  TH2F *h_mtVsDepth[2];              //!
  TH2F *h_etaVsDepth[2];             //!
  TH2F *h_phiVsDepth[2];             //!
  TH1F *h_pointingSig[2];            //!
  TH2F *h_timeVsPathDiff[2];         //!
  TH2F *h_dtVsPathDiff[2];           //!
  TH2F *h_AvgFebTime[2];             //!
  TH2F *h_RunAvgFTTime[2][NRUNS];    //!
  TH2F *h_RunAvgChannelTime[3][NRUNS];      //!    
  TH2F *h_RunAvgFebTime[2][NRUNS];   //!
  TH2F *h_AvgChannelTime[2];         //!
  TH2F *h_AvgEnergyTime[2][22+7];      //!

  TH2F *h_bunch_pos[3][22]; //!
  TH2F *h_timeVsmu[3];       //!
  
  // this is a standard constructor
  MainCalib ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  void bookHist ();
  void addHistOutput ();
  void addOutTree ();


  
  bool setEventWeight (xAOD::TEvent* event);
  bool setORdeco (xAOD::TEvent* event);
  bool passClean (xAOD::TEvent* event);
  bool getSignalElectrons (xAOD::TEvent* event, GoodObjects &goodElectrons );
  bool getSignalMuons (xAOD::TStore* store, xAOD::TEvent* event, GoodObjects &goodMuons );
  bool getMET (xAOD::TEvent* event);
  bool setLeptonVector (GoodObjects const &goodElectrons,
			GoodObjects const &goodMuons);
  void setNeutrinoVector ();
  bool passTrigger ();
  bool passMET ();
  
  bool getPreselEl (xAOD::TEvent* event, const xAOD::Electron &el );
  bool getPreselMu (xAOD::TEvent* event, const xAOD::Muon &mu );
  bool getPreselJet (const xAOD::Jet &jet );
  double muCalibPt ( xAOD::Muon &mu );
  double elCalibPt (const xAOD::Electron &el );
  double elRecoEff (const xAOD::Electron &el );

  bool CPToolsInit (xAOD::TEvent* event);
  void CPToolsFin ();

  double jetCalibPt (const xAOD::Jet &jet );

  //W-Timing
  bool doWZSlim          (GoodObjects &goodElectrons,
                          GoodObjects &goodMuons);
  bool doWTiming         ();
  bool commonCutsWZ      (GoodObjects &goodElectrons,
                          GoodObjects &goodMuons);
  bool specificCutsW     ();
  bool specificCutsZ     ();
  bool saveAuxContainers ();
  bool vertexTracks      ();
  bool makeWTimingPlots  (const xAOD::Electron &el, double t_corr );
  bool makeWTimingTrees  (const xAOD::Electron &el, double t_corr );   

  double tofCorrection   (const xAOD::Electron &el);
  double applyPass0Corr  (int ft, double t_corr, int gain);
  double applyPass1Corr  (int feb, double t_corr, int gain);
  double applyPass2Corr  (int ch,  double t_corr, int gain);
  double applyPass3Corr  (double en, int slot, double t_corr, int gain);
  double applyPass4Corr  (double dphi, double deta, int slot, double t_corr, int gain);
  double applyPass5Corr  (float f1, float f3, int slot, double t_corr, int gain);
  double applyPass7Corr  (int ch, double t_corr, int gain);
  double applyPass6Corr  (int dist, int slot, double t_corr, int gain);
  double applyPassCorr_notused  (double mu, double t_corr, int gain);
  void setChannelList    ();
  void setRunNumberList  ();
  int  getFtNo           (unsigned long int onlId);
  int  getSlotNo         (unsigned long int onlId);
  int  getSubslotNo 	 (unsigned long int onlId);
  int  getFebNo          (unsigned long int onlId);
  int  getChannelNo      (unsigned long int onlId);
  unsigned long int getOnlId (int benc, int pn, int ft, int slot, int ch);


  // this is needed to distribute the algorithm to the workers
  ClassDef(MainCalib, 1);
  //tree related variables
  string treename;
  std::string outputName;
  TTree *tree; //!
  int t_ft;
  int t_feb;
  int t_gain;
  int t_run;
  int t_channel;
  int t_slot;
  int t_wgt;
  double t_depth ;
  double t_energy;
  double t_f1;
  double t_f3;
  double t_time;
  unsigned int t_bcid;
  double t_mu;
  int t_dis_from_front;
  double t_average_mu;
  double t_caloCluster_eta;
  double t_caloCluster_phi;
  double t_electronPt;

  double t_deta;
  double t_dphi;
//for z
  double t_time1;
  double t_time2;
  double t_energy1;
  double t_energy2;
 

 protected:
  CP::PhotonVertexSelectionTool * m_vertexTool; //!
  
};

#endif
