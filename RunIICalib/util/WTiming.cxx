//________________________________________________________________
//
// WTiming.cxx
//
//  Use:
//  1) Sending W/Z slmming DAOD jobs to Condor on xenia
//
//  Configuration:
//  ==In WTiming.cxx
//    -- Look at the flags/ preselection cuts and change as
//       needed
//    -- Update the trigger list as needed, select the
//       correct array
//    -- Set either:
//         >> set_slimW = 1;
//       OR (not both)
//         >> set_slimZ = 1;
//  
//  Running:
//  ==Go to ArcondCalib directory
//    -- Follow detailed instructions for running on xenia
//       in the README
//    -- GetDatasetPaths.sh, DistributeDatasets.sh
//       (make sure compiled) MakeTar.sh, update
//       run.py, then:
//         $ ./run.py
//
//  Output: 
//  ==Inside the ArcondCalib direcory
//    -- Follow detailed instructions in README
//         $ ./CheckAndAddOutputs.sh
//
//  Post Run:
//  ==Go to the RootOutputs/ directory
//    -- Merge all slimmed xAODs into a single file, ex:
//        $ xAODMerge IOV1_W_slim.root data*.root
//    -- Move the merged, slimmed xAOD into a safe place, ex:
//        $ cp IOV1_W_slim.root /data/users/rc2809/data/2016/
//  ==This will now be the input file for each pass, to be run
//    with the LocalWTiming macro, make sure to edit the path in
//    RunIICalib/util/LocalWTiming.cxx to point to this slimmed
//    xAOD
//
//_______________________________________________________________


// Standard
#include <memory>

// Root
#include <TChain.h>
#include <TStopwatch.h>

// Sample Handler
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"

// Event Loop
#include "EventLoop/Job.h"
#include "EventLoop/OutputStream.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopAlgs/NTupleSvc.h"

// xAOD
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// Header
#include "RunIICalib/MainCalib.h"

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

//
// WTiming
//
// Arguments to pass at command line
//  1: dataset name
//  2: data / MC
//  3: xenia node
//  4: how many events to run on (default: all)
//___________________________________
int main( int argc, char* argv[] ) {

  //Measure the execution speed 
  TStopwatch timer;

  // Which Analysis
  static const char* APP_NAME = "WTiming";
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  
  //____________________________
  // Set the command line options
  //

  // Read arguments passed
  string datasetname = (string)argv[1];         // which data to run on
  string hostname    = (string)argv[3];         // which node to run on
  string xenia       = hostname.substr( 5, 2 ); // extract node number
  TString path = "inlists/" + xenia + "/" + datasetname + ".txt";
  // Max events (default all)
  int maxEvents = ( argc > 4 ? std::stoi((string)argv[4]) : -1 );
  // Calibrate
  bool calib = true;

  
  //__________________________
  // Add data sample to run on
  //

  // Create TChain, reading Collection Tree
  SH::SampleHandler sh; 
  TChain ch( "CollectionTree" );
  
  //Open the inputs
  ifstream inputfile ( path );
  string line;

  // Read input files paths
  if( inputfile.is_open() ){
    while( inputfile.good() ){
      getline( inputfile,line );
      if( line != "" ){
        cout<<"Adding Sample "<<line<<" on node "<<xenia<<endl;
        ch.Add( line.c_str() );
        if( line.find("CxAOD") < line.length() ) calib = false;
      }
    }
    inputfile.close();
  }
  else {
    cout<<"No file "<<path<<"\nTHE JOB IS SUCCESSFUL"<<endl;
    return 0;
  }

  // Add tree to sample handler
  sh.add( SH::makeFromTChain( Form("%s%s",datasetname.c_str(),xenia.c_str()), ch ) );
  sh.setMetaString( "nc_tree", "CollectionTree" );
  sh.print();

  // Verify settings
  cout<<"isData option: "<<argv[2]<<endl;
  cout<<"doCalibrate option: "<<calib<<endl;
  cout<<"Running on "<<maxEvents<<" events."<<endl;
  
  // Give sample handler to job to run on, set max events
  EL::Job job;
  job.sampleHandler( sh ); 
  job.options()->setDouble( EL::Job::optMaxEvents, maxEvents );
  EL::OutputStream output( "MVAtree" );
  job.outputAdd(output);

  
  //_____________________________________
  // Call the analysis, set configuration
  //
  
  // Call the analysis
  MainCalib *alg = new MainCalib;

  // Ntuple for MVA
  EL::NTupleSvc *ntuple = new EL::NTupleSvc("MVAtree");
  job.algsAdd(ntuple);

  // Set Configuration
  // Analysis flags
  alg->set_analysis  = 2;        // W/ZTiming: 2
  alg->set_debug     = 1;        // More output
  alg->isData        = ( atoi(argv[2]) == 1 ? true : false ); // Looking at data, not MC 
  alg->doCalibrate   = calib;    // True for W/Z slimming
  alg->doSyst        = false;    // No systematics
  alg->set_preselect = 0;        // No Preselect
  alg->set_qcd       = 0;        // No QCD
  
  // Electron Preselection cuts 
  alg->set_el_eta        = 2.47;     // Central electrons
  alg->set_el_crack      = 1;        // Exclude crack region
  alg->set_el_pt         = 20000.;   // pt > 20 GeV
  alg->set_el_LH         = 1;        // LH = 1 (Medium); 0: loose; 2: tight
  alg->set_el_isolevel   = 0;        // Isolation = 0 (LooseTrackOnly)
  alg->set_el_trkquality = 1.;       // Require pass track quality
  alg->set_el_d0sBL      = 5.;       // d0 significance (transverse impact parameter)
  alg->set_el_z0sintheta = 0.5;      // z0*sin(theta) (longitudinal impact parameter)

  // MET cut for W sel
  alg->set_cut_METsum    = 25000.;   // MET > 25 GeV
  
  // Muon Preselection Cuts
  alg->set_mu_eta        = 2.5;      // Central Muons
  alg->set_mu_pt         = 25000.;   // pt > 25 GeV
  alg->set_mu_wp         = 1;        // Quality working point = 1 (Medium)
  alg->set_mu_isolevel   = 2;        // Isolation = 2 (Tight)
  alg->set_mu_trkquality = 1;        // Require track quality
  alg->set_mu_d0sBL      = 3.;       // d0 significance (transverse impact parameter)
  alg->set_mu_z0sintheta = 0.5;      // z0*sin(theta) (longitudinal impact parameter)

  // jet Preselection cuts
  alg->set_j_pt        = 20000.;     // pt  > 20 GeV
  alg->set_j_eta       = 2.5;        // eta < 2.5
  
  // Flags for WTiming
  alg->set_slimW      = 1;           // set only 1 of W/Z slims
  alg->set_slimZ      = 0;           // and other to 0
  alg->set_passNumber = 0;           // not used, WTiming is for slimming

  
  //________________________
  // Define Triggers
  //  
  // see Egamma(+MET) lowest unprescaled triggers:
  // https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled#Egamma_MET
  
  //=== W triggers (single electron)
  // 2015
  string temptriggs_W2015[] = {"HLT_e24_lhmedium_L1EM18VH", 
                               "HLT_e24_lhmedium_iloose_L1EM18VH",
                               "HLT_2e12_lhloose_L12EM10VH",
                               "HLT_e17_lhloose_2e9_lhloose",
                               "HLT_e24_lhmedium_L1EM20VH", 
                               "HLT_e60_lhmedium", 
                               "HLT_e120_lhloose" };
  // 2016
  string temptriggs_W2016[] = {"HLT_e24_lhtight_nod0_ivarloose", 
                               "HLT_e26_lhtight_nod0_ivarloose", 
                               "HLT_e60_lhmedium_nod0",
                               "HLT_e60_medium", 
                               "HLT_e140_lhloose_nod0", 
                               "HLT_e300_etcut"}; 
  // === Z triggers (single+multielectron)
  // 2015
  string temptriggs_Z2015[] = {"HLT_2e12_lhloose_L12EM10VH",
                               "HLT_e17_lhloose_2e9_lhloose",
                               "HLT_e24_lhmedium_L1EM20VH", 
                               "HLT_e60_lhmedium", 
                               "HLT_e120_lhloose" };
  // 2016
  string temptriggs_Z2016[] = {"HLT_e24_lhtight_nod0_ivarloose",
                               "HLT_e26_lhtight_nod0_ivarloose",
                               "HLT_e60_lhmedium_nod0",
                               "HLT_e60_medium",    
                               "HLT_e140_lhloose_nod0",
                               "HLT_e300_etcut",
                               "HLT_2e15_lhvloose_nod0_L12EM13VH",
                               "HLT_2e17_lhvloose_nod0",
                               "HLT_e17_lhloose_nod0_2e9_lhloose_nod0"};
  // Choose which trigger to use here
  // must select from arrays above
  alg->ntrigs =         ARRAY_SIZE(temptriggs_W2016);
  for( int i1 = 0; i1 < ARRAY_SIZE(temptriggs_W2016); i1++ )
               alg->triggers[i1] = temptriggs_W2016[i1];

 
  //_________________________
  // Submit the job
  //
  
  // Add the analysis to the job
  alg->outputName = "MVAtree";
  job.algsAdd( alg );
  // Select the driver
  EL::DirectDriver driver;
  // Submit
  driver.submit( job, "outputDir" );

  //Print stats
  timer.Stop();
  cout<<"\n\n\nProcessed in "<<timer.RealTime()<<" real, "<<timer.CpuTime()<<" CPU seconds\n\n\n"<<endl;

  return 0;
}


