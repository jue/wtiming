#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListEOS.h"
#include <TSystem.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>
#include "EventLoopGrid/PrunDriver.h"
#include <sys/stat.h>

#include "xAODRootAccess/tools/ReturnCheck.h"  
#include "RunIICalib/MainCalib.h" 
#include "TEnv.h"

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

int main( int argc, char* argv[] ) {
	std::string samplePath = ".";
	std::string inputTag = "";
	std::string outputTag = "";
	std::string submitDir = "submitDir";
	std::string userName = "";
	std::vector< std::string> options;
	for(int ii=1; ii < argc; ++ii){
		options.push_back( argv[ii] );
	}

	if (argc > 1 && options.at(0).compare("-h") == 0) {
		std::cout << std::endl
			<< " Job submission" << std::endl
			<< std::endl
			<< " Optional arguments:" << std::endl
			<< "  -h               Prints this menu" << std::endl
			<< "  -inFile          Path to a folder, root file, or text file" << std::endl
			<< "  -inputTag        A wildcarded file name to run on" << std::endl
			<< "  -outputTag       Version string to be appended to job name" << std::endl
			<< "  -submitDir       Name of output directory" << std::endl
			<< "  -userName      Path to user file" << std::endl
			<< std::endl;
		exit(1);
	}

	int iArg = 0;
	while(iArg < argc-1) {
		if (options.at(iArg).compare("-h") == 0) {
			++iArg;
		}else if (options.at(iArg).compare("-inFile") == 0) {
			char tmpChar = options.at(iArg+1)[0];
			if (iArg+1 == argc-1 || tmpChar == '-' ) {
				std::cout << " -inFile should be followed by a file or folder" << std::endl;
				return 1;
			} else {
				samplePath = options.at(iArg+1);
				iArg += 2;
			}
		} else if (options.at(iArg).compare("-inputTag") == 0) {
			char tmpChar = options.at(iArg+1)[0];
			if (iArg+1 == argc-1 || tmpChar == '-' ) {
				std::cout << " -inputTag is a wildcarded file name to run on" << std::endl;
				return 1;
			} else {
				inputTag = options.at(iArg+1);
				iArg += 2;
			}
		} else if (options.at(iArg).compare("-outputTag") == 0) {
			char tmpChar = options.at(iArg+1)[0];
			if (iArg+1 == argc-1 || tmpChar == '-' ) {
				std::cout << " -outputTag should be followed by a job version string" << std::endl;
				return 1;
			} else {
				outputTag = options.at(iArg+1);
				iArg += 2;
			}
		} else if (options.at(iArg).compare("-submitDir") == 0) {
			char tmpChar = options.at(iArg+1)[0];
			if (iArg+1 == argc-1 || tmpChar == '-' ) {
				std::cout << " -submitDir should be followed by a folder name" << std::endl;
				return 1;
			} else {
				submitDir = options.at(iArg+1);
				iArg += 2;
			}
		} else if (options.at(iArg).compare("-userName") == 0) {
			char tmpChar = options.at(iArg+1)[0];
			if (iArg+1 == argc-1 || tmpChar == '-' ) {
				std::cout << " -userName should be followed by a user file" << std::endl;
				return 1;
			} else {
				userName = options.at(iArg+1);
				iArg += 2;
			}
		}else{
			std::cout << "Couldn't understand argument " << options.at(iArg) << std::endl;
			return 1;
		}
	}//

	// Set up the job for xAOD access:
	static const char* APP_NAME = "makeslim";
	RETURN_CHECK( APP_NAME, xAOD::Init() );
	SH::SampleHandler sh;
 
	std::string containerName;
	std::vector< std::string > outputContainerNames; //for grid only
	//if grid job
	bool f_grid = false;

	  //Check if input is a directory or a file
	struct stat buf;
	stat(samplePath.c_str(), &buf);
	if( samplePath.substr(0, 4).find("eos") != std::string::npos){
		SH::DiskListEOS list(samplePath.c_str());
		if (inputTag.size() > 0){
			SH::scanDir (sh, list, inputTag); //Run on all files within dir containing inputTag
		}else{
			SH::scanDir (sh, list); //Run on all files within dir
		}
		std::cout << "Running on EOS directory " << samplePath << std::endl;
	}else if( S_ISDIR(buf.st_mode) ){ //if it is a local directory
		SH::DiskListLocal list (samplePath);
		if (inputTag.size() > 0){
			SH::scanDir (sh, list, inputTag); //Run on all files within dir containing inputTag
		}else{
			SH::scanDir (sh, list); //Run on all files within dir
		}
		std::cout << "Running Locally on directory  " << samplePath << std::endl;
	}
	else {  //if it is a file
		if( samplePath.substr( samplePath.size()-4 ).find(".txt") != std::string::npos){ //It is a text file of samples
			cout<<"here"<<endl;
			if( samplePath.find("grid") != std::string::npos ) //It is samples for the grid
				f_grid = true;

			std::ifstream inFile( samplePath );
			while(std::getline(inFile, containerName) ){
				if (containerName.size() > 1 && containerName.find("#") != 0 ){
					std::cout << "Adding container " << containerName << std::endl;
					if(f_grid){
						SH::scanRucio( sh, containerName);
						//Add output container name to file of containers
						//            //follows grid format: "user."+userName+".%in:name[1]%.%in:name[2]%.%in:name[3]%"+outputTag
						int namePosition = 0;
						namePosition = containerName.find_first_of(".", namePosition)+1;
						namePosition = containerName.find_first_of(".", namePosition)+1;
						namePosition = containerName.find_first_of(".", namePosition)+1;
						outputContainerNames.push_back( ("user."+userName+"."+containerName.substr(0, namePosition)+outputTag+"/") );
						//	cout<<outputContainerNames.c_str()<<endl;
					}else{
					//Get full path of file
					char fullPath[300];
					realpath( containerName.c_str(), fullPath );
					string thisPath = fullPath;
					string fileName = thisPath.substr(containerName.find_last_of("/")+1);
					thisPath = thisPath.substr(0, thisPath.find_last_of("/"));
					thisPath = thisPath.substr(0, thisPath.find_last_of("/"));
					std::cout << "path and filename are " << thisPath << " and " << fileName << std::endl;

					SH::DiskListLocal list (thisPath);
					SH::scanDir (sh, list, fileName); // specifying one particular file for testing
					
					}
				}
			}
		}
	}


	// Set the name of the input TTree. It's always "CollectionTree"
	// for xAOD files.
	sh.setMetaString( "nc_tree", "CollectionTree" );
	sh.setMetaString("nc_grid_filter", "*");   //Data files on grid to not end in .root


	// Print what we found:
	sh.print();

	// Create an EventLoop job:
	EL::Job job;
	job.sampleHandler( sh );
//	job.options()->setDouble (EL::Job::optMaxEvents, 500);

	// To automatically delete submitDir
	job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);
	job.options()->setDouble (EL::Job::optCacheSize, 10*1024*1024);
	job.options()->setDouble (EL::Job::optCacheLearnEntries, 20);


	// define an output and an ntuple associated to that output
	EL::OutputStream output  ("myOutput");
	job.outputAdd (output);
	// Add our analysis to the job
	MainCalib *alg = new MainCalib;
	EL::NTupleSvc *ntuple = new EL::NTupleSvc ("myOutput");
	job.algsAdd (ntuple);

	alg->outputName = "myOutput"; // give the name of the output to our algorithm
	
alg->set_debug     = 0;        // More output
alg->isData        = true;     // fixme hardcoded for now


// Electron Preselection cuts 
alg->set_el_eta        = 2.47;     // Central electrons
alg->set_el_crack      = 1;        // Exclude crack region
alg->set_el_pt         = 20000.;   // pt > 20 GeV
alg->set_el_LH         = 1;        // LH = 1 (Medium); 0: loose; 2: tight
alg->set_el_isolevel   = 0;        // Isolation = 0 (LooseTrackOnly)
alg->set_el_trkquality = 1.;       // Require pass track quality
alg->set_el_d0sBL      = 5.;       // d0 significance (transverse impact parameter)
alg->set_el_z0sintheta = 0.5;      // z0*sin(theta) (longitudinal impact parameter)

// MET cut for W sel
alg->set_cut_METsum    = 25000.;   // MET > 25 GeV

// Muon Preselection Cuts
alg->set_mu_eta        = 2.5;      // Central Muons
alg->set_mu_pt         = 25000.;   // pt > 25 GeV
alg->set_mu_wp         = 1;        // Quality working point = 1 (Medium)
alg->set_mu_isolevel   = 2;        // Isolation = 2 (Tight)
alg->set_mu_trkquality = 1;        // Require track quality
alg->set_mu_d0sBL      = 3.;       // d0 significance (transverse impact parameter)
alg->set_mu_z0sintheta = 0.5;      // z0*sin(theta) (longitudinal impact parameter)

// jet Preselection cuts
alg->set_j_pt        = 20000.;     // pt  > 20 GeV
alg->set_j_eta       = 2.5;        // eta < 2.5

// Flags for WTiming
alg->set_slimW      = 1;           // set only 1 of W/Z slims
alg->set_slimZ      = 0;           // and other to 0
alg->set_passNumber = 0;           // not used, WTiming is for slimming


//________________________
// Define Triggers
//  
// see Egamma(+MET) lowest unprescaled triggers:
// https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled#Egamma_MET

//=== W triggers (single electron)
// 2015
string temptriggs_W2015[] = {"HLT_e24_lhmedium_L1EM18VH", 
	"HLT_e24_lhmedium_iloose_L1EM18VH",
	"HLT_2e12_lhloose_L12EM10VH",
	"HLT_e17_lhloose_2e9_lhloose",
	"HLT_e24_lhmedium_L1EM20VH", 
	"HLT_e60_lhmedium", 
	"HLT_e120_lhloose" };
// 2016
string temptriggs_W2016[] = {"HLT_e24_lhtight_nod0_ivarloose", 
	"HLT_e26_lhtight_nod0_ivarloose", 
	"HLT_e60_lhmedium_nod0",
	"HLT_e60_medium", 
	"HLT_e140_lhloose_nod0", 
	"HLT_e300_etcut"}; 
// === Z triggers (single+multielectron)
// 2015
string temptriggs_Z2015[] = {"HLT_2e12_lhloose_L12EM10VH",
	"HLT_e17_lhloose_2e9_lhloose",
	"HLT_e24_lhmedium_L1EM20VH", 
	"HLT_e60_lhmedium", 
	"HLT_e120_lhloose" };
// 2016
string temptriggs_Z2016[] = {"HLT_e24_lhtight_nod0_ivarloose",
	"HLT_e26_lhtight_nod0_ivarloose",
	"HLT_e60_lhmedium_nod0",
	"HLT_e60_medium",    
	"HLT_e140_lhloose_nod0",
	"HLT_e300_etcut",
	"HLT_2e15_lhvloose_nod0_L12EM13VH",
	"HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH",
	"HLT_2e17_lhvloose_nod0",
	"HLT_e17_lhloose_nod0_2e9_lhloose_nod0"};
// Choose which trigger to use here
// must select from arrays above
alg->ntrigs =         ARRAY_SIZE(temptriggs_W2016);
for( int i1 = 0; i1 < ARRAY_SIZE(temptriggs_W2016); i1++ )
alg->triggers[i1] = temptriggs_W2016[i1];





	job.algsAdd( alg );


	std::string outputName;
	 if( outputTag.size() > 0)
		     outputTag = "."+outputTag+"/";
	   else
		       outputTag = "/";

	// if running on GRID, make sure no two runs have the same output name. tag as necessary

	if(f_grid) // follow GRID naming conventions
		outputName = "user."+userName+".%in:name[1]%.%in:name[2]%"+outputTag;
	else
		outputName = "%in:name%"+outputTag;
	cout<<outputName<<endl;
	  ///// For grid, save list of ouput containers to the submission directory /////
	  //

	// Run the job using the local/direct driver:
	if(!f_grid){
		EL::DirectDriver driver;
		driver.submit( job, submitDir );
	}
	else{
		EL::PrunDriver driver;
		driver.options()->setString("nc_outputSampleName", outputName);
		driver.options()->setDouble(EL::Job::optGridMergeOutput, 1); //run merging jobs for all samples before downloading (recommended) 
		driver.options()->setDouble(EL::Job::optGridNFilesPerJob, 2);
		// driver.options()->setDouble(EL::Job::optGridMemory, 10240); //10 GB
		//
		driver.submitOnly(job, submitDir); // submitOnly runs job without opening monitoring loop
	}

	  std::ofstream fileList((submitDir+"/outputContainers.txt"), std::ios_base::out);
	    for( unsigned int iCont=0; iCont < outputContainerNames.size(); ++iCont){
	        fileList << outputContainerNames.at(iCont)+"\n";
	      }
	      fileList.close();
	return 0;
}
