//____________________________________________________________
//   
//  LocalWTiming.cxx
//
//  Use:
//  1) Testing code on single samples
//  2) Running W/Z Timing passes on already slimmed xAODs
//  
//  Configurations:
//  ==In LocalTiming.cxx:
//    -- Change the input data file to an example you want
//       to test your code on. For slimming run on the DAODs and
//       for 2) run on the slimmed xAODs
//         >> ch.Add("/path/to/data/example.root");
//    -- For 2) you can set the pass number when you
//       execute the macro (first argument)
//    -- You can set the max events when you execute the
//       macro (second argument, default all events)
//    -- Look at the flags/ preselection cuts and change as
//       needed
//    -- Update the trigger list as needed, select the 
//       correct array
//    -- For slimming the DAODs, set either:
//          >> set_slimW = 1; 
//       OR (not both)
//          >> set_slimZ = 1;
//    -- For 2) set both to 0
//          >> set_slimW = 0;
//          >> set_slimZ = 0;
//  ==In Run/config
//    -- For 2), make sure RunList.txt has an ordered list of the
//       of the runs in your dataset
//    -- If you are doing Pass 5, make sure corrections for
//       passes 0-4 are in here to be read
//  ==In RunIICalib/RunIICalib/MainCalib.h
//    -- Make sure you set NRUNS equal to the number of runs in your
//       dataset
//
//  Running:
//    -- To run on the specified sample, all events, 
//       slimming (on DAOD, provided set_slimW/Z is set),
//       In the Run/ directory type:
//         $ LocalWTiming
//    -- If running on already slimmed W/Z xAODs, specify
//       the pass number (make sure set_slimW/Z = 0)
//         $ LocalWTiming N
//       where the pass number N = 0-7 for WTiming 
//       and N=8 for ZTiming
//
//  Output: 
//    -- For W/Z slimming, the output xAOD is located in:
//         outputDir/data-WTiming-slimmed/local.root
//    -- For passes, the output root file with histrograms
//       is located in:
//         outputDir/hist-local.root
//
//_________________________________________________________________

// Standard
#include <memory>

// Root
#include <TChain.h>
#include <TStopwatch.h>

// SampleHandler
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"

// Event Loop
#include "EventLoop/Job.h"
#include "EventLoop/OutputStream.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopAlgs/NTupleSvc.h"

// xAOD
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

// Header
#include "RunIICalib/MainCalib.h"

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

//
// Local WTiming
// 
// Optional arguments to pass at command line
//    1: Pass number
//    2: maxEvents
//    3: File Name
//_________________________________
int main( int argc, char* argv[] ) {

  // Measure the execution speed 
  TStopwatch timer;

  // Macro name
  static const char* APP_NAME = "LocalWTiming_local";
  RETURN_CHECK( APP_NAME, xAOD::Init() );
  
  
  //________________________
  // Set the command line options
  //

  // Set the pass number (else 0)
  int pass = ( argc > 1 ? std::stoi((string)argv[1]) : 0 );
  // Max events (else all events)
  int maxEvents = ( argc > 2 ? std::stoi((string)argv[2]) : -1 );
  // Set the filename
  string fName = ( argc > 3 ? (string)argv[3] : "setlocal" );
  string hostname    = (string)argv[4];         // which node to run on   
  string xenia       = hostname.substr( 5, 2 ); // extract node number 
  
  //__________________________
  // Add data sample to run on
  //

  // Create TChain, reading Collection Tree
  SH::SampleHandler sh; 
  TChain ch( "CollectionTree" );
  
  //Open the inputs
  string line;
/*
  // Read input files paths
  if( inputfile.is_open() ){
    while( inputfile.good() ){
      getline( inputfile,line );
      if( line != "" ){
        cout<<"Adding Sample "<<line<<" on node "<<xenia<<endl;
        ch.Add( line.c_str() );
      }
    }
    inputfile.close();
  }
  else {
    cout<<"No file "<<path<<"\nTHE JOB IS SUCCESSFUL"<<endl;
    return 0;
  }
  */
// fName="user.kalliopi.data16_13TeV.00310969.physics_Main.merge.DAOD_EGAM5.f758_m1710_p2840.16D20a_WTiming_slimmed.root";
// xenia="03";
line="/xrootdfs/atlas/dq2/rucio/user/jue/68/71/user.jue.10907731._000004.WTiming_slimmed.root";
line="/xrootdfs/atlas/dq2/rucio/user/jue/0d/6a/user.jue.11489488._000005.WTiming_slimmed.root";

ch.Add(line.c_str() );
  // Add tree to sample handler
  //sh.add( SH::makeFromTChain( Form("%s%s",fName.c_str(),xenia.c_str()), ch ) );
sh.add( SH::makeFromTChain("local", ch ) );     
  sh.setMetaString( "nc_tree", "CollectionTree" );
  sh.print();

  

  // Give sample handler to job to run on, set max events
  EL::Job job;
  job.sampleHandler( sh ); 
  job.options()->setDouble( EL::Job::optMaxEvents, maxEvents );
  EL::OutputStream output("local");
  job.outputAdd(output);

  
  //_____________________________________
  // Call the analysis, set configuration
  //

  // Call the analysis 
  MainCalib *alg = new MainCalib;

  // Set Configuration
  // Analysis flags
//  alg->set_analysis = 2;        // W/ZTiming: 2 
  alg->set_debug = 0;           // More output
  alg->isData = true;           // Looking at data, not MC
//  alg->doCalibrate = true;      // True for W/Z slimming 
//  alg->doSyst = false;          // No systematics
 // alg->set_preselect = 0;       // No Preselect
//  alg->set_qcd = 0;             // No QCD

  // Electron Preselection cuts
  alg->set_el_eta = 2.47;       // Central electrons
  alg->set_el_crack = 1;        // Exclude crack region
  alg->set_el_pt = 20000.;      // pt > 20 GeV
  alg->set_el_LH = 1;           // LH = 1 (Medium); 0: loose; 2: tight
  alg->set_el_isolevel = 0;     // Isolation = 0 (LooseTrackOnly)
  alg->set_el_trkquality = 1.;  // Require pass track quality
  alg->set_el_d0sBL = 5.;       // d0 significance (transverse impact parameter)
  alg->set_el_z0sintheta = 0.5; // z0*sin(theta) (longitudinal impact parameter)
  
  // MET cut for W sel
  alg->set_cut_METsum = 25000.; // MET > 25 GeV
 
  // Mu Preselection cuts
  alg->set_mu_eta = 2.5;        // Central Muons
  alg->set_mu_pt = 25000.;      // pt > 25 GeV
  alg->set_mu_wp = 1;           // Quality working point = 1 (Medium)
  alg->set_mu_isolevel = 2;     // Isolation = 2 (Tight)
  alg->set_mu_trkquality = 1;   // Require track quality
  alg->set_mu_d0sBL = 3.;       // d0 significance (transverse impact parameter)
  alg->set_mu_z0sintheta = 0.5; // z0*sin(theta) (longitudinal impact parameter)
  
  // Preselection for jets
  alg->set_j_pt = 20000.;       // pt  > 20 GeV
  alg->set_j_eta = 2.5;         // eta < 2.5
  alg->treename = "tree";
  // Flags for WTiming
  // Set whether to slim xAOD or run passes
  alg->set_slimW = 0;  // set to 0 to run passes
  alg->set_slimZ = 0;  // set to 0 to run passes
  // Sets pass number if specified
  if( pass ) alg->set_passNumber = pass;
  else       alg->set_passNumber = 0;

  
  //________________________
  // Define Triggers
  // 
  // see Egamma(+MET) lowest unprescaled triggers:
  // https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled#Egamma_MET
  
  //=== W triggers (single electron)
  // 2015
  string temptriggs_W2015[] = {"HLT_e24_lhmedium_L1EM18VH", 
                               "HLT_e24_lhmedium_iloose_L1EM18VH",
                               "HLT_2e12_lhloose_L12EM10VH",
                               "HLT_e17_lhloose_2e9_lhloose",
                               "HLT_e24_lhmedium_L1EM20VH", 
                               "HLT_e60_lhmedium", 
                               "HLT_e120_lhloose" };
  // 2016
  string temptriggs_W2016[] = {"HLT_e24_lhtight_nod0_ivarloose", 
                               "HLT_e26_lhtight_nod0_ivarloose", 
                               "HLT_e60_lhmedium_nod0",
                               "HLT_e60_medium", 
                               "HLT_e140_lhloose_nod0", 
                               "HLT_e300_etcut"}; 
  // === Z triggers (single+multielectron)
  // 2015
  string temptriggs_Z2015[] = {"HLT_2e12_lhloose_L12EM10VH",
                               "HLT_e17_lhloose_2e9_lhloose",
                               "HLT_e24_lhmedium_L1EM20VH", 
                               "HLT_e60_lhmedium", 
                               "HLT_e120_lhloose" };
  // 2016
  string temptriggs_Z2016[] = {"HLT_e24_lhtight_nod0_ivarloose",
                               "HLT_e26_lhtight_nod0_ivarloose",
                               "HLT_e60_lhmedium_nod0",
                               "HLT_e60_medium",        
                               "HLT_e140_lhloose_nod0",
                               "HLT_e300_etcut",
                               "HLT_2e15_lhvloose_nod0_L12EM13VH",
                               "HLT_2e17_lhvloose_nod0",
                               "HLT_e17_lhloose_nod0_2e9_lhloose_nod0"};
  // Choose which trigger to use here
  // must select from arrays above
  alg->ntrigs =         ARRAY_SIZE(temptriggs_W2016);
  for( int i1 = 0; i1 < ARRAY_SIZE(temptriggs_W2016); i1++ )
               alg->triggers[i1] = temptriggs_W2016[i1];

  
  //_________________
  // Submit the job
  //

  // Add the analysis to the job
  alg->outputName = "local";
  job.algsAdd( alg );

  // Select driver
   EL::DirectDriver driver;
  driver.submit( job, "outputDir" ); 
  // Print stats
  timer.Stop();
  cout<<"\n\n\nProcessed in "<<timer.RealTime()<<" real, "<<timer.CpuTime()<<" CPU seconds\n\n\n"<<endl;

  return 0;

}

